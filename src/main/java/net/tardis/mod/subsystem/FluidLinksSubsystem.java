package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.tileentities.ConsoleTile;

public class FluidLinksSubsystem extends Subsystem implements ITickable{

	public FluidLinksSubsystem(ConsoleTile console, Item item) {
		super(console, item);
		console.registerTicker(this);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

	@Override
	public void tick(ConsoleTile console) {
		if(this.getItem() == null || this.getItem().isEmpty()) {
			this.getItemFromWorld(console.getWorld());
			return;
		}
		
		if(console.getWorld().getGameTime() % (20 * 5) == 0 && console.isInFlight()) {
			this.getItem().attemptDamageItem(1, console.getWorld().getRandom(), null);
		}
	}

}
