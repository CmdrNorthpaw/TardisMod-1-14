package net.tardis.mod.misc.vm;

import java.util.HashMap;
import java.util.Map;

public class VortexMFunctions {
	public static Map<Integer, IFunction> FUNCTIONS = new HashMap<Integer, IFunction>(); 
	
	public void init() {
		FUNCTIONS.put(0, new TeleportFunction());
		FUNCTIONS.put(1, new ChargeStatusFunction());
	}
	
    public static IFunction getFunction(Integer id) {
        if (FUNCTIONS.containsKey(id)) {
            return FUNCTIONS.get(id);
        }
        return FUNCTIONS.get(id);
    }
    
    public static String getNameKey(Integer id) {
    	return FUNCTIONS.get(id).getNameKey();
    }

}
