package net.tardis.mod.events;


import static net.tardis.mod.sonic.SonicManager.ENTITY_INTERACT;

import net.minecraft.advancements.Advancement;
import net.minecraft.block.BedBlock;
import net.minecraft.block.BellBlock;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.INBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ChunkLoaderCapability;
import net.tardis.mod.cap.IChunkLoader;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.cap.ITardisWorldData.TardisWorldProvider;
import net.tardis.mod.cap.items.IWatch;
import net.tardis.mod.cap.items.WatchCapability;
import net.tardis.mod.cap.LightCapability;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.dimensions.TardisDimension;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.sonic.SonicManager;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

@Mod.EventBusSubscriber(modid = Tardis.MODID)
public class CommonEvents {
	
	public static final ResourceLocation LIGHT_CAP = new ResourceLocation(Tardis.MODID, "light");
	public static final ResourceLocation CHUNK_CAP = new ResourceLocation(Tardis.MODID, "loader");
	public static final ResourceLocation TARDIS_CAP = new ResourceLocation(Tardis.MODID, "tardis_data");
	public static final ResourceLocation WATCH_CAP = new ResourceLocation(Tardis.MODID, "watch");

	@SubscribeEvent
	public static void attachChunkCaps(AttachCapabilitiesEvent<Chunk> event) {
		if(event.getObject().getWorld().getDimension().getType().getModType() == TDimensions.TARDIS) {
			event.addCapability(LIGHT_CAP, new ILightCap.LightProvider(new LightCapability(event.getObject())));
		}
	}
	
	@SubscribeEvent
	public static void attachItemStackCap(AttachCapabilitiesEvent<ItemStack> event) {
		if(event.getObject().getItem() == TItems.POCKET_WATCH)
			event.addCapability(WATCH_CAP, new IWatch.Provider(new WatchCapability()));
	}
	
	@SubscribeEvent
	public static void attachChunkLoadCaps(AttachCapabilitiesEvent<World> event) {
		if (event.getObject().isRemote) return;
		
		if(event.getObject().getDimension() instanceof TardisDimension)
			event.addCapability(TARDIS_CAP, new TardisWorldProvider(event.getObject()));
		
		 final LazyOptional<IChunkLoader> inst = LazyOptional.of(() -> new ChunkLoaderCapability((ServerWorld)event.getObject()));
	        final ICapabilitySerializable<INBT> provider = new ICapabilitySerializable<INBT>() {
	            @Override
	            public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
	                return Capabilities.CHUNK_LOADER.orEmpty(cap, inst);
	            }

	            @Override
	            public INBT serializeNBT() {
	                return Capabilities.CHUNK_LOADER.writeNBT(inst.orElse(null), null);
	            }

	            @Override
	            public void deserializeNBT(INBT nbt) {
	            	Capabilities.CHUNK_LOADER.readNBT(inst.orElse(null), null, nbt);
	            }
	        };
        event.addCapability(CHUNK_CAP, provider);
        event.addListener(() -> inst.invalidate());
	}
	
	@SubscribeEvent
	public static void onChunkLoad(ChunkEvent.Load event) {
		
	}


	@SubscribeEvent
	public static void interactionWithEntity(PlayerInteractEvent.EntityInteract event) {
		//Armor stands do not allow items to interact with them properly, as right clicking on a armor stand
		//Puts the users active item into the armor stands hand, we don't want this behaviour unless the player is sneaking
		if (event.getTarget() instanceof ArmorStandEntity && !event.getEntityLiving().isSneaking()) {
			if (event.getItemStack().getItem() instanceof SonicItem) {
				event.setCanceled(true);
				ItemStack stack = event.getItemStack();
				SonicManager.ISonicMode mode = SonicItem.getCurrentMode(stack);
				if (mode == ENTITY_INTERACT.getSonicType()) {
					ENTITY_INTERACT.getSonicType().processEntity(event.getPlayer(), event.getTarget(), event.getItemStack());
				}
			}
		}
	}

	@SubscribeEvent
	public static void onBlockBreak(BlockEvent.BreakEvent event) {
		if(event.getState().getBlock() instanceof IDontBreak) {
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public static void onBlockClicked(PlayerInteractEvent.RightClickBlock event) {
		if(event.getWorld().getBlockState(event.getPos()).getBlock() instanceof BellBlock) {
			ChunkPos pos = event.getWorld().getChunk(event.getEntity().getPosition()).getPos();
			for(int x = -3; x < 3; ++x) {
				for(int z = -3; z < 3; ++z) {
					for(TileEntity te : event.getWorld().getChunk(pos.x + x, pos.z + z).getTileEntityMap().values()) {
						if(te instanceof ExteriorTile || te instanceof BrokenExteriorTile) {
							BlockPos soundPos = te.getPos().subtract(event.getEntity().getPosition());
							Vec3d vec = new Vec3d(soundPos.getX(), soundPos.getY(), soundPos.getZ()).normalize().scale(8);
							event.getWorld().playSound(null, event.getEntity().getPosition().add(new BlockPos(vec.x, vec.y, vec.z)), TSounds.SINGLE_CLOISTER, SoundCategory.BLOCKS, 1F, 0.5F);
						}
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onWorldTick(WorldTickEvent event) {
		event.world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> cap.tick());
	}


	@SubscribeEvent
	public static void onEntityJoin(EntityJoinWorldEvent event){
		if(event.getWorld().getDimension().getType().getModType() == TDimensions.TARDIS) {
			if(event.getEntity() instanceof IMob) {
				TileEntity te = event.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile)
					((ConsoleTile)te).getInteriorManager().setAlarmOn(true);
			}
			
			//If a player enters the TARDIS
			if(event.getEntity() instanceof ServerPlayerEntity) {
				ServerPlayerEntity player = (ServerPlayerEntity)event.getEntity();
				if(TardisHelper.isInOwnedTardis(player)){
					Advancement adv = player.world.getServer().getAdvancementManager().getAdvancement(new ResourceLocation(Tardis.MODID, "nemo_interior"));
					if(adv != null) {
						if(player.getAdvancements().getProgress(adv).isDone()) {
							//Unlock things based on Achievement
						}
					}
				}
			}
		}
	}
	
	//Only works in player's own tardis at the moment
	@SubscribeEvent
	public static void onSleep(PlayerSleepInBedEvent event) {
		//Checks if player have a Tardis
		if (TardisHelper.hasTARDIS(event.getPlayer().getServer(), event.getPlayer().getUniqueID())) {
		DimensionType tardis = TDimensions.registerOrGet(event.getPlayer().getUniqueID().toString(), TDimensions.TARDIS);
		ServerWorld tardisWorld = ServerLifecycleHooks.getCurrentServer().getWorld(tardis);
		ConsoleTile console = TardisHelper.getConsole(tardis);
		if (tardisWorld != null) {
			if(console instanceof ConsoleTile && console !=null) {
					if(TardisHelper.isInTardis(event.getPlayer())) {
						console.addOrUpdateBedLoc(event.getPlayer(), event.getPos()); //Sets player spawn to Tardis dim
					}
					else if (event.getPlayer().getSpawnDimension() != tardis){
						//If the player sleeps successfully in a non Tardis dimension, remove the bed pos so vanilla will overwrite the tardis one
						//Prevents you from always spawning in the tardis even if you sleep in another dimension
						console.removePlayerBedLoc(event.getPlayer()); 
					}
				}
			}	
		}
	}
	
	@SubscribeEvent
	public static void onPlayerRespawn(PlayerRespawnEvent event) {
		if (TardisHelper.hasTARDIS(event.getPlayer().getServer(), event.getPlayer().getUniqueID())) {
			DimensionType tardis = TDimensions.registerOrGet(event.getPlayer().getUniqueID().toString(), TDimensions.TARDIS);
			ServerWorld tardisWorld = ServerLifecycleHooks.getCurrentServer().getWorld(tardis);
			if (tardisWorld != null) {
				ConsoleTile console = TardisHelper.getConsole(tardis);
				if (console.getBedPosForPlayer(event.getPlayer()) != null) {
					BlockPos pos = console.getBedPosForPlayer(event.getPlayer());
					if(!(tardisWorld.getBlockState(pos).getBlock() instanceof BedBlock) || console.getBedPosForPlayer(event.getPlayer()).equals(null)) {
							return; //should respawn them at overworld if they break bed in tardis and die
					}
					event.getPlayer().setPosition(pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5);
					if (!TardisHelper.isInTardis(event.getPlayer())) {
						Helper.teleportEntities(event.getPlayer(), tardisWorld, pos.getX()  + 0.5, pos.getY() + 1, pos.getZ() + 0.5, event.getPlayer().rotationYaw, event.getPlayer().rotationPitch);
					}
				}
			}
		}
	}
}
