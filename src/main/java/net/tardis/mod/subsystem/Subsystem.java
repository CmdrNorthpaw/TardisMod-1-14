package net.tardis.mod.subsystem;

import javax.annotation.Nullable;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public abstract class Subsystem implements IRegisterable<Subsystem>, INBTSerializable<CompoundNBT>{

	private Item itemKey;
	private ResourceLocation registryName;
	private ItemStack item = ItemStack.EMPTY;
	protected ConsoleTile console;
	
	public Subsystem(ConsoleTile console, Item item) {
		this.console = console;
		this.itemKey = item;
	}

	@Override
	public Subsystem setRegistryName(ResourceLocation regName) {
		this.registryName = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	public void setItem(ItemStack stack) {
		this.item = stack;
	}
	
	public ItemStack getItem() {
		if(item == null || item.isEmpty())
			this.getItemFromWorld(console.getWorld());
		return this.item == null ? ItemStack.EMPTY : item;
	}

	public void damage(@Nullable ServerPlayerEntity player, int amt) {
		this.getItem().attemptDamageItem(amt, console.getWorld().rand, player);
	}
	
	public boolean canBeUsed() {
		return this.getItem().getDamage() < this.getItem().getMaxDamage();
	}

	public void getItemFromWorld(World world) {
		world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
			PanelInventory inv = cap.getEngineInventoryForSide(Direction.NORTH);
			for(int i = 0; i < inv.getSizeInventory(); ++i) {
				if(inv.getStackInSlot(i).getItem() == itemKey) {
					this.setItem(inv.getStackInSlot(i));
					return;
				}
			}
		});
	}
	
	public boolean stopFlight() {
		return !this.canBeUsed();
	}
}
