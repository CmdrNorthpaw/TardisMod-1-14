package net.tardis.mod.controls;

import java.util.Random;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class RandomiserControl extends BaseControl{
	
	private static Random rand = new Random();
	
	public RandomiserControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);
	}
	
	@Override
	public Vec3d getPos() {
		ConsoleTile console = this.getConsole();
		if(console instanceof NemoConsoleTile)
			return new Vec3d(-7 / 16.0, 8 / 16.0, -4 / 16.0);
		return new Vec3d(0, 12 / 16.0, 9 / 16.0);
	}


	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		
		if(!player.world.isRemote) {
			int rad = 5 * console.coordIncr;
			BlockPos dest = console.getDestination().add(rad - rand.nextInt(rad * 2), 0, rad - rand.nextInt(rad * 2));
			console.setDestination(console.getDestinationDimension(), dest);
			this.setAnimationTicks(10);
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.RANDOMISER;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}
	
	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
