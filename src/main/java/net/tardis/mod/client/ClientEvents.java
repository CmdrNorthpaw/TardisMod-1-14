package net.tardis.mod.client;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.renderers.layers.SonicLaserRenderLayer;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BessieHornMessage;

@Mod.EventBusSubscriber(modid = Tardis.MODID, value = Dist.CLIENT)
public class ClientEvents {

    @SubscribeEvent
    public static void onRenderWorldLast(RenderWorldLastEvent event) {
        SonicLaserRenderLayer.onRenderWorldLast(event);
    }

    @SubscribeEvent
    public static void input(InputUpdateEvent event) {
        if (event.getMovementInput().jump) {
            Network.sendToServer(new BessieHornMessage());
        }
    }

}
