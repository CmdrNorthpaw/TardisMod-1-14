package net.tardis.mod.client.models.entity.dalek;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.entity.DalekEntity;

public class SWDalekModel extends EntityModel<DalekEntity> {
	private final RendererModel dalek;
	private final RendererModel dalek_upper;
	private final RendererModel bone7;
	private final RendererModel bone13;
	private final RendererModel bone12;
	private final RendererModel head;
	private final RendererModel eyestalk;
	private final RendererModel eye;
	private final RendererModel gun;
	private final RendererModel dalek_lower;
	private final RendererModel base;
	private final RendererModel base2;
	private final RendererModel eggs;
	private final RendererModel bone2;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel bone4;
	private final RendererModel bone3;
	private final RendererModel bone;
	private final RendererModel bone11;
	private final RendererModel bone10;

    public SWDalekModel() {
		textureWidth = 128;
		textureHeight = 128;

		dalek = new RendererModel(this);
		dalek.setRotationPoint(0.0F, 24.0F, 0.0F);

		dalek_upper = new RendererModel(this);
		dalek_upper.setRotationPoint(0.0F, 0.0F, 2.5F);
		dalek.addChild(dalek_upper);
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 16, 57, -5.5F, -22.45F, -7.05F, 9, 6, 5, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 26, 56, 3.25F, -22.45F, -7.05F, 2, 6, 5, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 26, 56, 3.25F, -19.45F, -2.05F, 2, 3, 8, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 26, 56, -5.5F, -19.45F, -2.05F, 2, 3, 8, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 26, 56, -5.15F, -19.45F, 5.2F, 10, 3, 1, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 88, 100, -5.15F, -24.625F, -4.825F, 10, 4, 10, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 8, 110, -6.15F, -17.275F, -8.0F, 12, 1, 1, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 8, 110, -6.15F, -17.275F, 4.75F, 12, 1, 3, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 0, 103, -7.05F, -17.275F, -7.05F, 14, 1, 13, 0.0F, false));
		dalek_upper.cubeList.add(new ModelBox(dalek_upper, 81, 98, -5.15F, -22.5F, -6.05F, 10, 6, 12, 0.0F, false));

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, 0.0F, -2.5F);
		setRotationAngle(bone7, -0.3491F, 0.0F, 0.0F);
		dalek_upper.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 11, 54, -4.575F, -18.675F, -12.2F, 9, 6, 5, 0.0F, false));

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.0F, 0.0F, -2.5F);
		setRotationAngle(bone13, -0.3491F, 0.0F, 0.0F);
		dalek_upper.addChild(bone13);
		bone13.cubeList.add(new ModelBox(bone13, 93, 52, -4.075F, -18.175F, -12.225F, 8, 5, 0, 0.0F, false));

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, -15.0F, 3.75F);
		setRotationAngle(bone12, 0.0873F, 0.0F, 0.0F);
		dalek_upper.addChild(bone12);

		head = new RendererModel(this);
		head.setRotationPoint(0.0F, -23.0F, -1.0F);
		dalek_upper.addChild(head);
		head.cubeList.add(new ModelBox(head, 0, 84, -4.5F, -2.625F, -3.075F, 9, 1, 9, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 0, 84, -4.5F, -6.75F, -3.075F, 9, 1, 9, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 0, 84, -4.0F, -3.75F, -2.575F, 8, 2, 8, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 0, 0, -4.0F, -5.75F, -2.575F, 8, 2, 8, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 82, 105, -4.0F, -7.625F, -2.575F, 8, 1, 8, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 92, 101, -3.5F, -8.625F, -2.075F, 7, 1, 7, 0.0F, false));

		eyestalk = new RendererModel(this);
		eyestalk.setRotationPoint(0.0F, -7.0F, -3.5F);
		head.addChild(eyestalk);

		eye = new RendererModel(this);
		eye.setRotationPoint(0.0F, 6.75F, -0.125F);
		eyestalk.addChild(eye);

		gun = new RendererModel(this);
		gun.setRotationPoint(0.0F, -18.35F, -7.75F);
		dalek_upper.addChild(gun);
		gun.cubeList.add(new ModelBox(gun, 89, 49, -3.5F, -2.25F, -1.5F, 7, 4, 2, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 61, 91, -1.5F, -1.75F, -2.5F, 3, 3, 2, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 61, 91, -1.0F, -1.25F, -5.5F, 2, 2, 2, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 61, 91, -1.0F, -1.25F, -8.75F, 2, 2, 3, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 89, 49, -1.5F, -1.75F, -3.5F, 3, 3, 1, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 89, 49, -1.5F, -1.75F, -6.0F, 3, 3, 1, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 89, 49, -1.5F, -1.75F, -7.75F, 3, 3, 1, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 84, 27, -1.0F, -1.25F, -10.75F, 2, 2, 2, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 84, 27, -0.5F, -1.5F, -10.5F, 1, 2, 3, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 84, 27, -1.25F, -0.75F, -10.5F, 1, 1, 3, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 84, 27, 0.25F, -0.75F, -10.5F, 1, 1, 3, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 84, 27, -0.5F, -1.0F, -10.5F, 1, 2, 3, 0.0F, false));

		dalek_lower = new RendererModel(this);
		dalek_lower.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek.addChild(dalek_lower);
		dalek_lower.cubeList.add(new ModelBox(dalek_lower, 12, 104, -5.65F, -16.3F, 8.0F, 11, 2, 2, 0.0F, false));
		dalek_lower.cubeList.add(new ModelBox(dalek_lower, 0, 96, -6.55F, -16.3F, -4.55F, 13, 2, 13, 0.0F, false));
		dalek_lower.cubeList.add(new ModelBox(dalek_lower, 18, 105, -6.15F, -16.3F, -5.5F, 12, 2, 1, 0.0F, false));
		dalek_lower.cubeList.add(new ModelBox(dalek_lower, 12, 104, -3.5F, -16.3F, -7.875F, 7, 2, 2, 0.0F, false));
		dalek_lower.cubeList.add(new ModelBox(dalek_lower, 18, 105, -4.575F, -16.3F, -6.5F, 9, 2, 1, 0.0F, false));

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek_lower.addChild(base);
		base.cubeList.add(new ModelBox(base, 70, 12, -8.2F, -3.0F, -3.9F, 16, 3, 12, 0.0F, false));

		base2 = new RendererModel(this);
		base2.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek_lower.addChild(base2);
		base2.cubeList.add(new ModelBox(base2, 93, 12, -7.75F, -3.0F, -4.875F, 15, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -7.75F, -3.0F, 8.125F, 15, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -5.4F, -3.0F, -7.8F, 11, 3, 3, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -6.15F, -3.0F, 7.475F, 12, 3, 3, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -4.45F, -3.0F, -9.75F, 9, 3, 2, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 98, 28, -3.75F, -3.0F, -10.75F, 7, 3, 2, 0.0F, false));

		eggs = new RendererModel(this);
		eggs.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek_lower.addChild(eggs);
		eggs.cubeList.add(new ModelBox(eggs, 84, 90, -4.45F, -14.6F, -3.9F, 9, 12, 13, 0.0F, false));
		eggs.cubeList.add(new ModelBox(eggs, 38, 66, -1.0F, -13.375F, 8.33F, 2, 2, 1, 0.0F, false));
		eggs.cubeList.add(new ModelBox(eggs, 38, 66, -1.0F, -9.375F, 8.33F, 2, 2, 1, 0.0F, false));
		eggs.cubeList.add(new ModelBox(eggs, 38, 66, -1.0F, -5.375F, 8.33F, 2, 2, 1, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone2, 0.0F, 0.0F, 0.0873F);
		eggs.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 89, 98, -7.55F, -14.3F, -3.9F, 3, 13, 12, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone5, 0.0F, 0.0F, 0.0873F);
		eggs.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 102, 109, -6.25F, -14.3F, 6.75F, 3, 13, 3, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone6, 0.0F, 0.0F, -0.0873F);
		eggs.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 102, 109, 2.85F, -14.3F, 6.75F, 3, 13, 3, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone4, 0.0F, 0.0F, -0.0873F);
		eggs.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 96, 102, 4.15F, -14.3F, -3.9F, 3, 13, 12, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone3, -0.1745F, -0.6109F, 0.3491F);
		eggs.addChild(bone3);

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone, -0.1745F, 0.0F, 0.0F);
		eggs.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 99, 111, -4.1F, -13.0F, -9.1F, 8, 13, 3, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, -3.0F, -12.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, 1.25F, -12.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, 1.25F, -8.5F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, 1.25F, -5.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, -3.0F, -8.5F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, -3.0F, -5.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 109, 110, -0.35F, -13.0F, -10.075F, 1, 13, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 96, 107, -4.8F, -13.325F, -7.8F, 10, 13, 4, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone11, 0.0F, 0.0F, -0.0873F);
		eggs.addChild(bone11);
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -12.725F, -1.45F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -12.725F, 1.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -12.725F, 4.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -8.725F, -1.45F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -8.725F, 1.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -8.725F, 4.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -4.925F, -1.45F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -4.925F, 1.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -4.925F, 4.55F, 1, 2, 2, 0.0F, false));

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone10, 0.0F, 0.0F, 0.0873F);
		eggs.addChild(bone10);
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -8.825F, 4.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -8.825F, 1.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -8.825F, -1.45F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -4.925F, 4.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -4.925F, 1.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -4.925F, -1.45F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -12.725F, 4.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -12.725F, 1.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -12.725F, -1.45F, 1, 2, 2, 0.0F, false));
    }

    @Override
    public void render(DalekEntity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        setRotationAngles(entity, f, f1, f2, f3, f4, f5);
        dalek.render(f5);
    }

    public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
        RendererModel.rotateAngleX = x;
        RendererModel.rotateAngleY = y;
        RendererModel.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(DalekEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor) {
        dalek_upper.rotateAngleX = 0;
        dalek_upper.rotateAngleY = (float) Math.toRadians(netHeadYaw);
        dalek_upper.rotateAngleZ = 0;

        gun.rotateAngleX = (float) Math.toRadians(headPitch);
        gun.rotateAngleY = 0;
        gun.rotateAngleZ = 0;
    }
}