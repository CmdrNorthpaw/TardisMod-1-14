package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class HandbrakeControl extends BaseControl {

	/*
	 * True if brake is off and TARDIS can fly
	 */
	private boolean isFree = false;

	public HandbrakeControl(ConsoleTile console) {
		super(console);
		
	}
	
	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		return EntitySize.flexible(0.1875F, 0.1875F);
	}
	
	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-2 / 16.0, 8 / 16.0, -13 / 16.0F);
		return new Vec3d(5 / 16.0, 6 / 16.0, 15 / 16.0);
	}

	
	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.isFree = tag.getBoolean("free");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("free", this.isFree);
		return tag;
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(console == null || !console.hasWorld())
			return false;
		if(!console.getWorld().isRemote) {
			this.isFree = !this.isFree;
			if(console.getControl(ThrottleControl.class).getAmount() > 0.0F) {
				if(this.isFree()) {
					console.takeoff();
				}
			}
		}
		return true;
	}
	
	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return isFree() ? TSounds.HANDBRAKE_RELEASE : TSounds.HANDBRAKE_ENGAGE;
	}

	public boolean isFree() {
		return this.isFree;
	}

}
