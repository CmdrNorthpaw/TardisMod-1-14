package net.tardis.mod.client.models.consoles;
//Made with Blockbench
//Paste this code into your mod.

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.tileentities.ConsoleTile;

public class ModelConsoleCopper extends Model implements IConsoleModel{
	private final RendererModel copper;
	private final RendererModel rotortop;
	private final RendererModel rotor;
	private final RendererModel controls;
	private final RendererModel panelone;
	private final RendererModel greyshit;
	private final RendererModel blackbordnon;
	private final RendererModel blackboy2;
	private final RendererModel misc;
	private final RendererModel blackboy;
	private final RendererModel handbreak;
	private final RendererModel blackboard;
	private final RendererModel throttle;
	private final RendererModel levers;
	private final RendererModel white;
	private final RendererModel redswitches;
	private final RendererModel paneltwo;
	private final RendererModel whity;
	private final RendererModel slider;
	private final RendererModel rotator;
	private final RendererModel bone109;
	private final RendererModel bone113;
	private final RendererModel keyboard;
	private final RendererModel panelthree;
	private final RendererModel bluevalves;
	private final RendererModel bluevalves2;
	private final RendererModel plugandkey;
	private final RendererModel panelfour;
	private final RendererModel panelfour2;
	private final RendererModel panelfive;
	private final RendererModel panelfive2;
	private final RendererModel bone120;
	private final RendererModel panelfive3;
	private final RendererModel bone121;
	private final RendererModel panelsix;
	private final RendererModel wibbly;
	private final RendererModel pumps;
	private final RendererModel pump;
	private final RendererModel pump2;
	private final RendererModel pump3;
	private final RendererModel redlever;
	private final RendererModel bigboy;
	private final RendererModel blackboy3;
	private final RendererModel pump4;
	private final RendererModel pump5;
	private final RendererModel redlever2;
	private final RendererModel bigreder;
	private final RendererModel smallblack;
	private final RendererModel falsepump;
	private final RendererModel bone112;
	private final RendererModel rotorframe;
	private final RendererModel bone110;
	private final RendererModel bone111;
	private final RendererModel unit;
	private final RendererModel console2;
	private final RendererModel model;
	private final RendererModel console;
	private final RendererModel frame;
	private final RendererModel upper;
	private final RendererModel bone4;
	private final RendererModel monster2;
	private final RendererModel bone5;
	private final RendererModel monster;
	private final RendererModel upper2;
	private final RendererModel bone6;
	private final RendererModel bone59;
	private final RendererModel yeeeet;
	private final RendererModel bone71;
	private final RendererModel bone57;
	private final RendererModel bone58;
	private final RendererModel bone7;
	private final RendererModel bone72;
	private final RendererModel upper3;
	private final RendererModel bone8;
	private final RendererModel bone9;
	private final RendererModel bone70;
	private final RendererModel upp;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel bone13;
	private final RendererModel bone14;
	private final RendererModel bone15;
	private final RendererModel bone16;
	private final RendererModel bone18;
	private final RendererModel bone19;
	private final RendererModel bone17;
	private final RendererModel frame2;
	private final RendererModel upper4;
	private final RendererModel bone10;
	private final RendererModel bone20;
	private final RendererModel upper5;
	private final RendererModel bone21;
	private final RendererModel bone22;
	private final RendererModel upper6;
	private final RendererModel bone23;
	private final RendererModel bone24;
	private final RendererModel upp2;
	private final RendererModel bone25;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel upp3;
	private final RendererModel bone39;
	private final RendererModel bone40;
	private final RendererModel bone41;
	private final RendererModel bone28;
	private final RendererModel bone29;
	private final RendererModel bone30;
	private final RendererModel bone31;
	private final RendererModel bone32;
	private final RendererModel bone33;
	private final RendererModel cubes3;
	private final RendererModel cubes2;
	private final RendererModel cubes;
	private final RendererModel outerframe;
	private final RendererModel bone;
	private final RendererModel bone56;
	private final RendererModel bone2;
	private final RendererModel bone54;
	private final RendererModel bone3;
	private final RendererModel bone55;
	private final RendererModel bone119;
	private final RendererModel hollowglow;
	private final RendererModel bone36;
	private final RendererModel bone35;
	private final RendererModel bone34;
	private final RendererModel ihaveexam;
	private final RendererModel bone42;
	private final RendererModel bone38;
	private final RendererModel rando;
	private final RendererModel yes;
	private final RendererModel bone60;
	private final RendererModel bone65;
	private final RendererModel bone66;
	private final RendererModel bone67;
	private final RendererModel bone68;
	private final RendererModel bone69;
	private final RendererModel ihaveexam2;
	private final RendererModel bone43;
	private final RendererModel bone44;
	private final RendererModel bone45;
	private final RendererModel ihaveexam3;
	private final RendererModel bone48;
	private final RendererModel bone47;
	private final RendererModel bone52;
	private final RendererModel bone53;
	private final RendererModel bone46;
	private final RendererModel bone49;
	private final RendererModel bone50;
	private final RendererModel bone62;
	private final RendererModel bone63;
	private final RendererModel bone51;
	private final RendererModel bone73;
	private final RendererModel bone37;
	private final RendererModel bone64;
	private final RendererModel underpillar3;
	private final RendererModel bone79;
	private final RendererModel bone80;
	private final RendererModel bone81;
	private final RendererModel underpillar2;
	private final RendererModel bone61;
	private final RendererModel bone74;
	private final RendererModel bone75;
	private final RendererModel underpillar4;
	private final RendererModel bone76;
	private final RendererModel bone77;
	private final RendererModel bone78;
	private final RendererModel underpillar5;
	private final RendererModel bone82;
	private final RendererModel bone83;
	private final RendererModel bone84;
	private final RendererModel underpillar6;
	private final RendererModel bone85;
	private final RendererModel bone86;
	private final RendererModel bone87;
	private final RendererModel underpillar7;
	private final RendererModel bone88;
	private final RendererModel bone89;
	private final RendererModel bone90;
	private final RendererModel greenfloor;
	private final RendererModel bone91;
	private final RendererModel bone92;
	private final RendererModel bone93;
	private final RendererModel bone94;
	private final RendererModel bone95;
	private final RendererModel bone96;
	private final RendererModel greenfloor2;
	private final RendererModel bone97;
	private final RendererModel bone98;
	private final RendererModel bone99;
	private final RendererModel bone100;
	private final RendererModel bone101;
	private final RendererModel bone102;
	private final RendererModel greenwall;
	private final RendererModel wall;
	private final RendererModel under6;
	private final RendererModel bone108;
	private final RendererModel under5;
	private final RendererModel bone107;
	private final RendererModel under4;
	private final RendererModel bone106;
	private final RendererModel under3;
	private final RendererModel bone105;
	private final RendererModel under2;
	private final RendererModel bone104;
	private final RendererModel under;
	private final RendererModel bone103;

	public ModelConsoleCopper() {
		textureWidth = 51200;
		textureHeight = 51200;

		copper = new RendererModel(this);
		copper.setRotationPoint(0.0F, 24.0F, 0.0F);

		rotortop = new RendererModel(this);
		rotortop.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(rotortop);
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -175.0F, -4800.0F, -175.0F, 350, 200, 350, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -250.0F, -4600.0F, -250.0F, 500, 425, 500, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -150.0F, -5125.0F, -150.0F, 300, 325, 300, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -100.0F, -5400.0F, -100.0F, 200, 300, 200, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -50.0F, -5525.0F, -50.0F, 100, 350, 100, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -100.0F, -5800.0F, -100.0F, 200, 275, 200, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -175.0F, -6075.0F, -175.0F, 350, 325, 350, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 41800, 6100, -175.0F, -4175.0F, -175.0F, 350, 50, 350, 0.0F, false));

		rotor = new RendererModel(this);
		rotor.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(rotor);
		rotor.cubeList.add(new ModelBox(rotor, 41800, 6100, -175.0F, -2250.0F, -175.0F, 350, 200, 350, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 41800, 6100, -250.0F, -2050.0F, -250.0F, 500, 425, 500, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 41800, 6100, -150.0F, -2450.0F, -150.0F, 300, 200, 300, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 41800, 6100, -100.0F, -2650.0F, -100.0F, 200, 200, 200, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 41800, 6100, -50.0F, -2725.0F, -50.0F, 100, 75, 100, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 41800, 6100, -50.0F, -2900.0F, -50.0F, 100, 75, 100, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 41800, 6100, -75.0F, -2825.0F, -75.0F, 150, 100, 150, 0.0F, false));

		controls = new RendererModel(this);
		controls.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(controls);

		panelone = new RendererModel(this);
		panelone.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(panelone);

		greyshit = new RendererModel(this);
		greyshit.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(greyshit, 0.3491F, -0.5236F, 0.0F);
		panelone.addChild(greyshit);
		greyshit.cubeList.add(new ModelBox(greyshit, 39900, 18400, 112.5F, -1767.5F, -700.0F, 75, 100, 125, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 39900, 18400, 112.5F, -1742.5F, -525.0F, 75, 75, 75, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 39900, 18400, 112.5F, -1742.5F, -825.0F, 75, 75, 75, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 39900, 19700, 137.5F, -1742.5F, -750.0F, 25, 75, 75, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 39900, 19700, 137.5F, -1742.5F, -600.0F, 25, 75, 75, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 33900, 50700, -212.5F, -1742.5F, -950.0F, 125, 100, 200, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 43000, 20600, -187.5F, -1730.0F, -300.0F, 75, 100, 75, 0.0F, false));

		blackbordnon = new RendererModel(this);
		blackbordnon.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackbordnon, 0.0F, -1.0472F, 0.0F);
		panelone.addChild(blackbordnon);
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 38000, 25600, -137.5F, -1917.5F, -600.0F, 75, 175, 100, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 38900, 18000, -287.5F, -1592.5F, -775.0F, 75, 75, 50, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 39700, 19500, -262.5F, -1617.5F, -725.0F, 75, 100, 50, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 41100, 43900, -137.5F, -2017.5F, -700.0F, 75, 75, 125, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 41100, 43900, -137.5F, -1942.5F, -675.0F, 75, 25, 125, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 38000, 25600, -122.5F, -2042.5F, -690.0F, 50, 25, 50, 0.0F, false));

		blackboy2 = new RendererModel(this);
		blackboy2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackboy2, 0.0F, -0.5236F, 0.0F);
		panelone.addChild(blackboy2);
		blackboy2.cubeList.add(new ModelBox(blackboy2, 35800, 23700, 87.5F, -1192.5F, -1600.0F, 50, 75, 50, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 35800, 23700, 162.5F, -1142.5F, -1600.0F, 50, 75, 50, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 35800, 23700, 87.5F, -1192.5F, -1600.0F, 50, 75, 50, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 35800, 23700, 112.5F, -1067.5F, -1600.0F, 25, 25, 50, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 35800, 23700, -212.5F, -1167.5F, -1600.0F, 125, 125, 50, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 35800, 23700, -187.5F, -1142.5F, -1625.0F, 75, 75, 50, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 35800, 23700, -162.5F, -1117.5F, -1650.0F, 25, 25, 50, 0.0F, false));

		misc = new RendererModel(this);
		misc.setRotationPoint(0.0F, 0.0F, 0.0F);
		blackboy2.addChild(misc);

		blackboy = new RendererModel(this);
		blackboy.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackboy, 0.0F, -1.5708F, 0.0F);
		panelone.addChild(blackboy);
		blackboy.cubeList.add(new ModelBox(blackboy, 23000, 32000, 87.5F, -1142.5F, -1625.0F, 125, 100, 75, 0.0F, false));
		blackboy.cubeList.add(new ModelBox(blackboy, 23000, 32000, 137.5F, -1192.5F, -1625.0F, 75, 50, 75, 0.0F, false));
		blackboy.cubeList.add(new ModelBox(blackboy, 38400, 22900, -212.5F, -1167.5F, -1600.0F, 125, 125, 50, 0.0F, false));
		blackboy.cubeList.add(new ModelBox(blackboy, 38400, 22900, -187.5F, -1142.5F, -1625.0F, 75, 75, 50, 0.0F, false));

		handbreak = new RendererModel(this);
		handbreak.setRotationPoint(0.0F, -1100.0F, -1550.0F);
		setRotationAngle(handbreak, 0.6109F, 0.0F, 0.0F);
		blackboy.addChild(handbreak);
		handbreak.cubeList.add(new ModelBox(handbreak, 38400, 22900, -60.0F, -74.191F, -30.54F, 125, 125, 100, 0.0F, false));
		handbreak.cubeList.add(new ModelBox(handbreak, 38400, 22900, -20.0F, -424.191F, -55.54F, 50, 400, 50, 0.0F, false));

		blackboard = new RendererModel(this);
		blackboard.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackboard, -0.0873F, -1.0472F, 0.0F);
		panelone.addChild(blackboard);
		blackboard.cubeList.add(new ModelBox(blackboard, 49800, 25000, -37.5F, -1792.5F, -750.0F, 225, 150, 25, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 38000, 25600, -187.5F, -1892.5F, -750.0F, 50, 150, 50, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, -12.5F, -1767.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 37.5F, -1767.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 87.5F, -1767.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 137.5F, -1767.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, -12.5F, -1717.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 37.5F, -1717.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 87.5F, -1717.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 137.5F, -1717.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 112.5F, -1692.5F, -752.5F, 25, 25, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 45900, 35900, 62.5F, -1692.5F, -752.5F, 25, 25, 0, 0.0F, false));

		throttle = new RendererModel(this);
		throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(throttle, 0.0F, -1.5708F, 0.3491F);
		panelone.addChild(throttle);
		throttle.cubeList.add(new ModelBox(throttle, 26700, 31600, -200.0F, -1775.0F, -600.0F, 100, 175, 250, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 26700, 31600, -200.0F, -1825.0F, -550.0F, 100, 50, 150, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 2200, 27100, -225.0F, -1787.5F, -500.0F, 25, 25, 50, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -200.0F, -1840.0F, -450.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -200.0F, -1840.0F, -525.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -200.0F, -1790.0F, -400.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -200.0F, -1790.0F, -575.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -125.0F, -1790.0F, -575.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -125.0F, -1840.0F, -525.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -125.0F, -1840.0F, -450.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 33100, 37400, -125.0F, -1790.0F, -400.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 37000, 16100, 100.0F, -1725.0F, -800.0F, 100, 125, 225, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 41600, 22500, 162.5F, -1765.0F, -775.0F, 25, 15, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 37000, 16100, 112.5F, -1750.0F, -800.0F, 75, 25, 200, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 37000, 16100, 162.5F, -1740.0F, -825.0F, 25, 40, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 37000, 16100, 112.5F, -1740.0F, -825.0F, 25, 40, 25, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 37000, 16100, 162.5F, -1740.0F, -600.0F, 25, 65, 50, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 37000, 16100, 112.5F, -1740.0F, -600.0F, 25, 65, 50, 0.0F, false));

		levers = new RendererModel(this);
		levers.setRotationPoint(-141.071F, -1735.714F, -489.286F);
		throttle.addChild(levers);
		levers.cubeList.add(new ModelBox(levers, 40800, 22000, 41.071F, -26.786F, -10.714F, 25, 50, 100, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 39100, 20300, 41.071F, -1.786F, 89.286F, 25, 25, 125, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 41600, 22500, 16.071F, -1.786F, 214.286F, 50, 25, 25, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 41600, 22500, -83.929F, -1.786F, 214.286F, 50, 25, 25, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 39100, 20300, -83.929F, -1.786F, 89.286F, 25, 25, 125, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 40800, 22000, -83.929F, -26.786F, -10.714F, 25, 50, 100, 0.0F, false));

		white = new RendererModel(this);
		white.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(white, 0.0F, -1.5708F, 0.3491F);
		panelone.addChild(white);
		white.cubeList.add(new ModelBox(white, 41600, 22500, 112.5F, -1765.0F, -775.0F, 25, 15, 25, 0.0F, false));

		redswitches = new RendererModel(this);
		redswitches.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(redswitches, 0.3491F, -1.0472F, 0.0F);
		panelone.addChild(redswitches);
		redswitches.cubeList.add(new ModelBox(redswitches, 38900, 40000, -25.0F, -1650.0F, -700.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 38900, 40000, -100.0F, -1650.0F, -450.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 38900, 40000, 50.0F, -1650.0F, -450.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 38900, 40000, 100.0F, -1650.0F, -575.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 38900, 40000, -150.0F, -1650.0F, -575.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 38800, 22500, -25.0F, -1650.0F, -575.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 12900, 44600, -225.0F, -1650.0F, -775.0F, 100, 100, 100, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 44800, 23300, -350.0F, -1650.0F, -775.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 44800, 23300, -325.0F, -1650.0F, -675.0F, 50, 100, 50, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 40700, 29100, 125.0F, -1650.0F, -800.0F, 100, 100, 100, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 47800, 22200, -312.5F, -1660.0F, -662.5F, 25, 25, 25, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 47800, 22200, -337.5F, -1660.0F, -762.5F, 25, 25, 25, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 40700, 29100, 25.0F, -1637.5F, -475.0F, 100, 100, 100, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 40700, 29100, -125.0F, -1637.5F, -475.0F, 100, 100, 100, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 40700, 29100, -175.0F, -1637.5F, -600.0F, 100, 100, 100, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 40700, 29100, 75.0F, -1637.5F, -600.0F, 100, 100, 100, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 40700, 29100, -50.0F, -1637.5F, -725.0F, 100, 100, 100, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 41600, 23900, -12.5F, -1675.0F, -562.5F, 25, 25, 25, 0.0F, false));

		paneltwo = new RendererModel(this);
		paneltwo.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(paneltwo);

		whity = new RendererModel(this);
		whity.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(whity, 0.0F, -1.5708F, 0.3491F);
		paneltwo.addChild(whity);
		whity.cubeList.add(new ModelBox(whity, 37000, 16100, 100.0F, -1725.0F, -800.0F, 100, 125, 225, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 41600, 22500, 112.5F, -1765.0F, -775.0F, 25, 15, 25, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 41600, 22500, 162.5F, -1765.0F, -775.0F, 25, 15, 25, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 37000, 16100, 112.5F, -1750.0F, -800.0F, 75, 25, 200, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 37000, 16100, 162.5F, -1740.0F, -825.0F, 25, 40, 25, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 37000, 16100, 112.5F, -1740.0F, -825.0F, 25, 40, 25, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 37000, 16100, 162.5F, -1740.0F, -600.0F, 25, 65, 50, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 37000, 16100, 112.5F, -1740.0F, -600.0F, 25, 65, 50, 0.0F, false));

		slider = new RendererModel(this);
		slider.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(slider, 0.3491F, -2.0944F, 0.0F);
		paneltwo.addChild(slider);
		slider.cubeList.add(new ModelBox(slider, 34800, 16500, -425.0F, -1725.0F, -425.0F, 150, 100, 100, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 39700, 26000, -425.0F, -1726.25F, -400.0F, 150, 0, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 37400, 19700, -75.0F, -1635.0F, -575.0F, 125, 100, 125, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 38100, 41100, -50.0F, -1635.0F, -725.0F, 75, 75, 75, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 34600, 39700, -37.5F, -1645.0F, -712.5F, 50, 10, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 48000, 42000, -250.0F, -1610.0F, -1050.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, -250.0F, -1610.0F, -1125.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 48000, 42000, -175.0F, -1610.0F, -1050.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, -175.0F, -1610.0F, -1125.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, -175.0F, -1610.0F, -1200.0F, 150, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 29200, 35600, -100.0F, -1610.0F, -1050.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, -100.0F, -1610.0F, -1125.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 29200, 35600, -25.0F, -1610.0F, -1050.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, -25.0F, -1610.0F, -1125.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, -25.0F, -1610.0F, -1200.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 13000, 43800, 50.0F, -1610.0F, -1050.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, 50.0F, -1610.0F, -1125.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 36900, 25300, 50.0F, -1610.0F, -1200.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 16900, 49400, 125.0F, -1610.0F, -1050.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 35600, 37600, 125.0F, -1610.0F, -1125.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 35600, 37600, 125.0F, -1610.0F, -1200.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 10600, 36000, 200.0F, -1610.0F, -1050.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 35600, 37600, 200.0F, -1610.0F, -1125.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 35600, 37600, 200.0F, -1610.0F, -1200.0F, 50, 50, 50, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 41600, 20900, -225.0F, -1650.0F, -700.0F, 100, 75, 100, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 41600, 20900, -200.0F, -1675.0F, -675.0F, 50, 25, 50, 0.0F, false));

		rotator = new RendererModel(this);
		rotator.setRotationPoint(975.0F, -1550.0F, 525.0F);
		setRotationAngle(rotator, 0.3491F, -2.0944F, 0.0F);
		paneltwo.addChild(rotator);
		rotator.cubeList.add(new ModelBox(rotator, 33400, 47400, -29.663F, 85.097F, -55.009F, 100, 75, 100, 0.0F, false));
		rotator.cubeList.add(new ModelBox(rotator, 33400, 47400, -4.663F, 160.097F, -30.009F, 50, 150, 50, 0.0F, false));
		rotator.cubeList.add(new ModelBox(rotator, 33400, 47400, -4.663F, 35.097F, -30.009F, 50, 75, 50, 0.0F, false));

		bone109 = new RendererModel(this);
		bone109.setRotationPoint(32.837F, 1835.097F, 509.991F);
		setRotationAngle(bone109, 0.0F, 0.0F, -0.3491F);
		rotator.addChild(bone109);
		bone109.cubeList.add(new ModelBox(bone109, 11100, 40900, 475.0F, -1637.5F, -540.0F, 200, 50, 50, 0.0F, false));

		bone113 = new RendererModel(this);
		bone113.setRotationPoint(32.837F, 1235.097F, 509.991F);
		setRotationAngle(bone113, -0.3491F, -1.5708F, -0.3491F);
		rotator.addChild(bone113);
		bone113.cubeList.add(new ModelBox(bone113, 24100, 42500, -616.704F, -889.357F, -720.428F, 200, 50, 50, 0.0F, false));

		keyboard = new RendererModel(this);
		keyboard.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(keyboard, -0.3491F, 1.0472F, 0.0F);
		paneltwo.addChild(keyboard);
		keyboard.cubeList.add(new ModelBox(keyboard, 46400, 22700, -325.0F, -1600.0F, 962.5F, 650, 50, 275, 0.0F, false));
		keyboard.cubeList.add(new ModelBox(keyboard, 41800, 20600, 25.0F, -1600.0F, 687.5F, 50, 75, 275, 0.0F, false));
		keyboard.cubeList.add(new ModelBox(keyboard, 41800, 20600, -75.0F, -1600.0F, 687.5F, 50, 75, 275, 0.0F, false));
		keyboard.cubeList.add(new ModelBox(keyboard, 35800, 18800, -300.0F, -1603.25F, 987.5F, 600, 0, 225, 0.0F, false));

		panelthree = new RendererModel(this);
		panelthree.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelthree, 0.0F, 3.1416F, 0.0F);
		controls.addChild(panelthree);

		bluevalves = new RendererModel(this);
		bluevalves.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bluevalves, 0.3491F, 0.0F, 0.0F);
		panelthree.addChild(bluevalves);
		bluevalves.cubeList.add(new ModelBox(bluevalves, 11600, 42300, -335.0F, -1692.75F, -680.0F, 50, 0, 50, 0.0F, false));
		bluevalves.cubeList.add(new ModelBox(bluevalves, 11600, 42300, -240.0F, -1692.75F, -700.0F, 50, 0, 50, 0.0F, false));
		bluevalves.cubeList.add(new ModelBox(bluevalves, 11600, 42300, -312.5F, -1692.75F, -755.0F, 50, 0, 50, 0.0F, false));

		bluevalves2 = new RendererModel(this);
		bluevalves2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bluevalves2, 0.3491F, 0.0F, 0.0F);
		panelthree.addChild(bluevalves2);
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 37400, 20200, -322.5F, -1692.5F, -662.5F, 20, 75, 20, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 37400, 20200, -222.5F, -1692.5F, -687.5F, 20, 75, 20, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 37400, 20200, -297.5F, -1692.5F, -737.5F, 20, 75, 20, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 39200, 19500, 52.5F, -1642.5F, -612.5F, 170, 25, 145, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 39200, 19500, -117.5F, -1642.5F, -637.5F, 170, 25, 145, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 24600, 13400, -47.5F, -1667.5F, -587.5F, 70, 75, 70, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 22000, 13400, -22.5F, -1767.5F, -565.0F, 20, 150, 20, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 43700, 20900, 102.5F, -1667.5F, -587.5F, 95, 75, 95, 0.0F, false));

		plugandkey = new RendererModel(this);
		plugandkey.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plugandkey, 0.3491F, -0.5236F, 0.0F);
		panelthree.addChild(plugandkey);
		plugandkey.cubeList.add(new ModelBox(plugandkey, 40900, 22000, -195.0F, -1725.0F, -1000.0F, 85, 25, 245, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 41300, 19700, -170.0F, -1750.0F, -975.0F, 35, 25, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 41100, 20200, -170.0F, -1750.0F, -925.0F, 35, 25, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 41300, 19700, -170.0F, -1735.0F, -875.0F, 35, 25, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 39500, 43000, -170.0F, -1785.0F, -875.0F, 35, 50, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 41300, 19700, -170.0F, -1735.0F, -825.0F, 35, 25, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 35500, 25100, -170.0F, -1775.0F, -825.0F, 35, 40, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 41800, 21100, -170.0F, -1780.0F, -825.0F, 35, 5, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 39900, 19500, -217.5F, -1725.0F, -400.0F, 135, 25, 120, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 19700, 13200, -192.5F, -1750.0F, -375.0F, 85, 25, 70, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 32000, 19900, -192.5F, -1825.0F, -375.0F, 85, 25, 70, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 22700, 10600, -192.5F, -1775.0F, -375.0F, 85, 25, 70, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 19700, 13200, -192.5F, -1800.0F, -375.0F, 85, 25, 70, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 39900, 19500, -217.5F, -1725.0F, -700.0F, 135, 25, 245, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 40300, 23600, 105.0F, -1725.0F, -975.0F, 85, 25, 85, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 40300, 23600, 105.0F, -1725.0F, -825.0F, 85, 25, 85, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 43500, 23800, 130.0F, -1750.0F, -950.0F, 35, 25, 35, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 43500, 23800, 130.0F, -1750.0F, -800.0F, 35, 25, 35, 0.0F, false));

		panelfour = new RendererModel(this);
		panelfour.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfour, 0.0F, -1.0472F, 0.0F);
		controls.addChild(panelfour);
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, -105.0F, -1849.5F, 600.0F, 100, 47, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, 125.0F, -1799.5F, 582.5F, 50, 47, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, 70.0F, -1799.5F, 582.5F, 50, 47, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, 70.0F, -1742.0F, 582.5F, 50, 47, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, 125.0F, -1742.0F, 582.5F, 50, 47, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 45500, 22400, 82.5F, -1787.0F, 587.5F, 25, 22, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 45500, 22400, 137.5F, -1787.0F, 587.5F, 25, 22, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 45500, 22400, 137.5F, -1729.5F, 587.5F, 25, 22, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 45500, 22400, 82.5F, -1729.5F, 587.5F, 25, 22, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, 0.0F, -1849.5F, 600.0F, 50, 47, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, 0.0F, -1679.5F, 600.0F, 50, 47, 25, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 46000, 16200, -105.0F, -1679.5F, 600.0F, 100, 47, 25, 0.0F, false));

		panelfour2 = new RendererModel(this);
		panelfour2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfour2, 0.0F, -1.0472F, 0.0F);
		controls.addChild(panelfour2);
		panelfour2.cubeList.add(new ModelBox(panelfour2, 39100, 23600, 0.0F, -1802.0F, 600.0F, 50, 122, 25, 0.0F, false));
		panelfour2.cubeList.add(new ModelBox(panelfour2, 39100, 23600, -105.0F, -1802.0F, 600.0F, 100, 122, 25, 0.0F, false));

		panelfive = new RendererModel(this);
		panelfive.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfive, 0.0F, -0.5236F, 0.0F);
		controls.addChild(panelfive);
		panelfive.cubeList.add(new ModelBox(panelfive, 18400, 43500, -975.0F, -1425.0F, -35.0F, 75, 100, 75, 0.0F, false));

		panelfive2 = new RendererModel(this);
		panelfive2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfive2, 0.0F, -0.5236F, 0.0F);
		controls.addChild(panelfive2);
		panelfive2.cubeList.add(new ModelBox(panelfive2, 39300, 21900, -975.0F, -1525.0F, -35.0F, 75, 100, 75, 0.0F, false));

		bone120 = new RendererModel(this);
		bone120.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone120, -0.2618F, -1.5708F, 0.0F);
		panelfive2.addChild(bone120);
		bone120.cubeList.add(new ModelBox(bone120, 36100, 21600, -25.0F, -1702.5F, 450.0F, 50, 50, 150, 0.0F, false));

		panelfive3 = new RendererModel(this);
		panelfive3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfive3, 0.0F, -1.5708F, 0.0F);
		controls.addChild(panelfive3);
		panelfive3.cubeList.add(new ModelBox(panelfive3, 32700, 38600, -1000.0F, -1525.0F, -60.0F, 125, 100, 125, 0.0F, false));
		panelfive3.cubeList.add(new ModelBox(panelfive3, 32700, 38600, -975.0F, -1550.0F, -35.0F, 75, 225, 75, 0.0F, false));

		bone121 = new RendererModel(this);
		bone121.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone121, -0.2618F, -1.5708F, 0.0F);
		panelfive3.addChild(bone121);

		panelsix = new RendererModel(this);
		panelsix.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelsix, -0.3491F, 3.1416F, 0.0F);
		controls.addChild(panelsix);
		panelsix.cubeList.add(new ModelBox(panelsix, 35400, 19200, -75.0F, -1637.5F, 317.5F, 150, 150, 150, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 21600, 22400, 350.0F, -1700.0F, 400.0F, 100, 100, 100, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 35400, 19200, -350.0F, -1637.5F, 717.5F, 100, 150, 50, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 35400, 19200, -250.0F, -1637.5F, 592.5F, 50, 150, 100, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 35400, 19200, -350.0F, -1637.5F, 642.5F, 75, 150, 50, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 35400, 19200, 175.0F, -1637.5F, 667.5F, 100, 150, 100, 0.0F, false));

		wibbly = new RendererModel(this);
		wibbly.setRotationPoint(400.0F, -1700.0F, 400.0F);
		panelsix.addChild(wibbly);
		wibbly.cubeList.add(new ModelBox(wibbly, 41600, 18700, -25.0F, -25.0F, 50.0F, 50, 25, 175, 0.0F, false));
		wibbly.cubeList.add(new ModelBox(wibbly, 38800, 23300, -25.0F, -25.0F, 0.0F, 50, 75, 50, 0.0F, false));
		wibbly.cubeList.add(new ModelBox(wibbly, 41600, 24800, -25.0F, -25.0F, 225.0F, 50, 25, 175, 0.0F, false));

		pumps = new RendererModel(this);
		pumps.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(pumps);

		pump = new RendererModel(this);
		pump.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump, 0.3491F, -0.5236F, 0.0F);
		pumps.addChild(pump);
		pump.cubeList.add(new ModelBox(pump, 36700, 9400, -75.0F, -1775.0F, -750.0F, 150, 175, 400, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 46000, 19000, -37.5F, -1742.5F, -825.0F, 75, 75, 125, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 39500, 28800, -62.5F, -1767.5F, -875.0F, 125, 125, 50, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 47800, 22200, -37.5F, -1742.5F, -900.0F, 75, 75, 25, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 46000, 19000, -87.5F, -1792.5F, -375.0F, 175, 150, 50, 0.0F, false));

		pump2 = new RendererModel(this);
		pump2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump2, 0.3491F, -1.5708F, 0.0F);
		pumps.addChild(pump2);
		pump2.cubeList.add(new ModelBox(pump2, 36700, 9400, -75.0F, -1775.0F, -750.0F, 150, 175, 400, 0.0F, false));
		pump2.cubeList.add(new ModelBox(pump2, 46000, 19000, -37.5F, -1742.5F, -825.0F, 75, 75, 125, 0.0F, false));
		pump2.cubeList.add(new ModelBox(pump2, 39500, 28800, -62.5F, -1767.5F, -875.0F, 125, 125, 50, 0.0F, false));
		pump2.cubeList.add(new ModelBox(pump2, 47800, 22200, -37.5F, -1742.5F, -900.0F, 75, 75, 25, 0.0F, false));
		pump2.cubeList.add(new ModelBox(pump2, 46000, 19000, -87.5F, -1792.5F, -375.0F, 175, 150, 50, 0.0F, false));

		pump3 = new RendererModel(this);
		pump3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump3, 0.3491F, -2.618F, 0.0F);
		pumps.addChild(pump3);
		pump3.cubeList.add(new ModelBox(pump3, 36700, 9400, -75.0F, -1775.0F, -750.0F, 150, 175, 400, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 46000, 19000, -37.5F, -1742.5F, -825.0F, 75, 75, 125, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 39500, 28800, -62.5F, -1767.5F, -875.0F, 125, 125, 50, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 47800, 22200, -37.5F, -1742.5F, -900.0F, 75, 75, 25, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 46000, 19000, -87.5F, -1792.5F, -375.0F, 175, 150, 50, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 39200, 23700, -207.5F, -1725.0F, -675.0F, 125, 175, 250, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 34700, 33400, -192.5F, -1745.0F, -540.0F, 100, 25, 100, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 34700, 33400, -192.5F, -1745.0F, -660.0F, 100, 25, 100, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 48500, 24100, -187.5F, -1720.0F, -860.0F, 75, 25, 75, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 24600, 13400, 92.5F, -1725.0F, -300.0F, 125, 175, 125, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 33900, 43000, 117.5F, -1775.0F, -275.0F, 75, 125, 75, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 33900, 43000, 130.0F, -1800.0F, -260.0F, 50, 25, 50, 0.0F, false));

		redlever = new RendererModel(this);
		redlever.setRotationPoint(0.0F, 0.0F, 0.0F);
		pump3.addChild(redlever);
		redlever.cubeList.add(new ModelBox(redlever, 41800, 18800, 92.5F, -1775.0F, -675.0F, 125, 225, 250, 0.0F, false));
		redlever.cubeList.add(new ModelBox(redlever, 41800, 18800, 105.0F, -1800.0F, -625.0F, 50, 25, 125, 0.0F, false));

		bigboy = new RendererModel(this);
		bigboy.setRotationPoint(150.0F, -1750.0F, -550.0F);
		setRotationAngle(bigboy, -0.6981F, 0.0F, 0.0F);
		redlever.addChild(bigboy);
		bigboy.cubeList.add(new ModelBox(bigboy, 43000, 50000, -45.0F, -162.955F, -28.797F, 50, 25, 25, 0.0F, false));
		bigboy.cubeList.add(new ModelBox(bigboy, 41800, 18800, -40.0F, -137.955F, -28.797F, 12, 120, 25, 0.0F, false));
		bigboy.cubeList.add(new ModelBox(bigboy, 43000, 50000, -70.0F, -187.955F, -28.797F, 100, 25, 25, 0.0F, false));
		bigboy.cubeList.add(new ModelBox(bigboy, 41800, 18800, -12.5F, -137.955F, -28.797F, 12, 120, 25, 0.0F, false));

		blackboy3 = new RendererModel(this);
		blackboy3.setRotationPoint(175.0F, -1675.0F, -525.0F);
		setRotationAngle(blackboy3, 0.6981F, 0.0F, 0.0F);
		redlever.addChild(blackboy3);
		blackboy3.cubeList.add(new ModelBox(blackboy3, 43900, 29500, -20.0F, -179.412F, 0.504F, 25, 25, 25, 0.0F, false));
		blackboy3.cubeList.add(new ModelBox(blackboy3, 41800, 18800, -15.0F, -154.412F, 0.504F, 22, 120, 25, 0.0F, false));
		blackboy3.cubeList.add(new ModelBox(blackboy3, 43400, 30600, -45.0F, -204.412F, 0.504F, 100, 25, 25, 0.0F, false));

		pump4 = new RendererModel(this);
		pump4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump4, 0.3491F, 0.5236F, 0.0F);
		pumps.addChild(pump4);
		pump4.cubeList.add(new ModelBox(pump4, 36700, 9400, -75.0F, -1775.0F, -750.0F, 150, 175, 400, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 46000, 19000, -37.5F, -1742.5F, -825.0F, 75, 75, 125, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 39500, 28800, -62.5F, -1767.5F, -875.0F, 125, 125, 50, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 47800, 22200, -37.5F, -1742.5F, -900.0F, 75, 75, 25, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 46000, 19000, -87.5F, -1792.5F, -375.0F, 175, 150, 50, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 24600, 12500, -200.0F, -1825.0F, -475.0F, 100, 75, 175, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 41100, 21100, -200.0F, -1825.0F, -575.0F, 100, 75, 100, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 41100, 21100, -175.0F, -1775.0F, -700.0F, 50, 50, 150, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 41100, 21100, -200.0F, -1825.0F, -300.0F, 100, 75, 25, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 39600, 25600, -200.0F, -1825.0F, -275.0F, 100, 75, 150, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 41100, 21100, -175.0F, -1750.0F, -350.0F, 50, 75, 50, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 41100, 21100, -175.0F, -1750.0F, -450.0F, 50, 75, 50, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 45700, 22800, -175.0F, -1800.0F, -125.0F, 50, 25, 500, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 41100, 21100, -175.0F, -1750.0F, -700.0F, 50, 75, 50, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 42800, 24100, -200.0F, -1725.0F, -975.0F, 100, 75, 100, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 42800, 24100, 100.0F, -1725.0F, -975.0F, 100, 75, 100, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 42500, 21600, 100.0F, -1725.0F, -400.0F, 100, 75, 100, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 44800, 19400, 100.0F, -1750.0F, -250.0F, 100, 100, 100, 0.0F, false));

		pump5 = new RendererModel(this);
		pump5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump5, 0.3491F, 1.5708F, 0.0F);
		pumps.addChild(pump5);
		pump5.cubeList.add(new ModelBox(pump5, 36700, 9400, -75.0F, -1775.0F, -750.0F, 150, 175, 400, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 46000, 19000, -37.5F, -1742.5F, -825.0F, 75, 75, 125, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 39500, 28800, -62.5F, -1767.5F, -875.0F, 125, 125, 50, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 47800, 22200, -37.5F, -1742.5F, -900.0F, 75, 75, 25, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 46000, 19000, -87.5F, -1792.5F, -375.0F, 175, 150, 50, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 19900, 11000, -127.5F, -1722.5F, -477.5F, 32, 25, 137, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 22100, 9800, -160.0F, -1722.5F, -577.5F, 22, 25, 337, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 19900, 11000, -202.5F, -1722.5F, -477.5F, 32, 25, 137, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 19900, 11000, -170.0F, -1732.5F, -800.0F, 50, 25, 100, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 19900, 11000, -157.5F, -1732.5F, -890.0F, 25, 25, 200, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 19900, 11000, -195.0F, -1732.5F, -700.0F, 25, 25, 25, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 19900, 11000, -120.0F, -1732.5F, -700.0F, 25, 25, 25, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 39300, 24100, -157.5F, -1737.5F, -890.0F, 25, 5, 25, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 24300, 12300, -207.5F, -1725.0F, -900.0F, 125, 175, 250, 0.0F, false));

		redlever2 = new RendererModel(this);
		redlever2.setRotationPoint(0.0F, 0.0F, 0.0F);
		pump5.addChild(redlever2);
		redlever2.cubeList.add(new ModelBox(redlever2, 41800, 18800, 92.5F, -1775.0F, -675.0F, 125, 225, 250, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 41800, 18800, 105.0F, -1800.0F, -625.0F, 50, 25, 125, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 41800, 18800, 92.5F, -1725.0F, -325.0F, 125, 175, 125, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 41800, 18800, 117.5F, -1750.0F, -300.0F, 75, 25, 75, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 43800, 16200, 117.5F, -1750.0F, -300.0F, 75, 0, 75, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 43000, 22800, 92.5F, -1725.0F, -925.0F, 125, 175, 125, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 41800, 18800, 117.5F, -1800.0F, -875.0F, 75, 75, 25, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 41800, 18800, 142.5F, -1800.0F, -850.0F, 25, 75, 25, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 41800, 18800, 142.5F, -1800.0F, -900.0F, 25, 75, 25, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 43000, 22800, 117.5F, -1812.0F, -900.0F, 75, 12, 75, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 32400, 51200, 130.0F, -1837.0F, -890.0F, 50, 37, 50, 0.0F, false));

		bigreder = new RendererModel(this);
		bigreder.setRotationPoint(125.0F, -1700.0F, -550.0F);
		setRotationAngle(bigreder, -0.6981F, 0.0F, 0.0F);
		redlever2.addChild(bigreder);
		bigreder.cubeList.add(new ModelBox(bigreder, 43000, 50000, -20.0F, -201.258F, -60.937F, 50, 25, 25, 0.0F, false));
		bigreder.cubeList.add(new ModelBox(bigreder, 41800, 18800, -15.0F, -176.258F, -60.937F, 12, 120, 25, 0.0F, false));
		bigreder.cubeList.add(new ModelBox(bigreder, 43000, 50000, -45.0F, -226.258F, -60.937F, 100, 25, 25, 0.0F, false));
		bigreder.cubeList.add(new ModelBox(bigreder, 41800, 18800, 12.5F, -176.258F, -60.937F, 12, 120, 25, 0.0F, false));

		smallblack = new RendererModel(this);
		smallblack.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(smallblack, 0.6981F, 0.0F, 0.0F);
		redlever2.addChild(smallblack);
		smallblack.cubeList.add(new ModelBox(smallblack, 43900, 29500, 155.0F, -1800.0F, 675.0F, 25, 25, 25, 0.0F, false));
		smallblack.cubeList.add(new ModelBox(smallblack, 41800, 18800, 160.0F, -1775.0F, 675.0F, 22, 120, 25, 0.0F, false));
		smallblack.cubeList.add(new ModelBox(smallblack, 43400, 30600, 130.0F, -1825.0F, 675.0F, 100, 25, 25, 0.0F, false));

		falsepump = new RendererModel(this);
		falsepump.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(falsepump, 0.3491F, 2.618F, 0.0F);
		pumps.addChild(falsepump);
		falsepump.cubeList.add(new ModelBox(falsepump, 41100, 20600, -57.5F, -1692.5F, -450.0F, 75, 125, 75, 0.0F, false));
		falsepump.cubeList.add(new ModelBox(falsepump, 41100, 20600, 12.5F, -1717.5F, -507.5F, 50, 125, 50, 0.0F, false));

		bone112 = new RendererModel(this);
		bone112.setRotationPoint(-50.0F, -1650.0F, -700.0F);
		setRotationAngle(bone112, 0.0F, 0.3491F, 0.0F);
		falsepump.addChild(bone112);
		bone112.cubeList.add(new ModelBox(bone112, 41100, 20600, 27.5F, -42.5F, -100.0F, 75, 125, 125, 0.0F, false));

		rotorframe = new RendererModel(this);
		rotorframe.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(rotorframe);

		bone110 = new RendererModel(this);
		bone110.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone110, 0.0175F, 0.0F, 0.0F);
		rotorframe.addChild(bone110);
		bone110.cubeList.add(new ModelBox(bone110, 26800, 9800, -200.0F, -6075.0F, -350.0F, 400, 4300, 100, 0.0F, false));

		bone111 = new RendererModel(this);
		bone111.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone111, -0.0175F, 0.0F, 0.0F);
		rotorframe.addChild(bone111);
		bone111.cubeList.add(new ModelBox(bone111, 26800, 9800, -200.0F, -6075.0F, 225.0F, 400, 4300, 100, 0.0F, false));

		unit = new RendererModel(this);
		unit.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(unit);

		console2 = new RendererModel(this);
		console2.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(console2);

		model = new RendererModel(this);
		model.setRotationPoint(0.0F, 0.0F, 0.0F);
		console2.addChild(model);

		console = new RendererModel(this);
		console.setRotationPoint(0.0F, -1100.0F, 0.0F);
		model.addChild(console);

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(frame);

		upper = new RendererModel(this);
		upper.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(upper);

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone4, 0.0F, 0.0F, -1.2217F);
		upper.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 21712, 22855, 529.523F, 150.652F, -225.0F, 150, 1300, 150, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 21712, 22855, 529.523F, 150.652F, 75.0F, 150, 1300, 150, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 26855, 23655, 454.523F, 200.652F, -150.0F, 150, 1200, 250, 0.0F, false));

		monster2 = new RendererModel(this);
		monster2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(monster2);

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone5, 0.0F, 0.0F, 1.2217F);
		upper.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 21598, 20912, -679.523F, 150.652F, 75.0F, 150, 1300, 150, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 22398, 22741, -679.523F, 150.652F, -225.0F, 150, 1300, 150, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 4800, 16300, -604.523F, 200.652F, -75.0F, 150, 1200, 150, 0.0F, false));

		monster = new RendererModel(this);
		monster.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(monster, 0.0F, 3.1416F, 0.0F);
		bone5.addChild(monster);

		upper2 = new RendererModel(this);
		upper2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper2, 0.0F, -1.0472F, 0.0F);
		frame.addChild(upper2);

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone6, 0.0F, 0.0F, -1.2217F);
		upper2.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 21712, 23541, 529.523F, 150.652F, -225.0F, 150, 1300, 150, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 5400, 16300, 454.523F, 200.652F, -150.0F, 150, 1200, 250, 0.0F, false));

		bone59 = new RendererModel(this);
		bone59.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone59, 0.0F, 0.0F, -1.2217F);
		upper2.addChild(bone59);
		bone59.cubeList.add(new ModelBox(bone59, 4800, 16300, 454.523F, 200.652F, -150.0F, 150, 1200, 250, 0.0F, false));

		yeeeet = new RendererModel(this);
		yeeeet.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(yeeeet, 0.0F, 0.0F, -1.2217F);
		upper2.addChild(yeeeet);
		yeeeet.cubeList.add(new ModelBox(yeeeet, 21598, 20912, 529.523F, 150.652F, 75.0F, 150, 1300, 150, 0.0F, false));
		yeeeet.cubeList.add(new ModelBox(yeeeet, 27600, 22600, 454.523F, 200.652F, -150.0F, 150, 1200, 250, 0.0F, false));

		bone71 = new RendererModel(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		yeeeet.addChild(bone71);

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(0.0F, 0.0F, 0.0F);
		yeeeet.addChild(bone57);

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone58, 0.0F, 3.1416F, -1.2217F);
		upper2.addChild(bone58);

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone7, 0.0F, 0.0F, 1.2217F);
		upper2.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 22398, 22741, -679.523F, 150.652F, 75.0F, 150, 1300, 150, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 22398, 22741, -679.523F, 150.652F, -225.0F, 150, 1300, 150, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 4800, 16300, -604.523F, 200.652F, -75.0F, 150, 1200, 150, 0.0F, false));

		bone72 = new RendererModel(this);
		bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone72, 0.0F, 3.1416F, 0.0F);
		bone7.addChild(bone72);

		upper3 = new RendererModel(this);
		upper3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper3, 0.0F, -2.0944F, 0.0F);
		frame.addChild(upper3);

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone8, 0.0F, 0.0F, -1.2217F);
		upper3.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 21598, 20912, 529.523F, 150.652F, -225.0F, 150, 1300, 150, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 21598, 20912, 529.523F, 150.652F, 75.0F, 150, 1300, 150, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 27312, 19883, 454.523F, 300.652F, -150.0F, 150, 1100, 250, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone9, 0.0F, 0.0F, 1.2217F);
		upper3.addChild(bone9);
		bone9.cubeList.add(new ModelBox(bone9, 22512, 21026, -679.523F, 150.652F, -225.0F, 150, 1300, 150, 0.0F, false));
		bone9.cubeList.add(new ModelBox(bone9, 22398, 22741, -679.523F, 150.652F, 75.0F, 150, 1300, 150, 0.0F, false));
		bone9.cubeList.add(new ModelBox(bone9, 4800, 16300, -604.523F, 200.652F, -75.0F, 150, 1200, 150, 0.0F, false));

		bone70 = new RendererModel(this);
		bone70.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone70, 0.0F, 3.1416F, 1.2217F);
		upper3.addChild(bone70);

		upp = new RendererModel(this);
		upp.setRotationPoint(0.0F, 25.0F, 0.0F);
		frame.addChild(upp);

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		upp.addChild(bone11);

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone12, 0.0F, -1.0472F, 0.0F);
		upp.addChild(bone12);

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone13, 0.0F, -2.0944F, 0.0F);
		upp.addChild(bone13);

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone14, -0.3491F, 0.0F, 0.0F);
		frame.addChild(bone14);
		bone14.cubeList.add(new ModelBox(bone14, 43700, 8400, -575.0F, -600.0F, 1087.5F, 1150, 150, 150, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 1600, 26000, -125.0F, -600.0F, 187.5F, 250, 150, 250, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 45200, 2700, -400.0F, -600.0F, 787.5F, 800, 150, 150, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 40800, 300, -275.0F, -600.0F, 537.5F, 550, 150, 100, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 41700, 1600, -325.0F, -600.0F, 637.5F, 650, 150, 150, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 21598, 20912, -225.0F, -651.508F, 446.051F, 450, 150, 100, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 44100, 4100, -500.0F, -600.0F, 937.5F, 1000, 150, 150, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 3000, 25800, -125.0F, -651.508F, 346.051F, 250, 150, 100, 0.0F, false));

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone15, -0.3491F, 3.1416F, 0.0F);
		frame.addChild(bone15);
		bone15.cubeList.add(new ModelBox(bone15, 44100, 4100, -475.0F, -600.0F, 937.5F, 950, 150, 150, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 800, 26000, -125.0F, -600.0F, 187.5F, 250, 150, 250, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 45200, 2700, -400.0F, -600.0F, 787.5F, 800, 150, 150, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 40800, 300, -275.0F, -600.0F, 537.5F, 550, 150, 100, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 41700, 1600, -325.0F, -600.0F, 637.5F, 650, 150, 150, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 22398, 22741, -225.0F, -651.508F, 446.051F, 450, 150, 100, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 43700, 8400, -575.0F, -600.0F, 1087.5F, 1150, 150, 125, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 3000, 25800, -125.0F, -651.508F, 346.051F, 250, 150, 100, 0.0F, false));

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone16, -0.3491F, -1.0472F, 0.0F);
		frame.addChild(bone16);
		bone16.cubeList.add(new ModelBox(bone16, 43700, 8400, -575.0F, -600.0F, 1087.5F, 1150, 150, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 0, 26000, -125.0F, -600.0F, 187.5F, 250, 150, 250, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 45200, 2700, -400.0F, -600.0F, 787.5F, 800, 150, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 40800, 300, -275.0F, -600.0F, 537.5F, 550, 150, 100, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 41700, 1600, -325.0F, -600.0F, 637.5F, 650, 150, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 20900, 25400, -225.0F, -651.508F, 446.051F, 450, 150, 100, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 44100, 4100, -475.0F, -600.0F, 937.5F, 950, 150, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 20900, 25400, -225.0F, -651.508F, 371.051F, 450, 150, 100, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 35200, 16700, -150.0F, -650.0F, 737.5F, 300, 150, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 19600, 11000, -175.0F, -650.0F, 737.5F, 25, 150, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 19600, 11000, -175.0F, -650.0F, 837.5F, 25, 150, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 19600, 11000, 150.0F, -650.0F, 837.5F, 25, 150, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 19600, 11000, 150.0F, -650.0F, 737.5F, 25, 150, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 16000, 39600, -225.0F, -625.0F, 1037.5F, 300, 50, 275, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 16000, 39600, -225.0F, -700.0F, 1037.5F, 300, 75, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 16200, 49700, -175.0F, -725.0F, 962.5F, 200, 75, 75, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, -165.0F, -727.5F, 980.0F, 175, 25, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, -215.0F, -627.5F, 1205.0F, 275, 25, 100, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 44000, 21100, 25.0F, -702.5F, 1005.0F, 25, 75, 107, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 44000, 21100, -150.0F, -702.5F, 1030.0F, 175, 75, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, -140.0F, -702.5F, 1080.0F, 125, 25, 75, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, -190.0F, -702.5F, 1130.0F, 50, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, -15.0F, -702.5F, 1130.0F, 50, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 37100, 23100, 200.0F, -625.0F, 1062.5F, 100, 75, 100, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 37100, 23100, 225.0F, -650.0F, 1087.5F, 50, 25, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 22600, 13500, 237.5F, -660.0F, 1100.0F, 25, 5, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 37600, 20900, 225.0F, -655.0F, 1087.5F, 50, 5, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, 60.0F, -652.5F, 780.0F, 75, 25, 75, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, -140.0F, -652.5F, 780.0F, 75, 25, 75, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36100, 24600, -65.0F, -652.5F, 805.0F, 125, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 32000, 18900, 245.0F, -700.0F, 662.5F, 137, 125, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 25800, 14200, 250.0F, -700.0F, 812.5F, 125, 125, 50, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36600, 20600, 325.0F, -705.0F, 687.5F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 300.0F, -705.0F, 687.5F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36600, 20600, 275.0F, -705.0F, 687.5F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 250.0F, -705.0F, 687.5F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 350.0F, -705.0F, 702.5F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36600, 20600, 325.0F, -705.0F, 725.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 300.0F, -705.0F, 725.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36600, 20600, 275.0F, -705.0F, 725.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 250.0F, -705.0F, 725.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 350.0F, -705.0F, 740.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36600, 20600, 325.0F, -705.0F, 760.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 300.0F, -705.0F, 760.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 36600, 20600, 275.0F, -705.0F, 760.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 250.0F, -705.0F, 760.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 39300, 20400, 350.0F, -705.0F, 775.0F, 25, 25, 25, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 33400, 20100, 220.0F, -675.0F, 687.5F, 37, 125, 150, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 32000, 18900, 370.0F, -700.0F, 737.5F, 37, 125, 75, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 32000, 18900, 370.0F, -700.0F, 737.5F, 37, 125, 75, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 25800, 14200, 250.0F, -675.0F, 862.5F, 125, 100, 75, 0.0F, false));

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone18, -0.3491F, 1.0472F, 0.0F);
		frame.addChild(bone18);
		bone18.cubeList.add(new ModelBox(bone18, 43700, 8400, -575.0F, -600.0F, 1087.5F, 1150, 150, 150, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 2400, 26000, -125.0F, -600.0F, 187.5F, 250, 150, 250, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 45200, 2700, -400.0F, -600.0F, 787.5F, 800, 150, 150, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 40800, 300, -275.0F, -600.0F, 537.5F, 550, 150, 100, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 41700, 1600, -325.0F, -600.0F, 637.5F, 650, 150, 150, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 21712, 23541, -225.0F, -651.508F, 446.051F, 450, 150, 100, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 44100, 4100, -475.0F, -600.0F, 937.5F, 950, 150, 150, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 3000, 25800, -125.0F, -651.508F, 346.051F, 250, 150, 100, 0.0F, false));

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone19, -0.3491F, 2.0944F, 0.0F);
		frame.addChild(bone19);
		bone19.cubeList.add(new ModelBox(bone19, 8600, 26200, -125.0F, -600.0F, 187.5F, 250, 150, 150, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 45200, 2700, -400.0F, -600.0F, 787.5F, 800, 150, 150, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 4000, 25400, -175.0F, -600.0F, 337.5F, 350, 150, 200, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 41700, 1600, -325.0F, -600.0F, 637.5F, 650, 150, 150, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 40800, 300, -275.0F, -600.0F, 537.5F, 550, 150, 100, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 21712, 23541, -225.0F, -651.508F, 446.051F, 450, 150, 100, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 43700, 8400, -575.0F, -600.0F, 1087.5F, 1150, 150, 150, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 44100, 4100, -475.0F, -600.0F, 937.5F, 950, 150, 150, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 3000, 25800, -125.0F, -651.508F, 346.051F, 250, 150, 100, 0.0F, false));

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone17, -0.3491F, -2.0944F, 0.0F);
		frame.addChild(bone17);
		bone17.cubeList.add(new ModelBox(bone17, 43700, 8400, -575.0F, -600.0F, 1087.5F, 1150, 150, 150, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 9200, 26000, -125.0F, -600.0F, 187.5F, 250, 150, 150, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 45200, 2700, -400.0F, -600.0F, 787.5F, 800, 150, 150, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 40800, 300, -275.0F, -600.0F, 537.5F, 550, 150, 100, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 41700, 1600, -325.0F, -600.0F, 637.5F, 650, 150, 150, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 3200, 26000, -175.0F, -600.0F, 337.5F, 350, 150, 100, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 20900, 25400, -225.0F, -651.508F, 446.051F, 450, 150, 100, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 44100, 4100, -475.0F, -600.0F, 937.5F, 950, 150, 150, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 20900, 25400, -125.0F, -651.508F, 346.051F, 250, 150, 100, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, -62.5F, -625.0F, 987.5F, 125, 150, 100, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, -27.5F, -625.0F, 1087.5F, 50, 150, 50, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, -87.5F, -625.0F, 1062.5F, 50, 150, 50, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, -87.5F, -625.0F, 962.5F, 50, 150, 50, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, 37.5F, -625.0F, 1062.5F, 50, 150, 50, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, 37.5F, -625.0F, 962.5F, 50, 150, 50, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, -27.5F, -625.0F, 937.5F, 50, 150, 50, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 35400, 19200, -37.5F, -627.5F, 1000.0F, 75, 150, 75, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 37100, 21900, 200.0F, -620.0F, 1050.0F, 100, 150, 100, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 37100, 21900, 225.0F, -645.0F, 1075.0F, 50, 25, 50, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 35400, 19200, -75.0F, -602.5F, 700.0F, 150, 150, 150, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 33900, 39300, -325.0F, -645.0F, 1125.0F, 50, 25, 50, 0.0F, false));

		frame2 = new RendererModel(this);
		frame2.setRotationPoint(0.0F, -150.0F, 0.0F);
		setRotationAngle(frame2, 0.0F, 0.0F, 3.1416F);
		console.addChild(frame2);

		upper4 = new RendererModel(this);
		upper4.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame2.addChild(upper4);

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone10, 0.0F, 0.0F, -1.2217F);
		upper4.addChild(bone10);

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone20, 0.0F, 0.0F, 1.2217F);
		upper4.addChild(bone20);

		upper5 = new RendererModel(this);
		upper5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper5, 0.0F, -1.0472F, 0.0F);
		frame2.addChild(upper5);

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone21, 0.0F, 0.0F, -1.2217F);
		upper5.addChild(bone21);

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone22, 0.0F, 0.0F, 1.2217F);
		upper5.addChild(bone22);

		upper6 = new RendererModel(this);
		upper6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper6, 0.0F, -2.0944F, 0.0F);
		frame2.addChild(upper6);

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone23, 0.0F, 0.0F, -1.2217F);
		upper6.addChild(bone23);

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone24, 0.0F, 0.0F, 1.2217F);
		upper6.addChild(bone24);

		upp2 = new RendererModel(this);
		upp2.setRotationPoint(0.0F, 25.0F, 0.0F);
		frame2.addChild(upp2);

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
		upp2.addChild(bone25);

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone26, 0.0F, -1.0472F, 0.0F);
		upp2.addChild(bone26);

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone27, 0.0F, -2.0944F, 0.0F);
		upp2.addChild(bone27);

		upp3 = new RendererModel(this);
		upp3.setRotationPoint(0.0F, -600.0F, 0.0F);
		frame2.addChild(upp3);

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.0F, 0.0F, 0.0F);
		upp3.addChild(bone39);

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone40, 0.0F, -1.0472F, 0.0F);
		upp3.addChild(bone40);

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone41, 0.0F, -2.0944F, 0.0F);
		upp3.addChild(bone41);

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone28, -0.3491F, 0.0F, 0.0F);
		frame2.addChild(bone28);

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone29, -0.3491F, 3.1416F, 0.0F);
		frame2.addChild(bone29);

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone30, -0.3491F, -1.0472F, 0.0F);
		frame2.addChild(bone30);

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone31, -0.3491F, 1.0472F, 0.0F);
		frame2.addChild(bone31);

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone32, -0.3491F, 2.0944F, 0.0F);
		frame2.addChild(bone32);

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone33, -0.3491F, -2.0944F, 0.0F);
		frame2.addChild(bone33);

		cubes3 = new RendererModel(this);
		cubes3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(cubes3, 0.0F, -2.0944F, 0.0F);
		console.addChild(cubes3);

		cubes2 = new RendererModel(this);
		cubes2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(cubes2, 0.0F, -1.0472F, 0.0F);
		console.addChild(cubes2);

		cubes = new RendererModel(this);
		cubes.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(cubes);

		outerframe = new RendererModel(this);
		outerframe.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(outerframe);

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		outerframe.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 35700, 26300, -650.0F, -150.0F, 1350.0F, 1300, 275, 50, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 35700, 26300, -650.0F, -150.0F, -1375.0F, 1300, 275, 50, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 22100, 23500, -200.0F, -725.0F, -600.0F, 400, 275, 150, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 22100, 20600, 100.0F, -800.0F, -600.0F, 100, 75, 150, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 22600, 22400, -100.0F, -775.0F, -600.0F, 200, 50, 150, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 22100, 20600, -200.0F, -800.0F, -600.0F, 100, 75, 150, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38400, 24100, -177.5F, -700.0F, -625.0F, 75, 125, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38400, 24100, -85.0F, -700.0F, -625.0F, 75, 125, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38400, 24100, 10.0F, -700.0F, -625.0F, 75, 125, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38400, 24100, 102.5F, -700.0F, -625.0F, 75, 125, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 42800, 17700, -177.5F, -675.0F, -627.5F, 75, 50, 0, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 42800, 17700, -85.0F, -675.0F, -627.5F, 75, 50, 0, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 42800, 17700, 10.0F, -675.0F, -627.5F, 75, 50, 0, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 42800, 17700, 102.5F, -675.0F, -627.5F, 75, 50, 0, 0.0F, false));

		bone56 = new RendererModel(this);
		bone56.setRotationPoint(0.0F, -475.0F, 525.0F);
		setRotationAngle(bone56, 0.0873F, 0.0F, 0.0F);
		bone.addChild(bone56);
		bone56.cubeList.add(new ModelBox(bone56, 20900, 25400, -200.0F, -350.0F, -75.0F, 400, 450, 150, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 20900, 25400, -150.0F, -400.0F, -75.0F, 300, 50, 150, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 43900, 21300, 75.0F, -225.0F, 75.0F, 100, 100, 25, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 34600, 16700, -175.0F, -375.0F, 100.0F, 100, 300, 50, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 34600, 16700, 75.0F, -375.0F, 100.0F, 100, 300, 50, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 43900, 21300, -175.0F, -225.0F, 75.0F, 100, 100, 25, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 44600, 19700, -50.0F, -275.0F, 75.0F, 100, 150, 25, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 41300, 24600, 75.0F, -395.0F, 100.0F, 100, 20, 50, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 41300, 24600, -175.0F, -395.0F, 100.0F, 100, 20, 50, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 41300, 24600, 75.0F, -75.0F, 100.0F, 100, 20, 50, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 41300, 24600, -175.0F, -75.0F, 100.0F, 100, 20, 50, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		outerframe.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 35700, 26300, -650.0F, -150.0F, 1350.0F, 1300, 275, 50, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 35700, 26300, -625.0F, -150.0F, -1400.0F, 1250, 275, 50, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 22100, 23500, -200.0F, -725.0F, 450.0F, 400, 300, 150, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 22100, 20600, 100.0F, -800.0F, 450.0F, 100, 75, 150, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 22100, 20600, -100.0F, -775.0F, 450.0F, 200, 50, 150, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 22100, 20600, -200.0F, -800.0F, 450.0F, 100, 75, 150, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 45100, 22300, -175.0F, -75.0F, -1402.5F, 325, 100, 0, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(0.0F, -450.0F, -500.0F);
		setRotationAngle(bone54, -0.0873F, 0.0F, 0.0F);
		bone2.addChild(bone54);
		bone54.cubeList.add(new ModelBox(bone54, 20900, 25400, -200.0F, -375.0F, -100.0F, 400, 450, 150, 0.0F, false));
		bone54.cubeList.add(new ModelBox(bone54, 20900, 25400, -150.0F, -425.0F, -100.0F, 300, 150, 150, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone3, 0.0F, -2.0944F, 0.0F);
		outerframe.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 35700, 26300, -650.0F, -150.0F, 1350.0F, 1300, 275, 50, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 35700, 26300, -650.0F, -150.0F, -1400.0F, 1300, 275, 50, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 44600, 24400, -475.0F, -75.0F, -1425.0F, 100, 100, 50, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 22100, 20600, -200.0F, -800.0F, -600.0F, 100, 75, 150, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 22100, 20600, 100.0F, -800.0F, -600.0F, 100, 75, 150, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 22100, 20600, -100.0F, -775.0F, -600.0F, 200, 50, 150, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 22100, 23500, -200.0F, -725.0F, -600.0F, 400, 275, 150, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 44600, 24400, 375.0F, -75.0F, -1425.0F, 100, 100, 50, 0.0F, false));

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(0.0F, -475.0F, 525.0F);
		setRotationAngle(bone55, 0.0873F, 0.0F, 0.0F);
		bone3.addChild(bone55);
		bone55.cubeList.add(new ModelBox(bone55, 20900, 25400, -200.0F, -350.0F, -75.0F, 400, 450, 150, 0.0F, false));
		bone55.cubeList.add(new ModelBox(bone55, 20900, 25400, -150.0F, -400.0F, -75.0F, 300, 50, 150, 0.0F, false));
		bone55.cubeList.add(new ModelBox(bone55, 38400, 22400, -100.0F, -250.0F, 75.0F, 200, 125, 25, 0.0F, false));
		bone55.cubeList.add(new ModelBox(bone55, 38400, 22400, -25.0F, -250.0F, 100.0F, 50, 125, 25, 0.0F, false));

		bone119 = new RendererModel(this);
		bone119.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone119, -0.5236F, 0.0F, 0.0F);
		bone55.addChild(bone119);
		bone119.cubeList.add(new ModelBox(bone119, 39100, 16700, -125.0F, -275.0F, -25.0F, 250, 50, 175, 0.0F, false));
		bone119.cubeList.add(new ModelBox(bone119, 39100, 16700, -75.0F, -325.0F, 25.0F, 150, 50, 75, 0.0F, false));
		bone119.cubeList.add(new ModelBox(bone119, 38400, 22400, -50.0F, -332.5F, 40.0F, 100, 25, 50, 0.0F, false));

		hollowglow = new RendererModel(this);
		hollowglow.setRotationPoint(0.0F, 1100.0F, 0.0F);
		console.addChild(hollowglow);

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone36, 0.0F, -2.0944F, 0.0F);
		hollowglow.addChild(bone36);

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(0.0F, -1500.0F, 0.0F);
		setRotationAngle(bone35, 0.0F, -1.0472F, 0.0F);
		hollowglow.addChild(bone35);

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		hollowglow.addChild(bone34);

		ihaveexam = new RendererModel(this);
		ihaveexam.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(ihaveexam);

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone42, 0.0F, 1.5708F, 0.0F);
		ihaveexam.addChild(bone42);
		bone42.cubeList.add(new ModelBox(bone42, 22398, 22741, 75.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 21598, 20912, -225.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 39800, 23800, 100.0F, -1140.0F, -1620.0F, 100, 100, 50, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 34900, 20100, 125.0F, -1115.0F, -1670.0F, 50, 50, 50, 0.0F, false));

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone38, 0.0F, 0.5236F, 0.0F);
		ihaveexam.addChild(bone38);
		bone38.cubeList.add(new ModelBox(bone38, 22398, 22741, 75.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 22398, 22741, -225.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 27800, 12800, -225.0F, -1215.0F, -1695.0F, 150, 200, 100, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 42000, 18700, -225.0F, -1180.0F, -1820.0F, 150, 150, 125, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 40300, 23800, -175.0F, -1181.5F, -1820.0F, 50, 0, 125, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 40300, 23800, -200.0F, -1181.5F, -1820.0F, 25, 0, 25, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 40300, 23800, -125.0F, -1181.5F, -1770.0F, 25, 0, 25, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 40300, 23800, -125.0F, -1181.5F, -1720.0F, 25, 0, 25, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 40300, 23800, -200.0F, -1181.5F, -1720.0F, 25, 0, 25, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 40300, 23800, -200.0F, -1181.5F, -1770.0F, 25, 0, 25, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 51400, 16200, 175.0F, -1190.0F, -1620.0F, 25, 150, 50, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 51400, 16200, 100.0F, -1190.0F, -1620.0F, 25, 150, 50, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 51400, 16200, 150.0F, -1115.0F, -1620.0F, 25, 50, 50, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 51400, 16200, 125.0F, -1165.0F, -1620.0F, 25, 50, 50, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 51400, 16200, 100.0F, -1040.0F, -1620.0F, 100, 25, 50, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 51400, 16200, 100.0F, -1215.0F, -1620.0F, 100, 25, 50, 0.0F, false));

		rando = new RendererModel(this);
		rando.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone38.addChild(rando);
		rando.cubeList.add(new ModelBox(rando, 40600, 19900, -125.0F, -1205.0F, -1820.0F, 25, 25, 25, 0.0F, false));
		rando.cubeList.add(new ModelBox(rando, 39100, 25600, -125.0F, -1230.0F, -1820.0F, 25, 25, 25, 0.0F, false));

		yes = new RendererModel(this);
		yes.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(yes, 0.0F, -0.5236F, 0.0F);
		ihaveexam.addChild(yes);
		yes.cubeList.add(new ModelBox(yes, 22398, 22741, 75.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		yes.cubeList.add(new ModelBox(yes, 22398, 22741, -225.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));

		bone60 = new RendererModel(this);
		bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone60, -0.1745F, 0.0F, 0.0F);
		yes.addChild(bone60);

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone65, -0.1745F, 3.1416F, 0.0F);
		yes.addChild(bone65);

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone66, -0.1745F, -1.0472F, 0.0F);
		yes.addChild(bone66);

		bone67 = new RendererModel(this);
		bone67.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone67, -0.1745F, -2.0944F, 0.0F);
		yes.addChild(bone67);

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone68, -0.1745F, 1.0472F, 0.0F);
		yes.addChild(bone68);

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone69, -0.1745F, 2.0944F, 0.0F);
		yes.addChild(bone69);

		ihaveexam2 = new RendererModel(this);
		ihaveexam2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(ihaveexam2, 0.0F, 3.1416F, 0.0F);
		unit.addChild(ihaveexam2);

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone43, 0.0F, 1.5708F, 0.0F);
		ihaveexam2.addChild(bone43);
		bone43.cubeList.add(new ModelBox(bone43, 22283, 23083, 75.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 21712, 22855, -225.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));

		bone44 = new RendererModel(this);
		bone44.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone44, 0.0F, 0.5236F, 0.0F);
		ihaveexam2.addChild(bone44);
		bone44.cubeList.add(new ModelBox(bone44, 21598, 20912, 75.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 21826, 20569, -225.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 26500, 11000, -200.0F, -1165.0F, -1605.0F, 100, 100, 25, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 40600, 21600, 112.5F, -1152.5F, -1605.0F, 75, 75, 25, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 40600, 21600, 137.5F, -1152.5F, -1655.0F, 25, 75, 25, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 40600, 21600, 137.5F, -1102.5F, -1630.0F, 25, 25, 25, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 24800, 13200, -212.5F, -1177.5F, -1610.0F, 25, 25, 25, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 24800, 13200, -162.5F, -1065.0F, -1605.0F, 25, 25, 25, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 24800, 13200, -115.0F, -1177.5F, -1617.5F, 25, 25, 25, 0.0F, false));

		bone45 = new RendererModel(this);
		bone45.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone45, 0.0F, -0.5236F, 0.0F);
		ihaveexam2.addChild(bone45);
		bone45.cubeList.add(new ModelBox(bone45, 21598, 20912, 75.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 21598, 20912, -225.0F, -1240.0F, -1595.0F, 150, 250, 150, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 40300, 25300, -200.0F, -1165.0F, -1620.0F, 100, 100, 25, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 40300, 25300, -175.0F, -1140.0F, -1645.0F, 50, 50, 25, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 27800, 13200, 100.0F, -1115.0F, -1650.0F, 100, 50, 50, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 41800, 21400, 100.0F, -1190.0F, -1600.0F, 100, 125, 25, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 40800, 22100, 125.0F, -1165.0F, -1625.0F, 25, 50, 25, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 51400, 19900, 100.0F, -1140.0F, -1625.0F, 25, 25, 25, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 51400, 19900, 175.0F, -1140.0F, -1625.0F, 25, 25, 25, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 40800, 22100, 100.0F, -1190.0F, -1650.0F, 25, 25, 50, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 26800, 31500, 100.0F, -1190.0F, -1675.0F, 25, 25, 25, 0.0F, false));

		ihaveexam3 = new RendererModel(this);
		ihaveexam3.setRotationPoint(0.0F, -1575.0F, 0.0F);
		unit.addChild(ihaveexam3);

		bone48 = new RendererModel(this);
		bone48.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone48, 0.0F, -2.0944F, 0.0F);
		ihaveexam3.addChild(bone48);
		bone48.cubeList.add(new ModelBox(bone48, 25100, 15000, -200.0F, -300.0F, -450.0F, 400, 300, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 25100, 15000, -200.0F, -300.0F, 350.0F, 400, 300, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 45700, 0, -400.0F, -75.0F, -350.0F, 825, 200, 700, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -675.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -675.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -1550.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -1550.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -2625.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -2625.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -3550.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -3550.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -200.0F, -4375.0F, -350.0F, 450, 150, 100, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 38600, 19200, -250.0F, -4375.0F, 250.0F, 450, 150, 100, 0.0F, false));

		bone47 = new RendererModel(this);
		bone47.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone47, 0.0F, -1.0472F, 0.0F);
		ihaveexam3.addChild(bone47);
		bone47.cubeList.add(new ModelBox(bone47, 25100, 15000, -200.0F, -300.0F, -450.0F, 400, 300, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 25100, 15000, -200.0F, -300.0F, 350.0F, 400, 300, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -675.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -675.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -1550.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -1550.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -2625.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -2625.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -3550.0F, -350.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -3550.0F, 250.0F, 400, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -250.0F, -4375.0F, -350.0F, 450, 150, 100, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 38600, 19200, -200.0F, -4375.0F, 250.0F, 450, 150, 100, 0.0F, false));

		bone52 = new RendererModel(this);
		bone52.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone52, 0.0262F, 0.0F, 0.0F);
		bone47.addChild(bone52);

		bone53 = new RendererModel(this);
		bone53.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone53, 0.0262F, 3.1416F, 0.0F);
		bone47.addChild(bone53);

		bone46 = new RendererModel(this);
		bone46.setRotationPoint(0.0F, 0.0F, 0.0F);
		ihaveexam3.addChild(bone46);
		bone46.cubeList.add(new ModelBox(bone46, 25100, 15000, -200.0F, -300.0F, -450.0F, 400, 300, 100, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 25100, 15000, -200.0F, -300.0F, 350.0F, 400, 300, 100, 0.0F, false));

		bone49 = new RendererModel(this);
		bone49.setRotationPoint(0.0F, 1600.0F, 0.0F);
		ihaveexam3.addChild(bone49);
		bone49.cubeList.add(new ModelBox(bone49, 25100, 15000, -500.0F, -1900.0F, -50.0F, 100, 400, 100, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 25100, 15000, 400.0F, -1900.0F, -50.0F, 100, 400, 100, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 11200, 6300, -1525.0F, -1215.0F, -150.0F, 1025, 200, 300, 0.0F, false));

		bone50 = new RendererModel(this);
		bone50.setRotationPoint(0.0F, 1600.0F, 0.0F);
		setRotationAngle(bone50, 0.0F, -1.0472F, 0.0F);
		ihaveexam3.addChild(bone50);
		bone50.cubeList.add(new ModelBox(bone50, 25100, 15000, -500.0F, -1900.0F, -50.0F, 100, 400, 100, 0.0F, false));
		bone50.cubeList.add(new ModelBox(bone50, 25100, 15000, 400.0F, -1900.0F, -50.0F, 100, 400, 100, 0.0F, false));
		bone50.cubeList.add(new ModelBox(bone50, 11200, 6300, 600.0F, -1215.0F, -150.0F, 925, 200, 300, 0.0F, false));

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(0.0F, -25.0F, 0.0F);
		bone50.addChild(bone62);
		bone62.cubeList.add(new ModelBox(bone62, 11200, 6300, -1525.0F, -1190.0F, -150.0F, 900, 200, 300, 0.0F, false));
		bone62.cubeList.add(new ModelBox(bone62, 27100, 21400, -725.0F, -1040.0F, -150.0F, 100, 1025, 300, 0.0F, false));
		bone62.cubeList.add(new ModelBox(bone62, 27100, 21400, -1325.0F, -50.0F, -100.0F, 700, 50, 200, 0.0F, false));

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(0.0F, -25.0F, 0.0F);
		setRotationAngle(bone63, 0.0F, 3.1416F, 0.0F);
		bone50.addChild(bone63);
		bone63.cubeList.add(new ModelBox(bone63, 27100, 21400, -725.0F, -1040.0F, -150.0F, 100, 1025, 300, 0.0F, false));
		bone63.cubeList.add(new ModelBox(bone63, 27100, 21400, -1335.0F, -50.0F, -100.0F, 725, 50, 200, 0.0F, false));

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(0.0F, 1600.0F, 0.0F);
		setRotationAngle(bone51, 0.0F, -2.0944F, 0.0F);
		ihaveexam3.addChild(bone51);
		bone51.cubeList.add(new ModelBox(bone51, 25100, 15000, -500.0F, -1900.0F, -50.0F, 100, 400, 100, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 25100, 15000, 400.0F, -1900.0F, -50.0F, 100, 400, 100, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 11200, 6300, -1525.0F, -1215.0F, -150.0F, 900, 200, 300, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 27100, 21400, -725.0F, -1065.0F, -150.0F, 100, 1025, 300, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 27100, 21400, -1332.5F, -65.0F, -100.0F, 700, 50, 200, 0.0F, false));

		bone73 = new RendererModel(this);
		bone73.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone73, 0.0F, 3.1416F, 0.0F);
		bone51.addChild(bone73);
		bone73.cubeList.add(new ModelBox(bone73, 27100, 21400, -1335.0F, -75.0F, -100.0F, 750, 50, 200, 0.0F, false));
		bone73.cubeList.add(new ModelBox(bone73, 27100, 21400, -725.0F, -1065.0F, -150.0F, 100, 1025, 300, 0.0F, false));
		bone73.cubeList.add(new ModelBox(bone73, 11200, 6300, -1525.0F, -1215.0F, -150.0F, 900, 200, 300, 0.0F, false));

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.0F, 1575.0F, 0.0F);
		ihaveexam3.addChild(bone37);
		bone37.cubeList.add(new ModelBox(bone37, 27100, 21400, -725.0F, -1040.0F, -150.0F, 100, 1025, 300, 0.0F, false));
		bone37.cubeList.add(new ModelBox(bone37, 27100, 21400, -1325.0F, -40.0F, -100.0F, 700, 50, 200, 0.0F, false));

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(0.0F, 1575.0F, 0.0F);
		setRotationAngle(bone64, 0.0F, 3.1416F, 0.0F);
		ihaveexam3.addChild(bone64);
		bone64.cubeList.add(new ModelBox(bone64, 11200, 6300, -1525.0F, -1190.0F, -150.0F, 900, 200, 300, 0.0F, false));
		bone64.cubeList.add(new ModelBox(bone64, 27100, 21400, -725.0F, -1040.0F, -150.0F, 100, 1025, 300, 0.0F, false));
		bone64.cubeList.add(new ModelBox(bone64, 27100, 21400, -1335.0F, -50.0F, -100.0F, 700, 50, 200, 0.0F, false));

		underpillar3 = new RendererModel(this);
		underpillar3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar3, 0.0F, -2.618F, 0.0F);
		unit.addChild(underpillar3);

		bone79 = new RendererModel(this);
		bone79.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone79, -1.2217F, 0.0F, 0.0F);
		underpillar3.addChild(bone79);
		bone79.cubeList.add(new ModelBox(bone79, 20900, 25400, -225.477F, -1836.848F, -535.0F, 150, 925, 150, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 25100, 20800, 74.523F, -1636.848F, -735.0F, 150, 1000, 200, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 25100, 20800, -225.477F, -1636.848F, -735.0F, 150, 1025, 200, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 20900, 25400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 20400, 19400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));

		bone80 = new RendererModel(this);
		bone80.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone80, 0.1745F, 0.0F, 0.0F);
		underpillar3.addChild(bone80);
		bone80.cubeList.add(new ModelBox(bone80, 20900, 25400, 75.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 25100, 20800, -225.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 25100, 20800, 75.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 20900, 25400, -225.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));

		bone81 = new RendererModel(this);
		bone81.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone81, -0.0873F, 0.0F, 0.0F);
		underpillar3.addChild(bone81);
		bone81.cubeList.add(new ModelBox(bone81, 20900, 25400, 75.0F, -215.0F, 880.0F, 150, 100, 450, 0.0F, false));
		bone81.cubeList.add(new ModelBox(bone81, 20900, 25400, -225.0F, -215.0F, 805.0F, 150, 100, 525, 0.0F, false));

		underpillar2 = new RendererModel(this);
		underpillar2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar2, 0.0F, -0.5236F, 0.0F);
		unit.addChild(underpillar2);

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone61, -1.2217F, 0.0F, 0.0F);
		underpillar2.addChild(bone61);
		bone61.cubeList.add(new ModelBox(bone61, 20900, 25400, -225.477F, -1836.848F, -535.0F, 150, 925, 150, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 25100, 20800, 74.523F, -1636.848F, -735.0F, 150, 1000, 200, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 25100, 20800, -225.477F, -1636.848F, -735.0F, 150, 1025, 200, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 25100, 20800, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 20900, 25400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));

		bone74 = new RendererModel(this);
		bone74.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone74, 0.1745F, 0.0F, 0.0F);
		underpillar2.addChild(bone74);
		bone74.cubeList.add(new ModelBox(bone74, 20900, 25400, 75.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 25100, 20800, -225.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 25100, 20800, 75.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 20900, 25400, -225.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));

		bone75 = new RendererModel(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone75, -0.0873F, 0.0F, 0.0F);
		underpillar2.addChild(bone75);
		bone75.cubeList.add(new ModelBox(bone75, 20900, 25400, 75.0F, -215.0F, 880.0F, 150, 100, 450, 0.0F, false));
		bone75.cubeList.add(new ModelBox(bone75, 20900, 25400, -225.0F, -215.0F, 805.0F, 150, 100, 525, 0.0F, false));

		underpillar4 = new RendererModel(this);
		underpillar4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar4, 0.0F, -1.5708F, 0.0F);
		unit.addChild(underpillar4);

		bone76 = new RendererModel(this);
		bone76.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone76, -1.2217F, 0.0F, 0.0F);
		underpillar4.addChild(bone76);
		bone76.cubeList.add(new ModelBox(bone76, 20900, 25400, -225.477F, -1836.848F, -535.0F, 150, 925, 150, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 25100, 20800, 74.523F, -1636.848F, -735.0F, 150, 1000, 200, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 25100, 20800, -225.477F, -1636.848F, -735.0F, 150, 1025, 200, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 25100, 20800, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 20900, 25400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));

		bone77 = new RendererModel(this);
		bone77.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone77, 0.1745F, 0.0F, 0.0F);
		underpillar4.addChild(bone77);
		bone77.cubeList.add(new ModelBox(bone77, 20900, 25400, 75.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));
		bone77.cubeList.add(new ModelBox(bone77, 25100, 20800, -225.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone77.cubeList.add(new ModelBox(bone77, 25100, 20800, 75.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone77.cubeList.add(new ModelBox(bone77, 20900, 25400, -225.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));

		bone78 = new RendererModel(this);
		bone78.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone78, -0.0873F, 0.0F, 0.0F);
		underpillar4.addChild(bone78);
		bone78.cubeList.add(new ModelBox(bone78, 20900, 25400, 75.0F, -215.0F, 880.0F, 150, 100, 450, 0.0F, false));
		bone78.cubeList.add(new ModelBox(bone78, 20900, 25400, -225.0F, -215.0F, 805.0F, 150, 100, 525, 0.0F, false));

		underpillar5 = new RendererModel(this);
		underpillar5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar5, 0.0F, 2.618F, 0.0F);
		unit.addChild(underpillar5);

		bone82 = new RendererModel(this);
		bone82.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone82, -1.2217F, 0.0F, 0.0F);
		underpillar5.addChild(bone82);
		bone82.cubeList.add(new ModelBox(bone82, 20900, 25400, -225.477F, -1836.848F, -535.0F, 150, 925, 150, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 25100, 20800, 74.523F, -1636.848F, -735.0F, 150, 1000, 200, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 25100, 20800, -225.477F, -1636.848F, -735.0F, 150, 1025, 200, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 20900, 25400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 20400, 19400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));

		bone83 = new RendererModel(this);
		bone83.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone83, 0.1745F, 0.0F, 0.0F);
		underpillar5.addChild(bone83);
		bone83.cubeList.add(new ModelBox(bone83, 20900, 25400, 75.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 25100, 20800, -225.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 25100, 20800, 75.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 20900, 25400, -225.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));

		bone84 = new RendererModel(this);
		bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone84, -0.0873F, 0.0F, 0.0F);
		underpillar5.addChild(bone84);
		bone84.cubeList.add(new ModelBox(bone84, 20900, 25400, 75.0F, -215.0F, 880.0F, 150, 100, 450, 0.0F, false));
		bone84.cubeList.add(new ModelBox(bone84, 20900, 25400, -225.0F, -215.0F, 805.0F, 150, 100, 525, 0.0F, false));

		underpillar6 = new RendererModel(this);
		underpillar6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar6, 0.0F, 0.5236F, 0.0F);
		unit.addChild(underpillar6);

		bone85 = new RendererModel(this);
		bone85.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone85, -1.2217F, 0.0F, 0.0F);
		underpillar6.addChild(bone85);
		bone85.cubeList.add(new ModelBox(bone85, 20900, 25400, -225.477F, -1836.848F, -535.0F, 150, 925, 150, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 25100, 20800, 74.523F, -1636.848F, -735.0F, 150, 1000, 200, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 25100, 20800, -225.477F, -1636.848F, -735.0F, 150, 1025, 200, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 20900, 25400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 20400, 19400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));

		bone86 = new RendererModel(this);
		bone86.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone86, 0.1745F, 0.0F, 0.0F);
		underpillar6.addChild(bone86);
		bone86.cubeList.add(new ModelBox(bone86, 20900, 25400, 75.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));
		bone86.cubeList.add(new ModelBox(bone86, 25100, 20800, -225.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone86.cubeList.add(new ModelBox(bone86, 25100, 20800, 75.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone86.cubeList.add(new ModelBox(bone86, 20900, 25400, -225.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));

		bone87 = new RendererModel(this);
		bone87.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone87, -0.0873F, 0.0F, 0.0F);
		underpillar6.addChild(bone87);
		bone87.cubeList.add(new ModelBox(bone87, 20900, 25400, 75.0F, -215.0F, 880.0F, 150, 100, 450, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 20900, 25400, -225.0F, -215.0F, 805.0F, 150, 100, 525, 0.0F, false));

		underpillar7 = new RendererModel(this);
		underpillar7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar7, 0.0F, 1.5708F, 0.0F);
		unit.addChild(underpillar7);

		bone88 = new RendererModel(this);
		bone88.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone88, -1.2217F, 0.0F, 0.0F);
		underpillar7.addChild(bone88);
		bone88.cubeList.add(new ModelBox(bone88, 20900, 25400, -225.477F, -1836.848F, -535.0F, 150, 925, 150, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 25100, 20800, 74.523F, -1636.848F, -735.0F, 150, 1000, 200, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 25100, 20800, -225.477F, -1636.848F, -735.0F, 150, 1025, 200, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 25100, 20800, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 20900, 25400, 77.023F, -1836.848F, -535.0F, 147, 925, 150, 0.0F, false));

		bone89 = new RendererModel(this);
		bone89.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone89, 0.1745F, 0.0F, 0.0F);
		underpillar7.addChild(bone89);
		bone89.cubeList.add(new ModelBox(bone89, 20900, 25400, 75.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 25100, 20800, -225.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 25100, 20800, 75.0F, -690.0F, 255.0F, 150, 775, 550, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 20900, 25400, -225.0F, -690.0F, 805.0F, 150, 800, 125, 0.0F, false));

		bone90 = new RendererModel(this);
		bone90.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone90, -0.0873F, 0.0F, 0.0F);
		underpillar7.addChild(bone90);
		bone90.cubeList.add(new ModelBox(bone90, 20900, 25400, 75.0F, -215.0F, 880.0F, 150, 100, 450, 0.0F, false));
		bone90.cubeList.add(new ModelBox(bone90, 20900, 25400, -225.0F, -215.0F, 805.0F, 150, 100, 525, 0.0F, false));

		greenfloor = new RendererModel(this);
		greenfloor.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(greenfloor, 0.0F, -0.5236F, 0.0F);
		unit.addChild(greenfloor);

		bone91 = new RendererModel(this);
		bone91.setRotationPoint(0.0F, 0.0F, 0.0F);
		greenfloor.addChild(bone91);
		bone91.cubeList.add(new ModelBox(bone91, 27100, 21400, 995.0F, -50.0F, -475.0F, 275, 50, 950, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 27100, 21400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone92 = new RendererModel(this);
		bone92.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone92, 0.0F, -1.0472F, 0.0F);
		greenfloor.addChild(bone92);
		bone92.cubeList.add(new ModelBox(bone92, 27100, 21400, 995.0F, -50.0F, -475.0F, 275, 50, 950, 0.0F, false));
		bone92.cubeList.add(new ModelBox(bone92, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone92.cubeList.add(new ModelBox(bone92, 27100, 21400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone93 = new RendererModel(this);
		bone93.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone93, 0.0F, -2.0944F, 0.0F);
		greenfloor.addChild(bone93);
		bone93.cubeList.add(new ModelBox(bone93, 27100, 21400, 995.0F, -50.0F, -475.0F, 275, 50, 950, 0.0F, false));
		bone93.cubeList.add(new ModelBox(bone93, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone93.cubeList.add(new ModelBox(bone93, 27100, 21400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone94 = new RendererModel(this);
		bone94.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone94, 0.0F, 3.1416F, 0.0F);
		greenfloor.addChild(bone94);
		bone94.cubeList.add(new ModelBox(bone94, 27100, 21400, 995.0F, -50.0F, -475.0F, 275, 50, 950, 0.0F, false));
		bone94.cubeList.add(new ModelBox(bone94, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone94.cubeList.add(new ModelBox(bone94, 27100, 21400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone95 = new RendererModel(this);
		bone95.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone95, 0.0F, 1.0472F, 0.0F);
		greenfloor.addChild(bone95);
		bone95.cubeList.add(new ModelBox(bone95, 27100, 21400, 995.0F, -50.0F, -475.0F, 275, 50, 950, 0.0F, false));
		bone95.cubeList.add(new ModelBox(bone95, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone95.cubeList.add(new ModelBox(bone95, 27100, 21400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone96 = new RendererModel(this);
		bone96.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone96, 0.0F, 2.0944F, 0.0F);
		greenfloor.addChild(bone96);
		bone96.cubeList.add(new ModelBox(bone96, 27100, 21400, 995.0F, -50.0F, -475.0F, 275, 50, 950, 0.0F, false));
		bone96.cubeList.add(new ModelBox(bone96, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone96.cubeList.add(new ModelBox(bone96, 27100, 21400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		greenfloor2 = new RendererModel(this);
		greenfloor2.setRotationPoint(0.0F, -1100.0F, 0.0F);
		setRotationAngle(greenfloor2, 0.0F, -0.5236F, 0.0F);
		unit.addChild(greenfloor2);

		bone97 = new RendererModel(this);
		bone97.setRotationPoint(0.0F, 0.0F, 0.0F);
		greenfloor2.addChild(bone97);
		bone97.cubeList.add(new ModelBox(bone97, 27100, 21400, 995.0F, -50.0F, -525.0F, 350, 50, 1050, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 9600, 13800, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone98 = new RendererModel(this);
		bone98.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone98, 0.0F, -1.0472F, 0.0F);
		greenfloor2.addChild(bone98);
		bone98.cubeList.add(new ModelBox(bone98, 27100, 21400, 995.0F, -50.0F, -525.0F, 350, 50, 1050, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 0, 14400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone99 = new RendererModel(this);
		bone99.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone99, 0.0F, -2.0944F, 0.0F);
		greenfloor2.addChild(bone99);
		bone99.cubeList.add(new ModelBox(bone99, 27100, 21400, 995.0F, -50.0F, -525.0F, 350, 50, 1050, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 9600, 14400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone100 = new RendererModel(this);
		bone100.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone100, 0.0F, 3.1416F, 0.0F);
		greenfloor2.addChild(bone100);
		bone100.cubeList.add(new ModelBox(bone100, 27100, 21400, 995.0F, -50.0F, -525.0F, 350, 50, 1050, 0.0F, false));
		bone100.cubeList.add(new ModelBox(bone100, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone100.cubeList.add(new ModelBox(bone100, 0, 15600, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone101 = new RendererModel(this);
		bone101.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone101, 0.0F, 1.0472F, 0.0F);
		greenfloor2.addChild(bone101);
		bone101.cubeList.add(new ModelBox(bone101, 27100, 21400, 995.0F, -50.0F, -525.0F, 375, 50, 1025, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 11200, 14400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		bone102 = new RendererModel(this);
		bone102.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone102, 0.0F, 2.0944F, 0.0F);
		greenfloor2.addChild(bone102);
		bone102.cubeList.add(new ModelBox(bone102, 27100, 21400, 995.0F, -50.0F, -525.0F, 350, 50, 1050, 0.0F, false));
		bone102.cubeList.add(new ModelBox(bone102, 27100, 21400, 720.0F, -50.0F, -325.0F, 275, 50, 650, 0.0F, false));
		bone102.cubeList.add(new ModelBox(bone102, 6400, 14400, 445.0F, -50.0F, -300.0F, 275, 50, 600, 0.0F, false));

		greenwall = new RendererModel(this);
		greenwall.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(greenwall);

		wall = new RendererModel(this);
		wall.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(wall, 0.0F, -1.0472F, 0.0F);
		greenwall.addChild(wall);

		under6 = new RendererModel(this);
		under6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under6, 0.0F, 3.1416F, 0.0F);
		unit.addChild(under6);

		bone108 = new RendererModel(this);
		bone108.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone108, 0.2618F, 0.0F, 0.0F);
		under6.addChild(bone108);
		bone108.cubeList.add(new ModelBox(bone108, 27100, 21400, -275.0F, -950.0F, 450.0F, 575, 150, 625, 0.0F, false));

		under5 = new RendererModel(this);
		under5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under5, 0.0F, 2.0944F, 0.0F);
		unit.addChild(under5);

		bone107 = new RendererModel(this);
		bone107.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone107, 0.2618F, 0.0F, 0.0F);
		under5.addChild(bone107);
		bone107.cubeList.add(new ModelBox(bone107, 27100, 21400, -275.0F, -950.0F, 450.0F, 575, 150, 625, 0.0F, false));

		under4 = new RendererModel(this);
		under4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under4, 0.0F, 1.0472F, 0.0F);
		unit.addChild(under4);

		bone106 = new RendererModel(this);
		bone106.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone106, 0.2618F, 0.0F, 0.0F);
		under4.addChild(bone106);
		bone106.cubeList.add(new ModelBox(bone106, 27100, 21400, -275.0F, -950.0F, 450.0F, 575, 150, 625, 0.0F, false));

		under3 = new RendererModel(this);
		under3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under3, 0.0F, -2.0944F, 0.0F);
		unit.addChild(under3);

		bone105 = new RendererModel(this);
		bone105.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone105, 0.2618F, 0.0F, 0.0F);
		under3.addChild(bone105);
		bone105.cubeList.add(new ModelBox(bone105, 27100, 21400, -275.0F, -950.0F, 450.0F, 575, 150, 625, 0.0F, false));

		under2 = new RendererModel(this);
		under2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under2, 0.0F, -1.0472F, 0.0F);
		unit.addChild(under2);

		bone104 = new RendererModel(this);
		bone104.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone104, 0.2618F, 0.0F, 0.0F);
		under2.addChild(bone104);
		bone104.cubeList.add(new ModelBox(bone104, 27100, 21400, -275.0F, -950.0F, 450.0F, 575, 150, 625, 0.0F, false));

		under = new RendererModel(this);
		under.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(under);

		bone103 = new RendererModel(this);
		bone103.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone103, 0.2618F, 0.0F, 0.0F);
		under.addChild(bone103);
		bone103.cubeList.add(new ModelBox(bone103, 27100, 21400, -275.0F, -950.0F, 450.0F, 575, 150, 625, 0.0F, false));
	}
	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ConsoleTile console, float scale) {
		GlStateManager.pushMatrix();
		GlStateManager.scaled(0.01, 0.01, 0.01);
		copper.render(scale);
		GlStateManager.popMatrix();
	}
}