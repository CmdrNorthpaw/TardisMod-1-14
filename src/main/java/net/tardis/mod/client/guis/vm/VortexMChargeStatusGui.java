package net.tardis.mod.client.guis.vm;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;


public class VortexMChargeStatusGui extends Screen{
	
	private static final ResourceLocation BACKGROUND = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui_function.png");
	
	public TextFieldWidget chargeValue;
	private Button back;

    private final int xOffset = 70;
    private final int yOffset = 70;


	private Minecraft mc;
	public FontRenderer fr;
	
	public VortexMChargeStatusGui(ITextComponent title) {
		super(title);
	}
	
	public VortexMChargeStatusGui() {
		this(new StringTextComponent("Charge Status"));
		mc = Minecraft.getInstance();
		fr = mc.fontRenderer;
	}
	
	
	@Override
	public void init() {
		super.init();
		String backButton = new TranslationTextComponent("button.vm.back").getFormattedText();;
		
		int centreX = Minecraft.getInstance().mainWindow.getScaledWidth() / 2;
		int centreY = Minecraft.getInstance().mainWindow.getScaledHeight() / 2;
		int guiLeft = centreX - xOffset;
		int guiTop = centreY - yOffset;
		
		chargeValue = new TextFieldWidget(fr, guiLeft + 45, guiTop + 75, 50, fr.FONT_HEIGHT, "test");
		back = new Button(guiLeft,guiTop + 30, fr.getStringWidth(backButton) + 8, fr.FONT_HEIGHT + 11, backButton, new Button.IPressable() {
			
			@Override
			public void onPress(Button button) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
			}
		});
		
		this.buttons.clear();
		this.addButton(chargeValue); //addButton also adds other widget types, mapping name can be misleading
		this.addButton(back);
		chargeValue.setFocused2(true);
	}
	
	@Override
	public void renderBackground(int background) {
		int sWidth = 138;
		int sHeight = 142;
		
		int centreX = Minecraft.getInstance().mainWindow.getScaledWidth() / 2;
		int centreY = Minecraft.getInstance().mainWindow.getScaledHeight() / 2;
		int guiLeft = centreX - xOffset;
		int guiTop = centreY - yOffset;
		
		GlStateManager.pushMatrix();
		GlStateManager.enableAlphaTest();
		GlStateManager.enableBlend();
		Minecraft.getInstance().getTextureManager().bindTexture(BACKGROUND);
		blit(guiLeft, guiTop, 0, 0, sWidth, sHeight);
		GlStateManager.popMatrix();
		drawCenteredString(fr, "Charge Status", guiLeft + 73, guiTop + 35, 0xFFFFFF);
	}
	
	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground();
        super.render(mouseX, mouseY, partialTicks);
    }
		@Override
	   public boolean shouldCloseOnEsc() {
		      return true;
		   }
		
		@Override
	   public void onClose() {
            this.minecraft.displayGuiScreen(null);
	   }
	
		@Override
	public boolean isPauseScreen() {
			   return true;
	}
	
}
