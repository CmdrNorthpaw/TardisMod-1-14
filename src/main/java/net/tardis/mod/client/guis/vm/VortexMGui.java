package net.tardis.mod.client.guis.vm;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.vm.widgets.*;
import net.tardis.mod.misc.vm.VortexMFunctions;
import org.lwjgl.glfw.GLFW;

/* 
 * Created by 50ap5ud5 12/09/2019
 * */

public class VortexMGui extends Screen{
	
	private static final ResourceLocation BACKGROUND = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui.png");
	
	private TextFieldWidget currentFunction;
	private Button select;
	private Button fnCycleLeft;
	private Button fnCycleRight;
	private Button catCycleUp;
	private Button catCycleDown;
	private Button closeBtn;
	
	private Minecraft mc;
	private FontRenderer fr;
	
    private final int xOffset = 70; //Offset from centre
    private final int yOffset = 70;
	
	private int id = 0;
	
	VortexMFunctions function = new VortexMFunctions();
	VortexMChargeStatusGui chargeStatus = new VortexMChargeStatusGui();
	
	
	public VortexMGui() {
		this(new StringTextComponent("Vortex Manipulator"));
		mc = Minecraft.getInstance();
		fr = mc.fontRenderer;
		function.init();
	}
	
	public VortexMGui(ITextComponent title) {
		super(title);
	}
	
	
	@Override
	public void init() {
		super.init();
		final int btnSelectW = 14, btnSelectH = 14;
		final int btnFnCycleW = 4, btnFnCycleH = 12;
		
		int centreX = Minecraft.getInstance().mainWindow.getScaledWidth() / 2; //Centre of screen in X direction
		int centreY = Minecraft.getInstance().mainWindow.getScaledHeight() / 2; //Centre of screen in Y direction
		int guiLeft = centreX - xOffset; //Left Corner of UI
		int guiTop = centreY - yOffset; //Top of UI
		
		currentFunction = new DisabledTextFieldWidget(fr, guiLeft + 30, guiTop + 123, 80, fr.FONT_HEIGHT + 2, VortexMFunctions.FUNCTIONS.get(id).getNameKey());
		fnCycleLeft = new VortexMButtonFnCycleLeft(guiLeft + 23, guiTop + 64, btnFnCycleW, btnFnCycleH, "", new VortexMButtonFnCycleLeft.IPressable() {
			@Override
			public void onPress(Button button) {
				decrementFunction();
			}
		});
		fnCycleRight = new VortexMButtonFnCycleRight(guiLeft + 39, guiTop +64, btnFnCycleW, btnFnCycleH, "", new VortexMButtonFnCycleLeft.IPressable() {
			@Override
			public void onPress(Button button) {
				incrementFunction();
			}
		});
		catCycleUp = new VortexMButtonCatCycleUp(guiLeft + 27, guiTop + 60, btnFnCycleW, btnFnCycleH, "", new VortexMButtonCatCycleUp.IPressable() {
			@Override
			public void onPress(Button button) {
				//WIP
			}
		});
		catCycleDown = new VortexMButtonCatCycleDown(guiLeft + 27, guiTop + 75, btnFnCycleW, btnFnCycleH, "", new VortexMButtonCatCycleDown.IPressable() {
			@Override
			public void onPress(Button button) {
				//WIP
			}
		});
		select = new VortexMButtonSelect(guiLeft + 43, guiTop + 49, btnSelectW, btnSelectH, "", new VortexMButtonSelect.IPressable() {
			@Override
			public void onPress(Button button) {
				if (VortexMFunctions.FUNCTIONS.get(id).stateComplete().equals(false)) {
					currentFunction.setText(new StringTextComponent("Locked: WIP").applyTextStyle(TextFormatting.RED).getFormattedText());
				}
				else {
					VortexMFunctions.getFunction(id).onActivated(Minecraft.getInstance().world, Minecraft.getInstance().player);
				}
			}
		});
		closeBtn = new VortexMButtonClose(guiLeft + 80, guiTop + 49, btnSelectW, btnSelectH, "", new VortexMButtonClose.IPressable() {
			@Override
			public void onPress(Button button) {
				onClose();
			}
		}); //TODO: Fix black rendering

		
		this.buttons.clear();
		this.addButton(select);
		this.addButton(fnCycleLeft);
		this.addButton(fnCycleRight);
		this.addButton(catCycleUp);
		this.addButton(catCycleDown);
		this.addButton(currentFunction);
		this.addButton(closeBtn);
		currentFunction.setEnabled(false);
		currentFunction.setText(new StringTextComponent(VortexMFunctions.FUNCTIONS.get(id).getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText()); //Set default function name on init
		currentFunction.setCursorPositionZero();
	}
	
	@Override
	public void renderBackground(int background) {
		int sWidth = 138;
		int sHeight = 146;
		
		int centreX = Minecraft.getInstance().mainWindow.getScaledWidth() / 2; //Centre of screen in X direction
		int centreY = Minecraft.getInstance().mainWindow.getScaledHeight() / 2; //Centre of screen in Y direction
		int guiLeft = centreX - xOffset; //Left Corner of UI
		int guiTop = centreY - yOffset; //Top of UI
		
		Minecraft.getInstance().getTextureManager().bindTexture(BACKGROUND);
		blit(guiLeft, guiTop, 0, 0, sWidth, sHeight);
		drawCenteredString(fr, "Selected Function:", guiLeft + 70, guiTop + 110, 0xFFFFFF);
	}
	
	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground();
        super.render(mouseX, mouseY, partialTicks);
    }
		
	@Override
   public void onClose() {
        this.minecraft.displayGuiScreen(null);
   }
		
	@Override
	public boolean keyPressed(int keyCode, int scanCode, int bitmaskModifier) {
		final int rightKey = GLFW.GLFW_KEY_RIGHT;
		final int leftKey = GLFW.GLFW_KEY_LEFT;
		final int escKey = GLFW.GLFW_KEY_ESCAPE;
		final int enterKey = GLFW.GLFW_KEY_ENTER;
		
		switch(keyCode) {
		case rightKey: //Right arrow key
			incrementFunction();
			return true;
		case leftKey: //Left arrow key
			decrementFunction();
			return true;
		case escKey: //Close Button, for some reason this overrides ShouldCloseOnEsc()
			onClose();
			return true;
		case enterKey:
			select.onPress();
			return true;
		default:
			return false;
		}
	}
	
	public void decrementFunction() {
		--id;
		if (id < 0) {
            id  = VortexMFunctions.FUNCTIONS.size() - 1;	//Safety check, gets last map key value
        }
		if(VortexMFunctions.FUNCTIONS.containsKey(id)) {
			currentFunction.setText(new StringTextComponent(VortexMFunctions.FUNCTIONS.get(id).getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText());
			currentFunction.setCursorPositionZero();
		}

	}
	
	public void incrementFunction() {
		if (id + 1 > VortexMFunctions.FUNCTIONS.size() - 1) {
            id = 0;
        } else {
            ++id;
        }
		if(VortexMFunctions.FUNCTIONS.containsKey(id)) {
          currentFunction.setText(new StringTextComponent(VortexMFunctions.FUNCTIONS.get(id).getNameKey()).applyTextStyle(TextFormatting.BLUE).getFormattedText());
          currentFunction.setCursorPositionZero();
		}
	}
	
	@Override
	public boolean isPauseScreen() {
			   return true;
	}
}
