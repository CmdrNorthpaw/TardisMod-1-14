package net.tardis.mod.client.renderers.sky;

import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraftforge.client.IRenderHandler;

public class EmptyRenderer implements IRenderHandler{

	@Override
	public void render(int ticks, float partialTicks, ClientWorld world, Minecraft mc) {}

}
