package net.tardis.mod.client.renderers.sky;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.IRenderHandler;
import net.tardis.mod.Tardis;

@OnlyIn(Dist.CLIENT)
public class VortexSkyRenderer implements IRenderHandler {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/sky/vortex.png");
	
	@Override
	public void render(int ticks, float partialTicks, ClientWorld world, Minecraft mc) {
		GlStateManager.pushMatrix();
		GlStateManager.disableFog();
		GlStateManager.disableCull();
		GlStateManager.color3f(1, 1, 1);
		GlStateManager.translated(0, -mc.player.posY, 0);
		GlStateManager.rotated((world.getGameTime() + partialTicks) % 360, 0, 1, 0);
		mc.getTextureManager().bindTexture(TEXTURE);
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		double minX = -25, minY = -100, minZ = -25;
		double maxX = 25, maxY = mc.player.posY + 1000, maxZ = 25;
		double maxU = 1, maxV = maxY / 25.0;
		
		bb.pos(minX, minY, minZ).tex(0, 0).endVertex();
		bb.pos(maxX, minY, minZ).tex(maxU, 0).endVertex();
		bb.pos(maxX, maxY, minZ).tex(maxU, maxV).endVertex();
		bb.pos(minX, maxY, minZ).tex(0, maxV).endVertex();
		
		bb.pos(minX, minY, 25).tex(0, 0).endVertex();
		bb.pos(minX, maxY, 25).tex(0, maxV).endVertex();
		bb.pos(maxX, maxY, 25).tex(maxU, maxV).endVertex();
		bb.pos(maxX, minY, 25).tex(maxU, 0).endVertex();
		
		bb.pos(-25, minY, minZ).tex(0, 0).endVertex();
		bb.pos(-25, maxY, minZ).tex(0, maxV).endVertex();
		bb.pos(-25, maxY, maxZ).tex(maxU, maxV).endVertex();
		bb.pos(-25, minY, maxZ).tex(maxU, 0).endVertex();
		
		bb.pos(25, minY, minZ).tex(0, 0).endVertex();
		bb.pos(25, minY, maxZ).tex(maxU, 0).endVertex();
		bb.pos(25, maxY, maxZ).tex(maxU, maxV).endVertex();
		bb.pos(25, maxY, minZ).tex(0, maxV).endVertex();
		
		Tessellator.getInstance().draw();
		GlStateManager.enableFog();
		GlStateManager.enableCull();
		GlStateManager.popMatrix();
	}
	
	public void drawQuad(BufferBuilder bb, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
		bb.pos(minX, minY, minZ).tex(0, 0).endVertex();
		bb.pos(maxX, minY, minZ).tex(1, 0).endVertex();
		bb.pos(maxX, maxY, minZ).tex(1, 1).endVertex();
		bb.pos(minX, maxY, minZ).tex(0, 1).endVertex();
	}

}
