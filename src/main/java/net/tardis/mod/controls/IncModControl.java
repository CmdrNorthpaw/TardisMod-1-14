package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class IncModControl extends BaseControl{
	
	public static final String USE_TRANSLATION = "control.use." + Tardis.MODID + ".coord_inc";
	public static int[] COORD_MODS = new int[] {1, 10, 100, 1000};
	public int index;
	
	public IncModControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(4 / 16.0F,  4 / 16.0F);
		return EntitySize.flexible(3 / 16.0F, 2 / 16F);
	}
	
	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-13 / 16.0, 6 / 16.0F, 7 / 16.0F);
		return new Vec3d(-11 / 16.0, 7 / 16.0, -11 / 16.0);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			if(player.isSneaking()) {
				if(index - 1 < 0)
					index = COORD_MODS.length - 1;
				else index -= 1;
				console.coordIncr = COORD_MODS[index];
			}
			else {
				if(index + 1 >= COORD_MODS.length)
					index = 0;
				else index += 1;
				console.coordIncr = COORD_MODS[index];
				console.updateClient();
			}
			this.setAnimationTicks(10);
			player.sendStatusMessage(new TranslationTextComponent(USE_TRANSLATION, console.coordIncr), true);
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_TWO;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.index = tag.getInt("index");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("index", index);
		return tag;
	}

}
