package net.tardis.mod.experimental;

import com.mojang.blaze3d.platform.GLX;

import net.minecraft.client.shader.Framebuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Created by Swirtzly
 * on 03/11/2019 @ 09:34
 */

//TODO: REMOVE WHEN FORGE ADDS THIS DIRECTLY
public class StencilBuffer {

	private static boolean isSet = false;
	
	@OnlyIn(Dist.CLIENT)
    public static void enableStencilBuffer(Framebuffer fbo) {
        if (fbo.useDepth) {
            GLX.glBindRenderbuffer(GLX.GL_RENDERBUFFER, fbo.depthBuffer);
            GLX.glRenderbufferStorage(GLX.GL_RENDERBUFFER, org.lwjgl.opengl.EXTPackedDepthStencil.GL_DEPTH24_STENCIL8_EXT, fbo.framebufferTextureWidth, fbo.framebufferTextureHeight);
            GLX.glFramebufferRenderbuffer(GLX.GL_FRAMEBUFFER, org.lwjgl.opengl.EXTFramebufferObject.GL_DEPTH_ATTACHMENT_EXT, GLX.GL_RENDERBUFFER, fbo.depthBuffer);
            GLX.glFramebufferRenderbuffer(GLX.GL_FRAMEBUFFER, org.lwjgl.opengl.EXTFramebufferObject.GL_STENCIL_ATTACHMENT_EXT, GLX.GL_RENDERBUFFER, fbo.depthBuffer);
            isSet = true;
        }
   
    }
	
	public static boolean isStencilBufferEnabled() {
		return isSet;
	}

}
