package net.tardis.mod.client.animation;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public interface IExteriorAnimation{

	void tick();
	ExteriorAnimationEntry<?> getType();
	
	public static class ExteriorAnimationEntry<T extends ExteriorAnimations> implements IRegisterable<ExteriorAnimationEntry<T>>{

		private ResourceLocation name;
		private IAnimSpawn<T> spawn;
		
		public ExteriorAnimationEntry(IAnimSpawn<T> spawn) {
			this.spawn = spawn;
		}
		
		@Override
		public ExteriorAnimationEntry<T> setRegistryName(ResourceLocation regName) {
			this.name = regName;
			return this;
		}

		@Override
		public ResourceLocation getRegistryName() {
			return this.name;
		}
		
		public T create(ExteriorTile tile) {
			return spawn.create(this, tile);
		}
		
	}
	
	public static interface IAnimSpawn<T extends ExteriorAnimations>{
		T create(ExteriorAnimationEntry<T> entry, ExteriorTile tile);
	}
}
