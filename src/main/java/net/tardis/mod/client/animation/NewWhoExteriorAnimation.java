package net.tardis.mod.client.animation;

import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class NewWhoExteriorAnimation extends ExteriorAnimations {

    public NewWhoExteriorAnimation(ExteriorAnimationEntry<?> entry, ExteriorTile exterior) {
		super(entry, exterior);
	}

    private int pulseTimer = 0;

	@Override
	public void tick() {
        switch (exterior.getMatterState()) {
            case DEMAT:
                if (this.pulseTimer == 0)
                    if (exterior.alpha > 0.7F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer = 1;
                    }
                if (this.pulseTimer == 1)
                    if (exterior.alpha < 1.0F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer = 2;
                    }
                if (this.pulseTimer == 2)
                    if (exterior.alpha > 0.8F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer = 3;
                    }
                if (this.pulseTimer == 3)
                    if (exterior.alpha < 1.0F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer = 4;
                    }
                if (this.pulseTimer == 4)
                    if (exterior.alpha > 0.6F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer = 5;
                    }
                if (this.pulseTimer == 5)
                    if (exterior.alpha < 0.9F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer = 6;
                    }
                if (this.pulseTimer == 6)
                    if (exterior.alpha > 0.4F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer = 7;
                    }
                if (this.pulseTimer == 7)
                    if (exterior.alpha < 0.7F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer = 8;
                    }
                if (this.pulseTimer == 8)
                    if (exterior.alpha > 0.3F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer = 9;
                    }
                if (this.pulseTimer == 9)
                    if (exterior.alpha < 0.6F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer = 10;
                    }
                if (this.pulseTimer == 10)
                    if (exterior.alpha > 0.0F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer = -1;
                    }
                break;
            case REMAT:
                if (this.pulseTimer == 0)
                    if (exterior.alpha < 0.3F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 1)
                    if (exterior.alpha > 0.1F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 2)
                    if (exterior.alpha < 0.4F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 3)
                    if (exterior.alpha > 0.2F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 4)
                    if (exterior.alpha < 0.5F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 5)
                    if (exterior.alpha > 0.3F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 6)
                    if (exterior.alpha < 0.6F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 7)
                    if (exterior.alpha > 0.3F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 8)
                    if (exterior.alpha < 0.7F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 9)
                    if (exterior.alpha > 0.4F) {
                        exterior.alpha -= 0.015F;
                    } else {
                        this.pulseTimer++;
                    }
                if (this.pulseTimer == 10)
                    if (exterior.alpha < 1.0F) {
                        exterior.alpha += 0.015F;
                    } else {
                        this.pulseTimer = 0;
                    }
                break;
		}
	}


}
