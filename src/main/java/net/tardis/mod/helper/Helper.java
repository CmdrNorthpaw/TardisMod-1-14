package net.tardis.mod.helper;

import java.util.UUID;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.state.IProperty;
import net.minecraft.state.IStateHolder;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.dimensions.TDimensions;

public class Helper {

	public static String formatBlockPos(BlockPos pos) {
		return pos.getX() + ", " + pos.getY() + ", " + pos.getZ();
	}

	public static String formatDimName(DimensionType dim) {
		if(dim == null)
			return "UNKNOWN";
		return DimensionType.getKey(dim).getPath().trim().replace("	", "").replace("_", " ");
	}
	
	public static float getAngleFromFacing(Direction dir) {
		if(dir == Direction.NORTH)
			return 0F;
		else if(dir == Direction.EAST)
			return 90;
		else if(dir == Direction.SOUTH)
			return 180;
		return 270F;
	}
	
	/**
	 * Used to determine if an object is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */

	public static boolean canTravelToDimension(DimensionType dimension) {
		for(String s : TConfig.CONFIG.blacklistedDims.get()) {
			if(DimensionType.getKey(dimension).toString().contentEquals(s))
				return false;
		}
		return dimension.getModType() != TDimensions.TARDIS;
	}
	
	/**
	 * Used to determine if the VM is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will require a config option to blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */
	public static boolean canVMTravelToDimension(DimensionType dimension) {
		if (TConfig.CONFIG.toggleVMWhitelistDims.get()) { //If using whitelist
			return TConfig.CONFIG.whitelistedVMDims.get().stream().anyMatch(dimName ->
				dimension.getRegistryName().toString().contentEquals(dimName));
		}
		else if (!TConfig.CONFIG.toggleVMWhitelistDims.get()){ //If using blacklist
			return TConfig.CONFIG.blacklistedVMDims.get().stream().anyMatch(dimName ->
				!dimension.getRegistryName().toString().contains(dimName));
		}
		return dimension.getModType() != TDimensions.TARDIS;
	}
	
	/**
	 * A more performance efficient version of the above to check if an object can be used in the current dimension
	 * @implNote Used for items/blocks that can only be used in the Tardis dimension
	 * @param dimension
	 * @return
	 */
	public static boolean isDimensionBlocked(DimensionType dimension) {
		return dimension.getModType() == TDimensions.TARDIS;
	}

	public static boolean isInBounds(int testX, int testY, int x, int y, int u, int v) {
		return (testX > x &&
				testX < u &&
				testY > y &&
				testY < v);
	}

	public static ResourceLocation getKeyFromDimType(DimensionType dimension) {
		ResourceLocation rl = DimensionType.getKey(dimension);
		return rl != null ? rl : new ResourceLocation("overworld");
	}

	public static boolean isOwnerOn(World world, DimensionType type) {
		if(!world.isRemote) {
			return world.getServer().getPlayerList().getPlayerByUUID(getPlayerFromTARDIS(type)) != null;
		}
		return false;
	}
	
	public static UUID getPlayerFromTARDIS(DimensionType type) {
		ResourceLocation loc = DimensionType.getKey(type);
		return UUID.fromString(loc.getPath());
	}
	
	public static CompoundNBT serializeBlockState(BlockState state) {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("block_id", state.getBlock().getRegistryName().toString());
		ListNBT nbt = new ListNBT();
		for(IProperty<?> prop : state.getProperties()) {
			CompoundNBT val = new CompoundNBT();
			val.putString("name", prop.getName());
			val.putString("value", IStateHolder.writePropertyValueToString(prop, state.get(prop)));
			nbt.add(val);
		}
		tag.put("prop", nbt);
		return tag;
	}
	
	public static BlockState deserializeBlockState(CompoundNBT tag) {
		BlockState state = Registry.BLOCK.getOrDefault(new ResourceLocation(tag.getString("block_id"))).getDefaultState();
		ListNBT prop = tag.getList("prop", Constants.NBT.TAG_COMPOUND);
		for(INBT nbt : prop) {
			CompoundNBT val = (CompoundNBT)nbt;
			String name = val.getString("name");
			String value = val.getString("value");
			for(IProperty<?> iprop : state.getProperties()) {
				if(iprop.getName().contentEquals(name)) {
					state = IStateHolder.func_215671_a(state, iprop, "", "", value);
				}
			}
		}
		return state;
	}
	
	public static void teleportEntities(Entity e, ServerWorld world, double x, double y, double z, float yaw, float pitch) {
		if(e instanceof ServerPlayerEntity) {
			ServerPlayerEntity player = (ServerPlayerEntity)e;
			if(player.world == world) {
				player.connection.setPlayerLocation(x, y, z, yaw, pitch);
			}
			else player.teleport(world, x, y, z, yaw, pitch);
		}
		else {
			//In in same dimension
			if(e.world == world) {
				e.setPosition(x, y, z);
				e.rotationYaw = yaw;
				e.rotationPitch = pitch;
			}
			else {
				//If interdimensional
				e.dimension = world.getDimension().getType();
				Entity old = e;
				Entity newE = e.getType().create(world);
				
				if(old.world instanceof ServerWorld)
					((ServerWorld)old.world).removeEntity(old);
				
				newE.copyDataFromOld(old);
				newE.setPosition(x, y, z);
				world.func_217460_e(newE);
			}
		}
	}
}
