package net.tardis.mod.cap.items;

import java.util.UUID;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;

public class RemoteCapability implements IRemote{

	private ConsoleTile tile;
	UUID owner;

	@Override
	public SpaceTimeCoord getExteriorLocation() {
		if(tile != null && !tile.isRemoved()) {
			return new SpaceTimeCoord(tile.getDimension(), tile.getLocation());
		}
		return SpaceTimeCoord.UNIVERAL_CENTER;
	}

	@Override
	public UUID getOwner() {
		return owner;
	}

	@Override
	public void onClick(World world, PlayerEntity player, BlockPos pos) {
		
	}

	@Override
	public ConsoleTile getConsole() {
		if(tile == null || tile.isRemoved()) {
			if(owner != null) {
				DimensionType type = DimensionType.byName(new ResourceLocation(Tardis.MODID, owner.toString()));
				if(type != null) {
					
				}
			}
		}
		return tile;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		return null;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		
	}

}
