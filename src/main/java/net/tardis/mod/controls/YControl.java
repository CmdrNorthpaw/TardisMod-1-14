package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class YControl extends BaseControl{
	
	public YControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote)
			console.setDestination(console.getDestinationDimension(), console.getDestination()
				.add(0, player.isSneaking() ? -console.getCoordIncr() : console.getCoordIncr(), 0));
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-6.5 / 16.0, 10.5 / 16.0, 4 / 16.0);
		return new Vec3d(-7.5 / 16.0, 9.6 / 16.0, -4.65 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

}
