package net.tardis.mod.client;


import static org.lwjgl.glfw.GLFW.GLFW_KEY_B;

import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.tardis.mod.Tardis;

@EventBusSubscriber(value = Dist.CLIENT)
public class TKeyBinds {
	public static KeyBinding TUTORIAL;
	
	public static void init() {
		TUTORIAL = new KeyBinding(new StringTextComponent(new TranslationTextComponent(Tardis.MODID + ".key.tutorial").getFormattedText()).getString(), GLFW_KEY_B, "New Tardis Mod");
		ClientRegistry.registerKeyBinding(TUTORIAL);
	}
	
}
