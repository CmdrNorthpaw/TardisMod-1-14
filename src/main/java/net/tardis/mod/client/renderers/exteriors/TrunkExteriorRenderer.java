package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ExteriorTrunkModel;
import net.tardis.mod.tileentities.exteriors.TrunkExteriorTile;

public class TrunkExteriorRenderer extends ExteriorRenderer<TrunkExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/trunk_base.png");
	public static final ExteriorTrunkModel MODEL = new ExteriorTrunkModel();
	
	@Override
	public void renderExterior(TrunkExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -0.25, 0);
		GlStateManager.scaled(0.5, 0.5, 0.5);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.popMatrix();
	}

}
