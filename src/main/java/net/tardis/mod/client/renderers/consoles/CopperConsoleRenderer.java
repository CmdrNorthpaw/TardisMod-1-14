package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.ModelConsoleCopper;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.consoles.CopperConsoleTile;

public class CopperConsoleRenderer extends TileEntityRenderer<CopperConsoleTile> {

	public static ModelConsoleCopper model = new ModelConsoleCopper();
	public static ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/copper.png");
	
	@Override
	public void render(CopperConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		this.bindTexture(TEXTURE);
		GlStateManager.rotated(60, 0, 1, 0);
		GlStateManager.scaled(1.25, 1.25, 1.25);
		GlStateManager.enableRescaleNormal();
		model.render(console, ModelHelper.RENDER_SCALE);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();

	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
