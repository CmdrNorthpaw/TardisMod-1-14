package net.tardis.mod.constants;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class Constants {

    public static float halfPI = 1.57079633f;

    public static class Gui {
		
		public static final int MONITOR_MAIN_STEAM = 0;
		public static final int MONITOR_ECHANGE = 1;
		
		public static final int NONE = 2;
		public static final int MONITOR_EDIT_INTERIOR = 3;
		
		public static final int VORTEX_MAIN = 4;
		public static final int VORTEX_TELE = 5;
		public static final int VORTEX_STATUS = 6;

		public static final int MONITOR_MAIN_EYE = 8;
		
		public static final int MANUAL = 9;
		
		public static final int ARS_EGG = 10;
	}
	
	public static class Protocols{
		public static final ResourceLocation EDIT_INTERIOR = new ResourceLocation(Tardis.MODID, "edit_interior");
		public static final ResourceLocation LIFE_SCAN = new ResourceLocation(Tardis.MODID, "life_scan");
		public static final ResourceLocation TOGGLE_ALARM = new ResourceLocation(Tardis.MODID, "toggle_alarm");
    }

    public static class DalekTypes {
		public static final ResourceLocation DALEK_TIMEWAR = new ResourceLocation(Tardis.MODID, "time_war");
		public static final ResourceLocation DALEK_SW = new ResourceLocation(Tardis.MODID, "special_weapons");
		public static final ResourceLocation DALEK_CLASSIC = new ResourceLocation(Tardis.MODID, "classic");
		public static final ResourceLocation DALEK_SEC = new ResourceLocation(Tardis.MODID, "sec");
		public static final ResourceLocation DALEK_SUPREME = new ResourceLocation(Tardis.MODID, "supreme");
        public static final ResourceLocation DALEK_IMPERIAL = new ResourceLocation(Tardis.MODID, "imperial");
    }
    
    public static class Translations{
    	public static final TranslationTextComponent NO_USE_OUTSIDE_TARDIS = new TranslationTextComponent("error." + Tardis.MODID + ".use.in_tardis");
    	
    	public static final String NOT_ENOUGH_ARTRON = "status." + Tardis.MODID + ".not_enough_artron";

		public static final String NO_COMPONENT = "error." + Tardis.MODID + ".no_subsystem";
    }
}
