package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;

public class WaypointLoadMessage {

	public static final String TRANSLATION = "status.waypoint.loaded";
	int index = 0;
	
	public WaypointLoadMessage(int index) {
		this.index = index;
	}
	
	public static void encode(WaypointLoadMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.index);
	}
	
	public static WaypointLoadMessage decode(PacketBuffer buf) {
		return new WaypointLoadMessage(buf.readInt());
	}
	
	public static void handle(WaypointLoadMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TileEntity te = cont.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				ConsoleTile console = (ConsoleTile)te;
				//Basic sanity checking
				if(mes.index < console.getWaypoints().size() && mes.index >= 0) {
					SpaceTimeCoord coord = console.getWaypoints().get(mes.index);
					if(!coord.equals(SpaceTimeCoord.UNIVERAL_CENTER)) {
						console.setDestination(DimensionType.byName(coord.getDimType()), coord.getPos());
						console.setDirection(coord.getFacing());
						cont.get().getSender().sendStatusMessage(new TranslationTextComponent(TRANSLATION, coord.getName()), true);
					}
				}
			}
		});
		cont.get().setPacketHandled(true);
	}
}
