package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.registries.TardisRegistries;

import java.util.function.Supplier;

public class ChangeHumMessage {
    private String soundPath;

    public ChangeHumMessage() {
    }

    public ChangeHumMessage(String soundPath) {
        this.soundPath = soundPath;
    }

    public static void encode(ChangeHumMessage mes, PacketBuffer buf) {
        buf.writeString(mes.soundPath);
    }

    public static ChangeHumMessage decode(PacketBuffer buf) {
        // PacketBuffer#readString(maximum length of the string)
        return new ChangeHumMessage(buf.readString(25));
    }

    public static void handle(ChangeHumMessage mes, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(()->
                TardisHelper.getConsole(ctx.get().getSender().dimension).getInteriorManager().toggleHum(TardisRegistries.HUM_REGISTRY.getValue(new ResourceLocation(Tardis.MODID, mes.soundPath)))
        );
        ctx.get().setPacketHandled(true);
    }
}

