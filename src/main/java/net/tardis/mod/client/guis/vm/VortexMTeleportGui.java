package net.tardis.mod.client.guis.vm;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMTeleportMessage;


public class VortexMTeleportGui extends Screen{
	
	private static final ResourceLocation BACKGROUND = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui_function.png");
	
	public TextFieldWidget xCoord;
	public TextFieldWidget yCoord;
	public TextFieldWidget zCoord;
	private Button teleport;
	private Button back;
	private Button teleportSetting;
	private boolean preciseTeleport = false;
	private int settingId = 0;
	
    private final int xOffset = 70;
    private final int yOffset = 70;
    
    private int centreX = Minecraft.getInstance().mainWindow.getScaledWidth() / 2;
    private int centreY = Minecraft.getInstance().mainWindow.getScaledHeight() / 2;
    private int guiLeft = centreX - xOffset;
    private int guiTop = centreY - yOffset;


	private Minecraft mc;
	public FontRenderer fr;
	
	public VortexMTeleportGui(ITextComponent title) {
		super(title);
	}
	
	public VortexMTeleportGui() {
		this(new StringTextComponent("Teleport"));
		mc = Minecraft.getInstance();
		fr = mc.fontRenderer;
	}
	
	
	@Override
	public void init() {
		super.init();
		String teleportTypeTop = new TranslationTextComponent("setting.vm.teleport.top").getFormattedText();
		String teleportTypePrecise = new TranslationTextComponent("setting.vm.teleport.precise").getFormattedText();
		String teleportButton = new TranslationTextComponent("button.vm.teleport").getFormattedText();
		String backButton = new TranslationTextComponent("button.vm.back").getFormattedText();
		final int btnH = 20;
		
		xCoord = new TextFieldWidget(fr, guiLeft + 45, guiTop + 75, 50, fr.FONT_HEIGHT, "");
		yCoord = new TextFieldWidget(fr, guiLeft + 45, guiTop + 90, 50, fr.FONT_HEIGHT, "");
		zCoord = new TextFieldWidget(fr, guiLeft + 45, guiTop + 105, 50, fr.FONT_HEIGHT, "");
		teleport = new Button(guiLeft + 45,guiTop + 120, fr.getStringWidth(teleportButton) + 10,btnH, teleportButton, new Button.IPressable() {
			
			@Override
			public void onPress(Button button) {
					BlockPos tpPos = new BlockPos(getInt(xCoord.getText(),COORD_TYPE.X), getInt(yCoord.getText(), COORD_TYPE.Y), getInt(zCoord.getText(), COORD_TYPE.Z));
					Network.INSTANCE.sendToServer(new VMTeleportMessage(tpPos, mc.player.getEntityId(), preciseTeleport));
			}
		});
		back = new Button(guiLeft,guiTop + 30, fr.getStringWidth(backButton) + 8, fr.FONT_HEIGHT + 11, backButton, new Button.IPressable() {
			
			@Override
			public void onPress(Button button) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
			}
		});
		teleportSetting = new Button(guiLeft + 170, guiTop + 50, 55, fr.FONT_HEIGHT + 11, teleportTypeTop, new Button.IPressable() {
			
			@Override
			public void onPress(Button button) {
				incrementId();
				switch(settingId) {
				case 0:
					teleportSetting.setMessage(teleportTypeTop);
					setTeleportType(!preciseTeleport);
					break;
				case 1:
					teleportSetting.setMessage(teleportTypePrecise);
					setTeleportType(!preciseTeleport);
					break;
				}
					
			}
		});
		
		this.buttons.clear();
		this.addButton(teleport);
		this.addButton(xCoord); //addButton also adds other widget types, mapping name can be misleading
		this.addButton(yCoord);
		this.addButton(zCoord);
		this.addButton(back);
		this.addButton(teleportSetting);
		xCoord.setFocused2(true);
	}
	
	@Override
	public void renderBackground(int background) {
		int sWidth = 241;
		int sHeight = 142;
		
		GlStateManager.pushMatrix();
		GlStateManager.enableAlphaTest();
		GlStateManager.enableBlend();
		Minecraft.getInstance().getTextureManager().bindTexture(BACKGROUND);
		blit(guiLeft, guiTop, 0, 0, sWidth, sHeight);
		GlStateManager.popMatrix();
	}
	
	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        super.render(mouseX, mouseY, partialTicks);
        drawCenteredString(fr, "Teleport Player", guiLeft + 73, guiTop + 35, 0xFFFFFF);
		drawCenteredString(fr, "X:", guiLeft + 35, guiTop + 75, 0xFFFFFF);
		drawCenteredString(fr, "Y:", guiLeft + 35, guiTop + 90, 0xFFFFFF);
		drawCenteredString(fr, "Z:", guiLeft + 35, guiTop + 105, 0xFFFFFF);
		drawCenteredString(fr, "Teleport Type:", guiLeft + 198, guiTop + 35, 0xFFFFFF);
		if (this.preciseTeleport) {
			drawCenteredString(fr,"Warning!", guiLeft + 198, guiTop + 75, 0xffcc00);
			drawCenteredString(fr,"This teleport", guiLeft + 198, guiTop + 85, 0xFFFFFF);
			drawCenteredString(fr,"setting can be", guiLeft + 196, guiTop + 95, 0xFFFFFF);
			drawCenteredString(fr,"dangerous!", guiLeft + 198, guiTop + 105, 0xFFFFFF);
		}
		
		
    }
	
	@Override
	public boolean shouldCloseOnEsc() {
		return true;
	}
		
	@Override
	public void onClose() {
        this.minecraft.displayGuiScreen(null);
	}
	
	@Override
	public boolean isPauseScreen() {
	   return true;
	}
	
	public enum COORD_TYPE {
			X, Y, Z
	}
	
	private int getInt(String num, COORD_TYPE type) {
			if (num != null && !num.isEmpty()) {
				int i;
				try {
					i = Integer.parseInt(num);
				} catch (Exception e) {
					i = 0;
				}
				return i;
			} 
			else {
				switch (type) {
					case X:
						return (int) mc.player.posX;
					case Y:
						return (int) mc.player.posY;
					case Z:
						return (int) mc.player.posZ;
					default:
						return 0;
			}
		}
	}
	
	public boolean getTeleportType() {
		return preciseTeleport;
	}
	
	public void setTeleportType(boolean type) {
		this.preciseTeleport = type;
	}
	
	public void incrementId() {
		if (settingId + 1 > 1) {
			settingId = 0;
        } else {
            ++settingId;
        }
	}
}
