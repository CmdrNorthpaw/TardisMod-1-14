package net.tardis.mod.client.renderers.monitor;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.monitors.MonitorTile;

public class MonitorRenderer extends TileEntityRenderer<MonitorTile> {
	
	private static WorldText monitor = new WorldText(0.75F, 0.5F, 0.006F, 0xFFFFFF);
	
	//TODO: Clean this up
	@Override
	public void render(MonitorTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		if(tile.getInfo() == null)
			return;
		
		GlStateManager.pushMatrix();
		Minecraft.getInstance().gameRenderer.disableLightmap();
		GlStateManager.color3f(1F, 1F, 1F);
		GlStateManager.translated(x + 0.5, y + 1, z + 0.5);
		
		Direction facing = tile.getWorld().getBlockState(tile.getPos()).get(BlockStateProperties.HORIZONTAL_FACING);
		boolean isHanging = tile.getWorld().getBlockState(tile.getPos()).get(BlockStateProperties.HANGING);
		if (isHanging) { //If in hanging state
			if (tile.getBlockState().getBlock().equals(TBlocks.steampunk_monitor)) {
				if (facing == Direction.NORTH) {
					GlStateManager.rotatef(-23F, 1F, 0, 0);
				}
				else if (facing == Direction.EAST) {
					GlStateManager.rotatef(-23F, 0, 0, 1);
				}
				else if (facing == Direction.SOUTH) {
					GlStateManager.rotatef(23F, 1, 0, 0);
				}
				else if (facing == Direction.WEST) {
					GlStateManager.rotatef(23F, 0, 0, 1);		
				}
				GlStateManager.rotatef(-Helper.getAngleFromFacing(facing), 0, 1, 0);
				GlStateManager.translated(0, 0, -3.1 / 16.0);
			}
			else  { //Have to do this check, because both blocks have the hanging property...
				GlStateManager.rotatef(-Helper.getAngleFromFacing(facing),0, 1,0);
			}
		}
		else { //When in Standing mode or wall mode
			GlStateManager.rotatef(-Helper.getAngleFromFacing(facing),0, 1,0);
		}
		GlStateManager.rotated(180, 0, 0, 1); // Rotates text 180 degrees - so it's the right way up
		GlStateManager.translated(tile.startX, tile.startY, tile.startZ);
		monitor.renderMonitor(tile.getInfo());
		Minecraft.getInstance().gameRenderer.enableLightmap();
		GlStateManager.popMatrix();
	}
	
	@Override
	protected void bindTexture(ResourceLocation location) {
		super.bindTexture(location);
	}
}
