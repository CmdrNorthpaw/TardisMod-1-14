package net.tardis.mod.helper;

import java.util.Iterator;
import java.util.UUID;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.tileentities.ConsoleTile;

/*
 * A helper for Tardis - specific things.
 */
public class TardisHelper {
	
	public static final BlockPos TARDIS_POS = new BlockPos(0, 128, 0).toImmutable();
	
	/*
	 * Server side only please
	 */
	public static ConsoleTile getConsole(DimensionType interior) {
		try {
			ServerWorld world = DimensionManager.getWorld(ServerLifecycleHooks.getCurrentServer(), interior, true, true);
			if(world == null)
				throw new Exception("World is not loadable!");
			TileEntity te = world.getTileEntity(TARDIS_POS);
			if(te instanceof ConsoleTile) {
				return (ConsoleTile)te;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean isInOwnedTardis(PlayerEntity player) {
		return player.dimension.getRegistryName().getPath().equals(player.getUniqueID().toString());
	}
	
	public static boolean isInTardis(PlayerEntity player) {
		return player.dimension.getModType() == TDimensions.TARDIS;
	}
	
	public static DimensionType setupPlayersTARDIS(ServerPlayerEntity player) {
		DimensionType tardis = TDimensions.registerOrGet(player.getUniqueID().toString(), TDimensions.TARDIS);
		ServerWorld world = ServerLifecycleHooks.getCurrentServer().getWorld(tardis);
		if(world != null && !(world.getTileEntity(TARDIS_POS) instanceof ConsoleTile)) {
			world.setBlockState(TARDIS_POS, TBlocks.console_steam.getDefaultState());
		}
		ConsoleRoom.STEAM.spawnConsoleRoom(world);
		return tardis;
	}
	
	public static boolean hasTARDIS(MinecraftServer server, UUID id) {
		Iterator<DimensionType> it = DimensionType.getAll().iterator();
		while(it.hasNext()) {
			DimensionType type = it.next();
			if(DimensionType.getKey(type).getPath().equals(id.toString())) {
				ServerWorld world = server.getWorld(type);
				if(world != null && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile)
					return true;
			}
		}
		return false;
	}

}
