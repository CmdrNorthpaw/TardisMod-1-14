package net.tardis.mod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.block.BlockState;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.minecraftforge.server.permission.PermissionAPI;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.BrokenExteriorBlock;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.exceptions.NoPlayerFoundException;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TeleportUtil;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisCommand {
    private static final String name = "tardis";

    public static void register(CommandDispatcher<CommandSource> dispatcher){
        dispatcher.register(Commands.literal(name)
            .then(Commands.literal("interior")
                .then(Commands.argument("username", StringArgumentType.string())
                    .suggests((ctx, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                        .executes(ctx -> interior(ctx.getSource(), StringArgumentType.getString(ctx, "username")))))
            .then(Commands.literal("exterior")
                .then(Commands.argument("username", StringArgumentType.string())
                    .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                        .executes(ctx -> exterior(ctx.getSource(), StringArgumentType.getString(ctx, "username")))))
            .then(Commands.literal("create")
            	.executes(ctx -> create(ctx.getSource())))
        );
    }

    private static int exterior(CommandSource source, String username) {
        try{
            ServerPlayerEntity sourcePlayer = source.asPlayer();
            if(!PermissionAPI.hasPermission(sourcePlayer, PermissionEnum.EXTERIOR.getNode())){
                source.sendErrorMessage(new StringTextComponent("You do not have permission to do that."));
                return Command.SINGLE_SUCCESS;
            }

            String playerUUID = PlayerHelper.getOnlinePlayerUUID(username).toString();
            DimensionType consoleDimension = DimensionType.byName(new ResourceLocation(Tardis.MODID, playerUUID));

            ConsoleTile console = TardisHelper.getConsole(consoleDimension);
            if(console != null){
                BlockPos tardisLocation = console.getLocation();
                DimensionType tardisDimension = console.getDimension();
                TeleportUtil.teleportEntity(sourcePlayer, tardisDimension, tardisLocation.getX() + 2, tardisLocation.getY(), tardisLocation.getZ());
            }
            else {
                source.sendErrorMessage(new StringTextComponent("Error: No TARDIS Found for this player"));
            }

        } catch (CommandSyntaxException e) {
            source.sendErrorMessage(new StringTextComponent("Error: You do not exist in this world!"));
        } catch (NoPlayerFoundException e) {
            source.sendErrorMessage(new StringTextComponent(e.getMessage()));
        }
        return Command.SINGLE_SUCCESS;
    }
    
    private static int create(CommandSource source) {
    	try {
			ServerPlayerEntity player = source.asPlayer();
            if(!PermissionAPI.hasPermission(player, PermissionEnum.CREATE.getNode())){
                source.sendErrorMessage(new StringTextComponent("You do not have permission to do that."));
                return Command.SINGLE_SUCCESS;
            }
            
            DimensionType interior = TardisHelper.setupPlayersTARDIS((ServerPlayerEntity)player);

			BlockPos pos = player.getPosition().up().offset(player.getHorizontalFacing(), 2);
			player.world.setBlockState(pos, TBlocks.broken_exterior.getDefaultState());
			TileEntity te = player.world.getTileEntity(pos);
			if(te instanceof BrokenExteriorTile) {
				BrokenExteriorTile brokeBoi = (BrokenExteriorTile)te;
				brokeBoi.setConsoleDimension(interior);
				brokeBoi.increaseLoyalty(101);
				BlockState state = player.world.getBlockState(pos);
				((BrokenExteriorBlock)state.getBlock()).replace(player.world, pos, interior, state.get(BlockStateProperties.HORIZONTAL_FACING));
			}
		}
    	catch (CommandSyntaxException e) {
			source.sendErrorMessage(new StringTextComponent("You are not a player, good sir / madam / other"));
		}
        return Command.SINGLE_SUCCESS;
    }

    private static int interior(CommandSource source, String username){
        try {
            ServerPlayerEntity sourcePlayer = source.asPlayer();
            if(!PermissionAPI.hasPermission(sourcePlayer, PermissionEnum.INTERIOR.getNode())){
                source.sendErrorMessage(new StringTextComponent("You do not have permission to do that."));
                return Command.SINGLE_SUCCESS;
            }

            String playerUUID = PlayerHelper.getOnlinePlayerUUID(username).toString();
            DimensionType consoleDimension = DimensionType.byName(new ResourceLocation(Tardis.MODID, playerUUID));

            TeleportUtil.teleportEntity(sourcePlayer, consoleDimension, TardisHelper.TARDIS_POS.getX() + 2, TardisHelper.TARDIS_POS.getY(), TardisHelper.TARDIS_POS.getZ());
        } catch (NoPlayerFoundException e) {
            source.sendErrorMessage(new StringTextComponent(e.getMessage()));
        } catch (CommandSyntaxException e) {
            source.sendErrorMessage(new StringTextComponent("Error : You're not a player !"));
        }
        return Command.SINGLE_SUCCESS;
    }
}
