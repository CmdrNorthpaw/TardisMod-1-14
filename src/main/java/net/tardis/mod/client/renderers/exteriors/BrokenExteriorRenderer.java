package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class BrokenExteriorRenderer extends TileEntityRenderer<BrokenExteriorTile> {

	public static SteamExteriorModel model = new SteamExteriorModel();
	public static final ResourceLocation TEXTURE = SteampunkExteriorRenderer.TEXTURE;
	
	public BrokenExteriorRenderer() {}

	@Override
	public void render(BrokenExteriorTile te, double x, double y, double z, float partialTicks, int destroyStage) {
		
		int ticks = Minecraft.getInstance().player.ticksExisted;
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y - 0.62, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		if(te.getWorld().getBlockState(te.getPos()).has(BlockStateProperties.HORIZONTAL_FACING)) {
			Direction dir = te.getWorld().getBlockState(te.getPos()).get(BlockStateProperties.HORIZONTAL_FACING);
			GlStateManager.rotated(dir.getHorizontalAngle() - 180, 0, 1, 0);
		}
		
		this.bindTexture(TEXTURE);
		model.renderDoor(0, (float)Math.sin((ticks % 60) * 0.1));
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}
}
