package net.tardis.mod.experimental.detective;

/**
 * Created by Swirtzly
 * on 08/04/2020 @ 10:34
 */

public @interface NeedsTranslated {
    String value() default "";
}
