package net.tardis.mod.tileentities.exteriors;

import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.TTiles;

public class ClockExteriorTile extends ExteriorTile{
	
	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, 0, -2, 2, 5, 2);
	
	public ClockExteriorTile() {
		super(TTiles.EXTERIOR_CLOCK);
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

	//TODO: make this right
	@Override
	public AxisAlignedBB getDoorAABB() {
		return this.getRenderBoundingBox().offset(this.getPos());
	}

}
