package net.tardis.mod.boti;

import net.minecraft.block.BlockState;
import net.minecraft.fluid.FluidState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockStore {

	BlockState state = null;
	FluidState fluidState = null;
	int lightVal = 0;
	CompoundNBT tileTag = null;
	
	public BlockStore(BlockState state, FluidState fluidState, int lightValue, TileEntity te) {
		this.state = state;
		this.fluidState = fluidState;
		this.lightVal = lightValue;
		this.tileTag = te.serializeNBT();
	}
	
	@OnlyIn(Dist.CLIENT)
	public TileEntity getTile() {
		return this.tileTag == null ? null : TileEntity.create(this.tileTag);
	}
	
	public BlockState getState() {
		return this.state;
	}
	
	public int getLight() {
		return this.lightVal;
	}
	
	public FluidState getFluidState() {
		return this.fluidState;
	}
	
}
