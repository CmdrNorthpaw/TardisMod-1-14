package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;

public class SubsystemEntry<T extends Subsystem> implements IRegisterable<SubsystemEntry<?>>{

	private ResourceLocation name;
	private IFactory<T> fact;
	private Item item;
	
	public SubsystemEntry(IFactory<T> fact, Item item){
		this.fact = fact;
		this.item = item;
	}
	
	public T create(ConsoleTile console) {
		T sub = this.fact.create(console, item);
		sub.setRegistryName(name);
		return sub;
	}
	
	@Override
	public SubsystemEntry<?> setRegistryName(ResourceLocation regName) {
		this.name = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return name;
	}
	
	public static interface IFactory<T extends Subsystem>{
		T create(ConsoleTile console, Item key);
	}

}
