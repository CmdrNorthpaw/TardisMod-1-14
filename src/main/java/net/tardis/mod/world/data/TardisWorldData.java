package net.tardis.mod.world.data;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.dimensions.TDimensions;

public class TardisWorldData extends WorldSavedData {

	public TardisWorldData(String name) {
		super(name);
		this.markDirty();
	}
	
	public TardisWorldData() {
		this("tardis");
	}

	@Override
	public void read(CompoundNBT nbt) {
		ListNBT list = nbt.getList("dimensions", NBT.TAG_STRING);
		if(list == null) return;
		TDimensions.TARDIS_DIMENSIONS.clear();
		for(INBT base : list) {
			TDimensions.TARDIS_DIMENSIONS.add(new ResourceLocation(((StringNBT)base).getString()));
			System.out.println("Read: " + ((StringNBT)base).getString());
		}
		System.out.println("Read complete!");
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT list = new ListNBT();
		for(ResourceLocation loc : TDimensions.TARDIS_DIMENSIONS) {
			list.add(new StringNBT(loc.toString()));
			System.out.println("Writing: " + loc.toString());
		}
		compound.put("dimensions", list);
		System.out.println("Write complete!");
		return compound;
	}

}
