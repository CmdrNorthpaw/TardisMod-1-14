package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.sounds.TSounds;


public class VMTeleportMessage {

	public BlockPos pos;
	public int value;
	public boolean teleportPrecise;
	
	public VMTeleportMessage(BlockPos pos, int value, boolean teleportPrecise) {
		super();
		this.pos = pos;
		this.value = value;
		this.teleportPrecise = teleportPrecise;
	}


	public static void encode(VMTeleportMessage mes,PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeInt(mes.value);
		buf.writeBoolean(mes.teleportPrecise);
	}
	
	public static VMTeleportMessage decode(PacketBuffer buf) {
		return new VMTeleportMessage(buf.readBlockPos(),buf.readInt(),buf.readBoolean());
	}
	
        public static void handle(VMTeleportMessage mes,  Supplier<NetworkEvent.Context> ctx)
        {
        	ctx.get().enqueueWork(()->{
        		 	ServerPlayerEntity sender = ctx.get().getSender();
        		 	if (!LandingSystem.isPosBelowOrAboveWorld(sender.world.getDimension().getType(),mes.pos.getY()) 
        		 			&& LandingSystem.isBlockBehindWorldBorder(sender.getServerWorld(), mes.pos.getX(),mes.pos.getY(),mes.pos.getZ())) {
        		 		if (mes.teleportPrecise == true) {
    		 				sender.connection.setPlayerLocation(mes.pos.getX(), mes.pos.getY(), mes.pos.getZ(), 0, 0);
    		 			}
    		 			else {
    		 				BlockPos pos = LandingSystem.getTopBlock(sender.world, mes.pos);
            		 		sender.connection.setPlayerLocation(pos.getX(), pos.getY(), pos.getZ(), 0, 0);
                		 	
    		 			}
    		 			sender.world.playSound(null, ctx.get().getSender().getPosition(),TSounds.VM_TELEPORT , SoundCategory.PLAYERS, 1F, 1F);
        		 	}
        		 	else {
        		 		sender.sendStatusMessage(new TranslationTextComponent("message.vm.invalidPos"), false);
        		 	}
        		 			
        	});
        	ctx.get().setPacketHandled(true);
        }


}
