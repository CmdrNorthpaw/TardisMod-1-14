package net.tardis.mod.controls;

import java.util.ArrayList;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.DimensionManager;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class DimensionControl extends BaseControl {
	
	private static final String MESSAGE = "message.tardis.control.dimchange";
	private ArrayList<DimensionType> dimList = new ArrayList<DimensionType>();
	private int index = 0;
	
	public DimensionControl(ConsoleTile console) {
		super(console);
		createDimList();
	}
	
	@Override
	public EntitySize getSize() {
		ConsoleTile console = this.getConsole();
		if(console instanceof NemoConsoleTile)
			return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);
		return EntitySize.flexible(6 / 16.0F, 2 / 16.0F);
	}
	

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(0, 12 / 16.0, 8 / 16.0);
		return new Vec3d(-0.25 / 16.0, 7 / 16.0, 12 / 16.0);
	}
	
	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		return this.doDimChangeAction(console, player);
	}
	
	@Override
	public void onHit(ConsoleTile console, PlayerEntity player) {
		this.doDimChangeAction(console, player);
	}
	
	private boolean doDimChangeAction(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			this.createDimList();
			if(!this.dimList.isEmpty()) {
				this.modIndex(player.isSneaking() ? -1 : 1);
				DimensionType type = this.dimList.get(index);
				console.setDestination(type, console.getDestination());
				player.sendStatusMessage(new TranslationTextComponent(MESSAGE, Helper.formatDimName(type)), true);
				this.setAnimationTicks(20);
			}
			else index = 0;
		}
		return true;
	}
	
	private void modIndex(int i) {
		if(this.index + i >= this.dimList.size()) {
			this.index = 0;
			return;
		}
		if(this.index + i < 0) {
			this.index = this.dimList.size() - 1;
			return;
		}
		this.index += i;
	}
	
	private void createDimList(){
		dimList = new ArrayList<DimensionType>();
		for(DimensionType type : DimensionManager.getRegistry()) {
			if(Helper.canTravelToDimension(type))
				dimList.add(type);
		}
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.DIMENSION;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

}
