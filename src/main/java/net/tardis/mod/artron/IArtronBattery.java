package net.tardis.mod.artron;

import net.minecraft.item.ItemStack;

public interface IArtronBattery {

	float charge(ItemStack stack, float amount);
	float discharge(ItemStack stack, float amount);
	
	float getMaxCharge(ItemStack stack);
	float getCharge(ItemStack stack);
}
