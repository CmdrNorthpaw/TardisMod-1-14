package net.tardis.mod.sounds;

import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.tileentities.ConsoleTile;

public class SoundSchemeBasic extends SoundSchemeBase {

	private SoundEvent land = TSounds.TARDIS_LAND;
	private SoundEvent takeoff = TSounds.TARDIS_TAKEOFF;
	
	@Override
	public void playFlightLoop(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), TSounds.TARDIS_FLY_LOOP, SoundCategory.BLOCKS, 0.25F, 1F);
	}

	@Override
	public void playInteriorTakeOff(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), TSounds.TARDIS_TAKEOFF, SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public void playExteriorTakeOff(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getDimension());
		if(world != null)
			world.playSound(null, console.getLocation(), takeoff, SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public void playInteriorLand(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), TSounds.TARDIS_LAND, SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public void playExteriorLand(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getDestinationDimension());
		if(world != null)
			world.playSound(null, console.getDestination(), land, SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public int getLoopTime() {
		return 32;
	}

	@Override
	public int getLandTime() {
		return 200;
	}

	@Override
	public int getTakeoffTime() {
		return 200;
	}

	@Override
	public void playTakeoffSounds(ConsoleTile console) {
		this.playInteriorTakeOff(console);
		this.playExteriorTakeOff(console);
	}

	@Override
	public void playLandSounds(ConsoleTile console) {
		this.playExteriorLand(console);
		this.playInteriorLand(console);
	}

	@Override
	public void playInteriorLandAfter(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), TSounds.REACHED_DESTINATION, SoundCategory.BLOCKS, 1F, 1F);
	}

}
