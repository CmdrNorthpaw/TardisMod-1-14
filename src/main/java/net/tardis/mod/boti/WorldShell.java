package net.tardis.mod.boti;

import java.util.HashMap;
import java.util.Map.Entry;

import net.minecraft.block.BlockState;
import net.minecraft.fluid.IFluidState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IEnviromentBlockReader;
import net.minecraft.world.LightType;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class WorldShell implements IEnviromentBlockReader{
	
	private HashMap<BlockPos, BlockStore> BLOCKS = new HashMap<>();
	private HashMap<BlockPos, TileEntity> TES = new HashMap<>();
	
	public void put(BlockPos pos, BlockStore store) {
		BLOCKS.put(pos, store);
	}
	
	public BlockStore get(BlockPos pos) {
		return BLOCKS.get(pos);
	}

	@Override
	public TileEntity getTileEntity(BlockPos pos) {
		return TES.get(pos);
	}

	@Override
	public BlockState getBlockState(BlockPos pos) {
		return BLOCKS.get(pos).getState();
	}

	@Override
	public IFluidState getFluidState(BlockPos pos) {
		return BLOCKS.get(pos).getFluidState();
	}

	@Override
	public Biome getBiome(BlockPos pos) {
		return Biomes.THE_VOID;
	}

	@Override
	public int getLightFor(LightType type, BlockPos pos) {
		return BLOCKS.get(pos).getLight();
	}
	
	@OnlyIn(Dist.CLIENT)
	public void setupTEs(){
		for(Entry<BlockPos, BlockStore> entry : BLOCKS.entrySet()) {
			if(entry.getValue().getTile() != null) {
				TES.put(entry.getKey(), entry.getValue().getTile());
			}
		}
	}
}
