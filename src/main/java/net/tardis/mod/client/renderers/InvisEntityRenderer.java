package net.tardis.mod.client.renderers;

import javax.annotation.Nullable;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.EntityRayTraceResult;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;

public class InvisEntityRenderer extends EntityRenderer<Entity> {

    public InvisEntityRenderer(EntityRendererManager p_i46179_1_) {
        super(p_i46179_1_);
    }

    @Override
    protected void renderName(Entity entity, double x, double y, double z) {
        GlStateManager.pushMatrix();
        super.renderName(entity, x, y, z);
        if(entity instanceof ControlEntity &&
        		Minecraft.getInstance().objectMouseOver instanceof EntityRayTraceResult && PlayerHelper.isInEitherHand(Minecraft.getInstance().player, TItems.MANUAL))
        	if(((EntityRayTraceResult)Minecraft.getInstance().objectMouseOver).getEntity() == entity)
        		this.renderLivingLabel(entity, entity.getDisplayName().getFormattedText(), x, y, z, 64);
        GlStateManager.popMatrix();
    }


    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(Entity control) {
        return null;
    }
}
