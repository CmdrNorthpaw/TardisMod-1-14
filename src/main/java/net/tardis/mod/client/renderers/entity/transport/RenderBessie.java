package net.tardis.mod.client.renderers.entity.transport;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.transport.BessieModel;
import net.tardis.mod.entity.BessieEntity;

import javax.annotation.Nullable;

/**
 * Created by Swirtzly
 * on 08/04/2020 @ 23:04
 */
public class RenderBessie extends LivingRenderer<BessieEntity, BessieModel> {

    public RenderBessie(EntityRendererManager rendererManager) {
        super(rendererManager, new BessieModel(), 1);
    }

    @Override
    public void doRender(BessieEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
        super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(BessieEntity entity) {
        return BessieModel.TEXTURE;
    }
}
