package net.tardis.mod.tileentities.monitors;

import java.text.DecimalFormat;
import java.util.ArrayList;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class MonitorTile extends TileEntity implements ITickableTileEntity{

	private String[] info;
	public double startX = 0;
	public double startY = 0;
	public double startZ = 0;
	
	public static DecimalFormat format = new DecimalFormat("###");
	
	public MonitorTile() {
		super(TTiles.STEAMPUNK_MONITOR);
	}
	
	public MonitorTile(TileEntityType<?> tile) {
		super(tile);
	}

	@Override
	public void tick() {
		ConsoleTile console = this.getConsole();
		if(console != null) {
			ArrayList<String> info = new ArrayList<String>();
			info.add("Location: " + Helper.formatBlockPos(console.getLocation()));
			info.add("Dimension: " + Helper.formatDimName(console.getDimension()));
			info.add("Target: " + Helper.formatBlockPos(console.getDestination()));
			info.add("Target Dim: " + Helper.formatDimName(console.getDestinationDimension()));
			info.add("Artron Units: " + format.format(console.getArtron()) + "AU");
			info.add("Journey: " + format.format(console.getPercentageJourney() * 100.0F) + "%");
			this.info = info.toArray(new String[] {});
		}
	}
	
	private ConsoleTile getConsole() {
		TileEntity te = this.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile)
			return (ConsoleTile)te;
		return null;
	}
	
	public void setGuiXYZ(double x, double y, double z) {
		this.startX = x;
		this.startY = y;
		this.startZ = z;
	}
	
	public String[] getInfo() {
		return info;
	}

}
