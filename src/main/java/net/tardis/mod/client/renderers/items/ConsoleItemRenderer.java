package net.tardis.mod.client.renderers.items;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.item.ItemStack;
import net.tardis.mod.client.models.consoles.ModelSteamConsole;
import net.tardis.mod.client.renderers.consoles.SteamConsoleRenderer;

public class ConsoleItemRenderer extends ItemStackTileEntityRenderer{

	private static ModelSteamConsole model = new ModelSteamConsole();
	
	@Override
	public void renderByItem(ItemStack itemStackIn) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0.5, 0.5, 0.5);
		GlStateManager.scaled(0.06, 0.06, 0.06);
		GlStateManager.rotated(180, 0, 0, 1);
		Minecraft.getInstance().getTextureManager().bindTexture(SteamConsoleRenderer.TEXURE);
		model.render(0.0625F);
		GlStateManager.popMatrix();
	}

}
