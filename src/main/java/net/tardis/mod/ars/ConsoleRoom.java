package net.tardis.mod.ars;

import java.util.HashMap;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class ConsoleRoom {

	public static HashMap<ResourceLocation, ConsoleRoom> REGISTRY = new HashMap<ResourceLocation, ConsoleRoom>();
	public static ConsoleRoom TOYOTA;
	public static ConsoleRoom STEAM;
	public static ConsoleRoom JADE;
	public static ConsoleRoom NAUTILUS;
	public static ConsoleRoom OMEGA;
	
	private ResourceLocation registryName;
	private ResourceLocation texture;
	private BlockPos offset;
	private ResourceLocation file;
	private String displayName;
	
	public ConsoleRoom(BlockPos offset, ResourceLocation file, ResourceLocation texture, String displayName) {
		this.offset = offset;
		this.file = file;
		this.texture = texture;
		this.displayName = displayName;
	}
	
	public ConsoleRoom(BlockPos offset, String file, String texture, String displayName) {
		this(offset,
				new ResourceLocation(Tardis.MODID, "tardis/structures/" + file),
				new ResourceLocation(Tardis.MODID, "textures/gui/interiors/" + texture + ".png"), 
				displayName);
	}
	
	
	public ConsoleRoom setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}
	
	public ResourceLocation getRegistryName() {
		return registryName;
	}
	
	public ResourceLocation getTexture() {
		return this.texture;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
	
	public void spawnConsoleRoom(ServerWorld world) {
		ConsoleTile console = null;
		CompoundNBT consoleData = null;
		BlockState consoleState = null;
		
		//Save the console
		if(world.getTileEntity(TardisHelper.TARDIS_POS) instanceof ConsoleTile) {
			console = (ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS);
			consoleData = console.serializeNBT();
			consoleState = world.getBlockState(TardisHelper.TARDIS_POS);
		}
		
		//Delete all old blocks
		BlockPos clearRadius = new BlockPos(20, 20, 20);
		BlockPos.getAllInBox(TardisHelper.TARDIS_POS.subtract(clearRadius), TardisHelper.TARDIS_POS.add(clearRadius)).forEach((pos) -> {
			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
		});
		
		//Kill all non-living entities
		AxisAlignedBB killBox = new AxisAlignedBB(-20, -20, -20, 20, 20, 20).offset(TardisHelper.TARDIS_POS);
		BlockPos tpPos = TardisHelper.TARDIS_POS.offset(Direction.NORTH);
		for(Entity entity : world.getEntitiesWithinAABB(Entity.class, killBox)){
			if(!(entity instanceof LivingEntity))
				entity.remove();
			if(entity instanceof PlayerEntity)
				entity.setPositionAndUpdate(tpPos.getX() + 0.5, tpPos.getY() + 0.5, tpPos.getZ() + 0.5);
			if(entity.getPersistentData().contains("tardis_is_killed"))
				entity.remove();
		}
		
		//Spawn console room
		Template temp = world.getStructureTemplateManager().getTemplate(file);
		temp.addBlocksToWorld(world, TardisHelper.TARDIS_POS.subtract(this.offset), new PlacementSettings().setIgnoreEntities(false));
		//Re-add console
		if(console != null) {
			world.setBlockState(TardisHelper.TARDIS_POS, consoleState);
			((ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS)).deserializeNBT(consoleData);
		}
		
		//Update lighting
		ChunkPos startPos = world.getChunk(TardisHelper.TARDIS_POS.subtract(temp.getSize())).getPos();
		ChunkPos endPos = world.getChunk(TardisHelper.TARDIS_POS.add(temp.getSize())).getPos();
		
		for(int x = startPos.x; x < endPos.x; ++x) {
			for(int z = startPos.z; z < endPos.z; ++z) {
				world.getChunkProvider().getLightManager().lightChunk(world.getChunk(x, z), true);
			}
		}
	}
	
	public static ConsoleRoom register(ConsoleRoom room, ResourceLocation registryName) {
		REGISTRY.put(registryName, room.setRegistryName(registryName));
		return room;
	}
	
	public static ConsoleRoom register(ConsoleRoom room, String registryName) {
		return ConsoleRoom.register(room, new ResourceLocation(Tardis.MODID, registryName));
	}
    
    @SubscribeEvent
    public static void registerConsoleRooms(FMLCommonSetupEvent event) {
    	ConsoleRoom.STEAM = register(new ConsoleRoom(new BlockPos(13, 9, 12), "interior_steam", "steam", "Steam"), "interior_steam");
        ConsoleRoom.JADE = register(new ConsoleRoom(new BlockPos(7, 11, 9), "interior_jade", "jade", "Jade"), "interior_jade");
        ConsoleRoom.NAUTILUS = register(new ConsoleRoom(new BlockPos(15, 13, 11), "interior_nautilus", "nautilus","Nautilus"), "nautilus");
        ConsoleRoom.OMEGA = register(new ConsoleRoom(new BlockPos(15, 16, 15), "interior_omega", "omega", "Omega"), "omega");
    }
}
