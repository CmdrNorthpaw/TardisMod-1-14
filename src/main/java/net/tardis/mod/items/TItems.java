package net.tardis.mod.items;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.sonic.ISonicPart;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TItems {

	/**
	 * Object holders are trash
	 */
	public static Item CIRCUITS = null;
	public static Item KEY = null;
	public static Item SONIC = null;
	public static Item INT_DOOR = null;
	public static Item KEY_01 = null;
	public static Item ATRIUM_UPGRADE = null;
	public static Item BLANK_UPGRADE = null;

	public static Item VORTEX_MANIP = null;
	public static Item VM_MODULE = null;
	public static Item VM_STRAP = null;
	
	public static Item STATTENHEIM_REMOTE = null;

	public static Item DEMAT_CIRCUIT = null;
	public static Item THERMOCOUPLING = null;
	public static Item FLUID_LINK = null;
	public static Item CHAMELEON_CIRCUIT = null;
	public static Item MERCURY_BOTTLE = null;
	public static Item CINNABAR = null;
	
	public static Item POCKET_WATCH = null;
	
	public static DalekSpawnItem SPAWN_EGG_DALEK_TIMEWAR = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_SEC = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_CLASSIC = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_SW = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_SUPREME = null;
	public static DalekSpawnItem SPAWN_EGG_DALEK_IMPERIAL = null;

	public static Item EARTHSHOCK_GUN = null;
	
	public static Item ANTIDEPRESSANT = null;
	public static Item DEPRESSANT = null;

	public static Item SONIC_EMITTER = null;
	public static Item SONIC_HANDLE = null;
	public static Item SONIC_ACTIVATOR = null;
	public static Item SONIC_END = null;
	
	public static Item BATTERY = null;
	
	public static Item MANUAL = null;


	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(
				KEY = createItem(new KeyItem(), "tardis_key"),
				INT_DOOR = createItem(new SpawnerItem(TEntities.DOOR), "int_door"),
				KEY_01 = createItem(new KeyItem(),"key_01"),
				VORTEX_MANIP = createItem(new VortexManipItem(),"vm"),
				VM_MODULE = createItem(new BaseItem(),"vm_module"),
				VM_STRAP = createItem(new BaseItem(),"vm_strap"),
				POCKET_WATCH = createItem(new PocketWatchItem(), "pocket_watch"),
				STATTENHEIM_REMOTE = createItem(new StatRemoteItem(Prop.Items.ONE.group(TItemGroups.MAIN)),"stattenheim_remote"),
				
				ATRIUM_UPGRADE = createItem(new BaseItem(Prop.Items.SIXTEEN.group(TItemGroups.MAINTENANCE)), "atrium_upgrade"),
				BLANK_UPGRADE = createItem(new BaseItem(Prop.Items.SIXTY_FOUR.group(TItemGroups.MAINTENANCE)), "blank_upgrade"),
				
				CIRCUITS = createItem(new BaseItem(Prop.Items.SIXTY_FOUR.group(TItemGroups.MAINTENANCE)), "circuits"),
				DEMAT_CIRCUIT = createItem(new TardisPartItem(),"dematerialisation_circuit"),
				THERMOCOUPLING = createItem(new TardisPartItem(),"thermocoupling"),
				FLUID_LINK = createItem(new TardisPartItem(),"fluid_link"),
				CHAMELEON_CIRCUIT = createItem(new TardisPartItem(),"chameleon_circuit"),
				MERCURY_BOTTLE = createItem(new BaseItem(Prop.Items.SIXTY_FOUR.group(TItemGroups.MAINTENANCE)), "mercury_bottle"),
				CINNABAR = createItem(new BaseItem(Prop.Items.SIXTY_FOUR.group(TItemGroups.MAINTENANCE)),"cinnabar"),
				BATTERY = createItem(new BatteryItem(),"battery"),

				SPAWN_EGG_DALEK_TIMEWAR = createItem(new DalekSpawnItem(), "dalek_timewar"),
				SPAWN_EGG_DALEK_SEC = createItem(new DalekSpawnItem(), "dalek_sec"),
				SPAWN_EGG_DALEK_SW = createItem(new DalekSpawnItem(), "dalek_special_weapons"),
				SPAWN_EGG_DALEK_CLASSIC = createItem(new DalekSpawnItem(), "dalek_classic"),
				SPAWN_EGG_DALEK_SUPREME = createItem(new DalekSpawnItem(), "dalek_supreme"),
				SPAWN_EGG_DALEK_IMPERIAL = createItem(new DalekSpawnItem(), "dalek_imperial"),
				
				EARTHSHOCK_GUN = createItem(new LaserGunItem(), "earthshock_gun"),
				
				ANTIDEPRESSANT = createItem(new MoodChangeItem(Prop.Items.ONE.group(TItemGroups.MAINTENANCE), 100), "antidepressant"),
				DEPRESSANT = createItem(new MoodChangeItem(Prop.Items.ONE.group(TItemGroups.MAINTENANCE), -100), "depressant"),
				SONIC_EMITTER = createItem(new SonicBasePart(ISonicPart.SonicPart.EMITTER), "sonic_emitter"),
				SONIC_ACTIVATOR = createItem(new SonicBasePart(ISonicPart.SonicPart.ACTIVATOR), "sonic_activator"),
				SONIC_HANDLE = createItem(new SonicBasePart(ISonicPart.SonicPart.HANDLE), "sonic_handle"),
				SONIC_END = createItem(new SonicBasePart(ISonicPart.SonicPart.END), "sonic_end"),
				SONIC = createItem(new SonicItem(), "sonic"),
				
				MANUAL = createItem(new ManualItem(), "manual")
		);
		
		for(Item item : TBlocks.ITEMS) {
			event.getRegistry().register(item);
		}
	}

	public static <T extends Item> T createItem(T item, String name){
		item.setRegistryName(Tardis.MODID, name);
		return item;
	}
}
