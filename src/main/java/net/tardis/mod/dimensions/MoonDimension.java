package net.tardis.mod.dimensions;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.biome.provider.SingleBiomeProviderSettings;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;

public class MoonDimension extends Dimension{

	public static SingleBiomeProvider PROVIDER = new SingleBiomeProvider(new SingleBiomeProviderSettings().setBiome(Biomes.FOREST));

	public MoonDimension(World worldIn, DimensionType typeIn) {
		super(worldIn, typeIn);
	}

	@Override
	public ChunkGenerator<?> createChunkGenerator() {
		TardisChunkGenerator gen = new TardisChunkGenerator(this.world, PROVIDER, new TardisChunkGenerator.GeneratorSettingsTardis());
		return gen;
	}

	@Override
	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
		return BlockPos.ZERO;
	}

	@Override
	public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
		return BlockPos.ZERO;
	}

	@Override
	public float calculateCelestialAngle(long worldTime, float partialTicks) {
		return 0F;
	}

	@Override
	public boolean isSurfaceWorld() {
		return false;
	}

	@Override
	public Vec3d getFogColor(float celestialAngle, float partialTicks) {
		return new Vec3d(0, 0, 0);
	}

	@Override
	public boolean canRespawnHere() {
		return false;
	}

	@Override
	public boolean doesXZShowFog(int x, int z) {
		return false;
	}

}
