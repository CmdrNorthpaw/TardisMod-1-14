	package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class FacingControl extends BaseControl{

	private Direction facing = Direction.NORTH;
	
	public FacingControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(3 / 16.0F,  2 / 16.0F);
		return EntitySize.flexible(3 / 16.0F, 5 / 16.0F);
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-9 / 16.0, 8 / 16.0, -9 / 16.0);
		return new Vec3d(-5 / 16.0, 5 / 16.0, 14.5 / 16.0);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		int index = this.getDirection().getHorizontalIndex() + (player.isSneaking() ? -1 : 1);
		if(index < 0)
			index = 3;
		else if(index > 3)
			index = 0;
		
		facing = Direction.byHorizontalIndex(index);
		if(!player.world.isRemote)
			console.setDirection(facing);
			player.sendStatusMessage(new StringTextComponent(new TranslationTextComponent("message.tardis.control.facing").getFormattedText() + " " 
			+ new StringTextComponent(facing.getName().toUpperCase()).applyTextStyle(TextFormatting.LIGHT_PURPLE).getFormattedText()) , true);
		return true;
	}
	
	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.DIRECTION;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
	
	public void setDirection(Direction facing) {
		this.facing = facing;
	}
	
	public Direction getDirection() {
		return this.facing;
	}
	

}
