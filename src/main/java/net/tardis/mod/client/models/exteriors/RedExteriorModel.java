package net.tardis.mod.client.models.exteriors;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class RedExteriorModel extends Model {
	private final RendererModel NorthPanel;
	private final RendererModel GlassPanel;
	private final RendererModel Beams;
	private final RendererModel Vertical2;
	private final RendererModel Horizontal2;
	private final RendererModel Handle;
	private final RendererModel Baseframe;
	private final RendererModel Pillars;
	private final RendererModel Sign;
	private final RendererModel Roof;
	private final RendererModel Edges;
	private final RendererModel EastEdge;
	private final RendererModel SouthEdge;
	private final RendererModel WestEdge;
	private final RendererModel NorthEdge;
	private final RendererModel SouthPanel;
	private final RendererModel Glass_Panel4;
	private final RendererModel Beams4;
	private final RendererModel Vertical7;
	private final RendererModel Horizontal7;
	private final RendererModel Inlay4;
	private final RendererModel Horizontal8;
	private final RendererModel Vertical8;
	private final RendererModel EastPanel;
	private final RendererModel Glass_Panel2;
	private final RendererModel Beams2;
	private final RendererModel Vertical3;
	private final RendererModel Horizontal3;
	private final RendererModel Inlay2;
	private final RendererModel Horizontal4;
	private final RendererModel Vertical4;
	private final RendererModel WestPanel;
	private final RendererModel Glass_Panel3;
	private final RendererModel Beams3;
	private final RendererModel Vertical5;
	private final RendererModel Horizontal5;
	private final RendererModel Inlay3;
	private final RendererModel Horizontal6;
	private final RendererModel Vertical6;

	public RedExteriorModel() {
		textureWidth = 128;
		textureHeight = 128;

		NorthPanel = new RendererModel(this);
		NorthPanel.setRotationPoint(-6.25F, 6.3333F, -7.1667F);
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 78, 16, 0.75F, -13.8333F, -0.5833F, 11, 1, 1, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 72, 16, -0.25F, 12.1667F, -0.8333F, 13, 5, 1, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 68, 0, -0.25F, -13.8333F, -1.0833F, 1, 31, 1, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 78, 16, 0.75F, -13.8333F, -1.0833F, 11, 1, 1, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 68, 0, 11.75F, -13.8333F, -1.0833F, 1, 31, 1, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 68, 0, 11.75F, -14.3333F, 0.1667F, 1, 27, 0, 0.0F, false));
		NorthPanel.cubeList.add(new ModelBox(NorthPanel, 68, 0, -0.25F, -13.8333F, 0.1667F, 1, 26, 0, 0.0F, false));

		GlassPanel = new RendererModel(this);
		GlassPanel.setRotationPoint(6.25F, 17.6667F, 7.1667F);
		NorthPanel.addChild(GlassPanel);
		GlassPanel.cubeList.add(new ModelBox(GlassPanel, 78, 40, -6.0F, -30.0F, -7.75F, 12, 24, 0, 0.0F, false));

		Beams = new RendererModel(this);
		Beams.setRotationPoint(-2.0F, -17.5F, -7.75F);
		GlassPanel.addChild(Beams);

		Vertical2 = new RendererModel(this);
		Vertical2.setRotationPoint(0.0F, 0.0F, 0.25F);
		setRotationAngle(Vertical2, 0.0F, -0.7854F, 0.0F);
		Beams.addChild(Vertical2);
		Vertical2.cubeList.add(new ModelBox(Vertical2, 0, 40, 4.8033F, -13.5F, -5.705F, 1, 25, 1, 0.0F, false));
		Vertical2.cubeList.add(new ModelBox(Vertical2, 0, 40, -0.5F, -13.5F, -0.5F, 1, 25, 1, 0.0F, false));
		Vertical2.cubeList.add(new ModelBox(Vertical2, 0, 40, 2.3284F, -13.5F, -3.3284F, 1, 25, 1, 0.0F, false));
		Vertical2.cubeList.add(new ModelBox(Vertical2, 0, 40, -2.9749F, -13.5F, 2.0732F, 1, 25, 1, 0.0F, false));

		Horizontal2 = new RendererModel(this);
		Horizontal2.setRotationPoint(2.0F, 8.0F, 0.25F);
		setRotationAngle(Horizontal2, -0.7854F, 0.0F, 0.0F);
		Beams.addChild(Horizontal2);
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -6.0F, -0.6768F, -0.6768F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -6.0F, -3.5052F, -3.5052F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -6.0F, -6.5104F, -6.5104F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -6.0F, -9.5156F, -9.5156F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -6.0F, -12.344F, -12.344F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -6.0F, -15.3492F, -15.3492F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -6.0F, 1.9749F, 1.9749F, 12, 1, 1, 0.0F, false));

		Handle = new RendererModel(this);
		Handle.setRotationPoint(12.5F, 1.1667F, -1.0833F);
		setRotationAngle(Handle, 0.0F, -1.5708F, 0.0F);
		NorthPanel.addChild(Handle);
		Handle.cubeList.add(new ModelBox(Handle, 0, 42, -0.4F, -3.5F, 0.0F, 1, 4, 0, 0.0F, false));

		Baseframe = new RendererModel(this);
		Baseframe.setRotationPoint(0.0F, 24.0F, 0.0F);
		Baseframe.cubeList.add(new ModelBox(Baseframe, 0, 41, -9.0F, -1.0F, -9.0F, 18, 1, 18, 0.0F, false));

		Pillars = new RendererModel(this);
		Pillars.setRotationPoint(0.0F, 0.0F, 0.0F);
		Baseframe.addChild(Pillars);
		Pillars.cubeList.add(new ModelBox(Pillars, 60, 0, 6.5F, -32.0F, -8.5F, 2, 31, 2, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 60, 0, -8.5F, -32.0F, -8.5F, 2, 31, 2, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 60, 0, -8.5F, -33.0F, 6.5F, 2, 32, 2, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 60, 0, 6.5F, -33.0F, 6.5F, 2, 32, 2, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 30, 20, -8.5F, -33.0F, -8.5F, 17, 1, 2, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 30, 20, -8.5F, -33.0F, 6.5F, 17, 1, 2, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 40, 20, 7.5F, -33.0F, -6.5F, 1, 1, 13, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 40, 20, -8.5F, -33.0F, -6.5F, 1, 1, 13, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 0, -8.75F, -34.0F, 7.75F, 1, 33, 1, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 0, -8.75F, -34.0F, -8.75F, 1, 33, 1, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 0, 7.75F, -34.0F, -8.75F, 1, 33, 1, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 0, 7.75F, -34.0F, 7.75F, 1, 33, 1, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 16, -8.0F, -34.0F, 7.75F, 16, 1, 1, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 16, 7.75F, -34.0F, -8.0F, 1, 1, 16, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 16, -8.0F, -34.0F, -8.75F, 16, 1, 1, 0.0F, false));
		Pillars.cubeList.add(new ModelBox(Pillars, 68, 16, -8.75F, -34.0F, -8.0F, 1, 1, 16, 0.0F, false));

		Sign = new RendererModel(this);
		Sign.setRotationPoint(0.0F, 0.0F, 0.0F);
		Baseframe.addChild(Sign);
		Sign.cubeList.add(new ModelBox(Sign, 0, 20, -8.0F, -37.0F, -8.0F, 16, 3, 16, 0.0F, false));
		Sign.cubeList.add(new ModelBox(Sign, 72, 0, -8.25F, -36.5F, -7.0F, 1, 2, 14, 0.0F, false));
		Sign.cubeList.add(new ModelBox(Sign, 72, 0, 7.25F, -36.5F, -7.0F, 1, 2, 14, 0.0F, false));
		Sign.cubeList.add(new ModelBox(Sign, 72, 0, -7.0F, -36.5F, -8.25F, 14, 2, 1, 0.0F, false));
		Sign.cubeList.add(new ModelBox(Sign, 72, 0, -7.0F, -36.5F, 7.25F, 14, 2, 1, 0.0F, false));

		Roof = new RendererModel(this);
		Roof.setRotationPoint(0.0F, 0.0F, 0.0F);
		Baseframe.addChild(Roof);
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -3.0F, -43.5F, -3.0F, 6, 1, 6, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -8.5F, -40.0F, -8.5F, 1, 3, 1, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, 7.5F, -40.0F, -8.5F, 1, 3, 1, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, 7.5F, -40.0F, 7.5F, 1, 3, 1, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -8.5F, -40.0F, 7.5F, 1, 3, 1, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -8.5F, -37.75F, -8.5F, 17, 1, 17, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -7.5F, -43.0F, -3.0F, 15, 1, 6, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -3.0F, -43.0F, -7.5F, 6, 1, 15, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -5.5F, -42.5F, -7.5F, 11, 1, 15, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 20, -7.5F, -42.5F, -5.5F, 15, 1, 11, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 0, -7.5F, -41.5F, -7.5F, 15, 4, 15, 0.0F, false));

		Edges = new RendererModel(this);
		Edges.setRotationPoint(0.0F, -1.0F, -7.5F);
		Roof.addChild(Edges);

		EastEdge = new RendererModel(this);
		EastEdge.setRotationPoint(-7.5F, -38.0F, 15.0F);
		Edges.addChild(EastEdge);
		EastEdge.cubeList.add(new ModelBox(EastEdge, 0, 21, -1.0F, -1.5F, -1.0F, 1, 1, 1, 0.0F, false));
		EastEdge.cubeList.add(new ModelBox(EastEdge, 0, 21, -1.0F, -1.5F, -15.0F, 1, 1, 1, 0.0F, false));
		EastEdge.cubeList.add(new ModelBox(EastEdge, 0, 21, -1.0F, -2.0F, -2.5F, 1, 1, 2, 0.0F, false));
		EastEdge.cubeList.add(new ModelBox(EastEdge, 0, 21, -1.0F, -2.0F, -14.5F, 1, 1, 2, 0.0F, false));
		EastEdge.cubeList.add(new ModelBox(EastEdge, 0, 21, -1.0F, -2.5F, -5.0F, 1, 1, 3, 0.0F, false));
		EastEdge.cubeList.add(new ModelBox(EastEdge, 0, 21, -1.0F, -2.5F, -13.0F, 1, 1, 3, 0.0F, false));
		EastEdge.cubeList.add(new ModelBox(EastEdge, 0, 21, -1.0F, -3.0F, -10.5F, 1, 1, 6, 0.0F, false));

		SouthEdge = new RendererModel(this);
		SouthEdge.setRotationPoint(0.0F, -38.0F, 7.5F);
		setRotationAngle(SouthEdge, 0.0F, 1.5708F, 0.0F);
		Edges.addChild(SouthEdge);
		SouthEdge.cubeList.add(new ModelBox(SouthEdge, 0, 21, -8.5F, -1.5F, 6.5F, 1, 1, 1, 0.0F, false));
		SouthEdge.cubeList.add(new ModelBox(SouthEdge, 0, 21, -8.5F, -1.5F, -7.5F, 1, 1, 1, 0.0F, false));
		SouthEdge.cubeList.add(new ModelBox(SouthEdge, 0, 21, -8.5F, -2.0F, 5.0F, 1, 1, 2, 0.0F, false));
		SouthEdge.cubeList.add(new ModelBox(SouthEdge, 0, 21, -8.5F, -2.0F, -7.0F, 1, 1, 2, 0.0F, false));
		SouthEdge.cubeList.add(new ModelBox(SouthEdge, 0, 21, -8.5F, -2.5F, 2.5F, 1, 1, 3, 0.0F, false));
		SouthEdge.cubeList.add(new ModelBox(SouthEdge, 0, 21, -8.5F, -2.5F, -5.5F, 1, 1, 3, 0.0F, false));
		SouthEdge.cubeList.add(new ModelBox(SouthEdge, 0, 21, -8.5F, -3.0F, -3.0F, 1, 1, 6, 0.0F, false));

		WestEdge = new RendererModel(this);
		WestEdge.setRotationPoint(0.0F, -38.0F, 7.5F);
		setRotationAngle(WestEdge, 0.0F, 3.1416F, 0.0F);
		Edges.addChild(WestEdge);
		WestEdge.cubeList.add(new ModelBox(WestEdge, 0, 24, -8.5F, -1.5F, 6.5F, 1, 1, 1, 0.0F, false));
		WestEdge.cubeList.add(new ModelBox(WestEdge, 0, 24, -8.5F, -1.5F, -7.5F, 1, 1, 1, 0.0F, false));
		WestEdge.cubeList.add(new ModelBox(WestEdge, 0, 24, -8.5F, -2.0F, 5.0F, 1, 1, 2, 0.0F, false));
		WestEdge.cubeList.add(new ModelBox(WestEdge, 0, 24, -8.5F, -2.0F, -7.0F, 1, 1, 2, 0.0F, false));
		WestEdge.cubeList.add(new ModelBox(WestEdge, 0, 24, -8.5F, -2.5F, 2.5F, 1, 1, 3, 0.0F, false));
		WestEdge.cubeList.add(new ModelBox(WestEdge, 0, 24, -8.5F, -2.5F, -5.5F, 1, 1, 3, 0.0F, false));
		WestEdge.cubeList.add(new ModelBox(WestEdge, 0, 24, -8.5F, -3.0F, -3.0F, 1, 1, 6, 0.0F, false));

		NorthEdge = new RendererModel(this);
		NorthEdge.setRotationPoint(0.0F, -39.6429F, 7.5F);
		setRotationAngle(NorthEdge, 0.0F, -1.5708F, 0.0F);
		Edges.addChild(NorthEdge);
		NorthEdge.cubeList.add(new ModelBox(NorthEdge, 0, 22, -8.5F, 0.1429F, 6.5F, 1, 1, 1, 0.0F, false));
		NorthEdge.cubeList.add(new ModelBox(NorthEdge, 0, 22, -8.5F, 0.1429F, -7.5F, 1, 1, 1, 0.0F, false));
		NorthEdge.cubeList.add(new ModelBox(NorthEdge, 0, 22, -8.5F, -0.3571F, 5.0F, 1, 1, 2, 0.0F, false));
		NorthEdge.cubeList.add(new ModelBox(NorthEdge, 0, 22, -8.5F, -0.3571F, -7.0F, 1, 1, 2, 0.0F, false));
		NorthEdge.cubeList.add(new ModelBox(NorthEdge, 0, 22, -8.5F, -0.8571F, 2.5F, 1, 1, 3, 0.0F, false));
		NorthEdge.cubeList.add(new ModelBox(NorthEdge, 0, 22, -8.5F, -0.8571F, -5.5F, 1, 1, 3, 0.0F, false));
		NorthEdge.cubeList.add(new ModelBox(NorthEdge, 0, 22, -8.5F, -1.3571F, -3.0F, 1, 1, 6, 0.0F, false));

		SouthPanel = new RendererModel(this);
		SouthPanel.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(SouthPanel, 0.0F, 3.1416F, 0.0F);
		SouthPanel.cubeList.add(new ModelBox(SouthPanel, 72, 16, -6.5F, -5.5F, -8.0F, 13, 5, 1, 0.0F, false));
		SouthPanel.cubeList.add(new ModelBox(SouthPanel, 68, 0, -6.5F, -32.5F, -8.25F, 1, 32, 1, 0.0F, false));
		SouthPanel.cubeList.add(new ModelBox(SouthPanel, 78, 16, -5.5F, -32.5F, -8.25F, 11, 2, 1, 0.0F, false));
		SouthPanel.cubeList.add(new ModelBox(SouthPanel, 68, 0, 5.5F, -32.5F, -8.25F, 1, 32, 1, 0.0F, false));
		SouthPanel.cubeList.add(new ModelBox(SouthPanel, 68, 0, 5.5F, -33.0F, -7.0F, 1, 27, 0, 0.0F, false));
		SouthPanel.cubeList.add(new ModelBox(SouthPanel, 68, 0, -6.5F, -33.0F, -7.0F, 1, 27, 0, 0.0F, false));
		SouthPanel.cubeList.add(new ModelBox(SouthPanel, 78, 16, -5.5F, -32.5F, -7.75F, 11, 2, 1, 0.0F, false));

		Glass_Panel4 = new RendererModel(this);
		Glass_Panel4.setRotationPoint(0.0F, 0.0F, 0.0F);
		SouthPanel.addChild(Glass_Panel4);
		Glass_Panel4.cubeList.add(new ModelBox(Glass_Panel4, 79, 40, -6.0F, -30.0F, -7.75F, 12, 24, 0, 0.0F, false));

		Beams4 = new RendererModel(this);
		Beams4.setRotationPoint(-2.0F, -17.5F, -7.75F);
		Glass_Panel4.addChild(Beams4);

		Vertical7 = new RendererModel(this);
		Vertical7.setRotationPoint(0.0F, 0.0F, 0.25F);
		setRotationAngle(Vertical7, 0.0F, -0.7854F, 0.0F);
		Beams4.addChild(Vertical7);
		Vertical7.cubeList.add(new ModelBox(Vertical7, 0, 40, 4.8033F, -13.5F, -5.705F, 1, 25, 1, 0.0F, false));
		Vertical7.cubeList.add(new ModelBox(Vertical7, 0, 40, -0.5F, -13.5F, -0.5F, 1, 25, 1, 0.0F, false));
		Vertical7.cubeList.add(new ModelBox(Vertical7, 0, 40, 2.3284F, -13.5F, -3.3284F, 1, 25, 1, 0.0F, false));
		Vertical7.cubeList.add(new ModelBox(Vertical7, 0, 40, -2.9749F, -13.5F, 2.0732F, 1, 25, 1, 0.0F, false));

		Horizontal7 = new RendererModel(this);
		Horizontal7.setRotationPoint(2.0F, 8.0F, 0.25F);
		setRotationAngle(Horizontal7, -0.7854F, 0.0F, 0.0F);
		Beams4.addChild(Horizontal7);
		Horizontal7.cubeList.add(new ModelBox(Horizontal7, 0, 40, -6.0F, -0.6768F, -0.6768F, 12, 1, 1, 0.0F, false));
		Horizontal7.cubeList.add(new ModelBox(Horizontal7, 0, 40, -6.0F, -3.5052F, -3.5052F, 12, 1, 1, 0.0F, false));
		Horizontal7.cubeList.add(new ModelBox(Horizontal7, 0, 40, -6.0F, -6.5104F, -6.5104F, 12, 1, 1, 0.0F, false));
		Horizontal7.cubeList.add(new ModelBox(Horizontal7, 0, 40, -6.0F, -9.5156F, -9.5156F, 12, 1, 1, 0.0F, false));
		Horizontal7.cubeList.add(new ModelBox(Horizontal7, 0, 40, -6.0F, -12.344F, -12.344F, 12, 1, 1, 0.0F, false));
		Horizontal7.cubeList.add(new ModelBox(Horizontal7, 0, 40, -6.0F, -15.3492F, -15.3492F, 12, 1, 1, 0.0F, false));
		Horizontal7.cubeList.add(new ModelBox(Horizontal7, 0, 40, -6.0F, 1.9749F, 1.9749F, 12, 1, 1, 0.0F, false));

		Inlay4 = new RendererModel(this);
		Inlay4.setRotationPoint(-0.5F, -4.5F, -7.75F);
		SouthPanel.addChild(Inlay4);

		Horizontal8 = new RendererModel(this);
		Horizontal8.setRotationPoint(-0.75F, 0.0F, 0.0F);
		setRotationAngle(Horizontal8, -0.7854F, 0.0F, 0.0F);
		Inlay4.addChild(Horizontal8);
		Horizontal8.cubeList.add(new ModelBox(Horizontal8, 0, 40, -4.0F, 1.2678F, 1.2678F, 10, 1, 1, 0.0F, false));
		Horizontal8.cubeList.add(new ModelBox(Horizontal8, 0, 40, -4.0F, -0.6768F, -0.6768F, 10, 1, 1, 0.0F, false));

		Vertical8 = new RendererModel(this);
		Vertical8.setRotationPoint(-5.5F, 2.114F, 0.0576F);
		setRotationAngle(Vertical8, 0.0F, -0.7854F, 0.0F);
		Inlay4.addChild(Vertical8);
		Vertical8.cubeList.add(new ModelBox(Vertical8, 0, 40, 0.2071F, -2.5F, -1.2071F, 1, 3, 1, 0.0F, false));
		Vertical8.cubeList.add(new ModelBox(Vertical8, 0, 40, 7.2782F, -2.5F, -8.2782F, 1, 3, 1, 0.0F, false));

		EastPanel = new RendererModel(this);
		EastPanel.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(EastPanel, 0.0F, 1.5708F, 0.0F);
		EastPanel.cubeList.add(new ModelBox(EastPanel, 72, 16, -6.5F, -5.5F, -8.0F, 13, 5, 1, 0.0F, false));
		EastPanel.cubeList.add(new ModelBox(EastPanel, 68, 0, -6.5F, -32.5F, -8.25F, 1, 32, 1, 0.0F, false));
		EastPanel.cubeList.add(new ModelBox(EastPanel, 78, 16, -5.5F, -32.5F, -8.25F, 11, 2, 1, 0.0F, false));
		EastPanel.cubeList.add(new ModelBox(EastPanel, 68, 0, 5.5F, -32.5F, -8.25F, 1, 32, 1, 0.0F, false));
		EastPanel.cubeList.add(new ModelBox(EastPanel, 68, 0, 5.5F, -33.0F, -7.0F, 1, 27, 0, 0.0F, false));
		EastPanel.cubeList.add(new ModelBox(EastPanel, 68, 0, -6.5F, -33.0F, -7.0F, 1, 27, 0, 0.0F, false));
		EastPanel.cubeList.add(new ModelBox(EastPanel, 78, 16, -5.5F, -32.5F, -7.75F, 11, 2, 1, 0.0F, false));

		Glass_Panel2 = new RendererModel(this);
		Glass_Panel2.setRotationPoint(0.0F, 0.0F, 0.0F);
		EastPanel.addChild(Glass_Panel2);
		Glass_Panel2.cubeList.add(new ModelBox(Glass_Panel2, 79, 40, -6.0F, -30.0F, -7.75F, 12, 24, 0, 0.0F, false));

		Beams2 = new RendererModel(this);
		Beams2.setRotationPoint(-2.0F, -17.5F, -7.75F);
		Glass_Panel2.addChild(Beams2);

		Vertical3 = new RendererModel(this);
		Vertical3.setRotationPoint(0.0F, 0.0F, 0.25F);
		setRotationAngle(Vertical3, 0.0F, -0.7854F, 0.0F);
		Beams2.addChild(Vertical3);
		Vertical3.cubeList.add(new ModelBox(Vertical3, 0, 40, 4.8033F, -13.5F, -5.705F, 1, 25, 1, 0.0F, false));
		Vertical3.cubeList.add(new ModelBox(Vertical3, 0, 40, -0.5F, -13.5F, -0.5F, 1, 25, 1, 0.0F, false));
		Vertical3.cubeList.add(new ModelBox(Vertical3, 0, 40, 2.3284F, -13.5F, -3.3284F, 1, 25, 1, 0.0F, false));
		Vertical3.cubeList.add(new ModelBox(Vertical3, 0, 40, -2.9749F, -13.5F, 2.0732F, 1, 25, 1, 0.0F, false));

		Horizontal3 = new RendererModel(this);
		Horizontal3.setRotationPoint(2.0F, 8.0F, 0.25F);
		setRotationAngle(Horizontal3, -0.7854F, 0.0F, 0.0F);
		Beams2.addChild(Horizontal3);
		Horizontal3.cubeList.add(new ModelBox(Horizontal3, 0, 40, -6.0F, -0.6768F, -0.6768F, 12, 1, 1, 0.0F, false));
		Horizontal3.cubeList.add(new ModelBox(Horizontal3, 0, 40, -6.0F, -3.5052F, -3.5052F, 12, 1, 1, 0.0F, false));
		Horizontal3.cubeList.add(new ModelBox(Horizontal3, 0, 40, -6.0F, -6.5104F, -6.5104F, 12, 1, 1, 0.0F, false));
		Horizontal3.cubeList.add(new ModelBox(Horizontal3, 0, 40, -6.0F, -9.5156F, -9.5156F, 12, 1, 1, 0.0F, false));
		Horizontal3.cubeList.add(new ModelBox(Horizontal3, 0, 40, -6.0F, -12.344F, -12.344F, 12, 1, 1, 0.0F, false));
		Horizontal3.cubeList.add(new ModelBox(Horizontal3, 0, 40, -6.0F, -15.3492F, -15.3492F, 12, 1, 1, 0.0F, false));
		Horizontal3.cubeList.add(new ModelBox(Horizontal3, 0, 40, -6.0F, 1.9749F, 1.9749F, 12, 1, 1, 0.0F, false));

		Inlay2 = new RendererModel(this);
		Inlay2.setRotationPoint(-0.5F, -4.5F, -7.75F);
		EastPanel.addChild(Inlay2);

		Horizontal4 = new RendererModel(this);
		Horizontal4.setRotationPoint(-0.75F, 0.0F, 0.0F);
		setRotationAngle(Horizontal4, -0.7854F, 0.0F, 0.0F);
		Inlay2.addChild(Horizontal4);
		Horizontal4.cubeList.add(new ModelBox(Horizontal4, 0, 40, -4.0F, 1.2678F, 1.2678F, 10, 1, 1, 0.0F, false));
		Horizontal4.cubeList.add(new ModelBox(Horizontal4, 0, 40, -4.0F, -0.6768F, -0.6768F, 10, 1, 1, 0.0F, false));

		Vertical4 = new RendererModel(this);
		Vertical4.setRotationPoint(-5.5F, 2.114F, 0.0576F);
		setRotationAngle(Vertical4, 0.0F, -0.7854F, 0.0F);
		Inlay2.addChild(Vertical4);
		Vertical4.cubeList.add(new ModelBox(Vertical4, 0, 40, 0.2071F, -2.5F, -1.2071F, 1, 3, 1, 0.0F, false));
		Vertical4.cubeList.add(new ModelBox(Vertical4, 0, 40, 7.2782F, -2.5F, -8.2782F, 1, 3, 1, 0.0F, false));

		WestPanel = new RendererModel(this);
		WestPanel.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(WestPanel, 0.0F, -1.5708F, 0.0F);
		WestPanel.cubeList.add(new ModelBox(WestPanel, 72, 16, -6.5F, -5.5F, -8.0F, 13, 5, 1, 0.0F, false));
		WestPanel.cubeList.add(new ModelBox(WestPanel, 68, 0, -6.5F, -32.5F, -8.25F, 1, 32, 1, 0.0F, false));
		WestPanel.cubeList.add(new ModelBox(WestPanel, 78, 16, -5.5F, -32.5F, -8.25F, 11, 2, 1, 0.0F, false));
		WestPanel.cubeList.add(new ModelBox(WestPanel, 68, 0, 5.5F, -32.5F, -8.25F, 1, 32, 1, 0.0F, false));
		WestPanel.cubeList.add(new ModelBox(WestPanel, 68, 0, 5.5F, -33.0F, -7.0F, 1, 27, 0, 0.0F, false));
		WestPanel.cubeList.add(new ModelBox(WestPanel, 68, 0, -6.5F, -33.0F, -7.0F, 1, 27, 0, 0.0F, false));
		WestPanel.cubeList.add(new ModelBox(WestPanel, 78, 16, -5.5F, -32.5F, -7.75F, 11, 2, 1, 0.0F, false));

		Glass_Panel3 = new RendererModel(this);
		Glass_Panel3.setRotationPoint(0.0F, 0.0F, 0.0F);
		WestPanel.addChild(Glass_Panel3);
		Glass_Panel3.cubeList.add(new ModelBox(Glass_Panel3, 79, 40, -6.0F, -30.0F, -7.75F, 12, 24, 0, 0.0F, false));

		Beams3 = new RendererModel(this);
		Beams3.setRotationPoint(-2.0F, -17.5F, -7.75F);
		Glass_Panel3.addChild(Beams3);

		Vertical5 = new RendererModel(this);
		Vertical5.setRotationPoint(0.0F, 0.0F, 0.25F);
		setRotationAngle(Vertical5, 0.0F, -0.7854F, 0.0F);
		Beams3.addChild(Vertical5);
		Vertical5.cubeList.add(new ModelBox(Vertical5, 0, 40, 4.8033F, -13.5F, -5.705F, 1, 25, 1, 0.0F, false));
		Vertical5.cubeList.add(new ModelBox(Vertical5, 0, 40, -0.5F, -13.5F, -0.5F, 1, 25, 1, 0.0F, false));
		Vertical5.cubeList.add(new ModelBox(Vertical5, 0, 40, 2.3284F, -13.5F, -3.3284F, 1, 25, 1, 0.0F, false));
		Vertical5.cubeList.add(new ModelBox(Vertical5, 0, 40, -2.9749F, -13.5F, 2.0732F, 1, 25, 1, 0.0F, false));

		Horizontal5 = new RendererModel(this);
		Horizontal5.setRotationPoint(2.0F, 8.0F, 0.25F);
		setRotationAngle(Horizontal5, -0.7854F, 0.0F, 0.0F);
		Beams3.addChild(Horizontal5);
		Horizontal5.cubeList.add(new ModelBox(Horizontal5, 0, 40, -6.0F, -0.6768F, -0.6768F, 12, 1, 1, 0.0F, false));
		Horizontal5.cubeList.add(new ModelBox(Horizontal5, 0, 40, -6.0F, -3.5052F, -3.5052F, 12, 1, 1, 0.0F, false));
		Horizontal5.cubeList.add(new ModelBox(Horizontal5, 0, 40, -6.0F, -6.5104F, -6.5104F, 12, 1, 1, 0.0F, false));
		Horizontal5.cubeList.add(new ModelBox(Horizontal5, 0, 40, -6.0F, -9.5156F, -9.5156F, 12, 1, 1, 0.0F, false));
		Horizontal5.cubeList.add(new ModelBox(Horizontal5, 0, 40, -6.0F, -12.344F, -12.344F, 12, 1, 1, 0.0F, false));
		Horizontal5.cubeList.add(new ModelBox(Horizontal5, 0, 40, -6.0F, -15.3492F, -15.3492F, 12, 1, 1, 0.0F, false));
		Horizontal5.cubeList.add(new ModelBox(Horizontal5, 0, 40, -6.0F, 1.9749F, 1.9749F, 12, 1, 1, 0.0F, false));

		Inlay3 = new RendererModel(this);
		Inlay3.setRotationPoint(-0.5F, -4.5F, -7.75F);
		WestPanel.addChild(Inlay3);

		Horizontal6 = new RendererModel(this);
		Horizontal6.setRotationPoint(-0.75F, 0.0F, 0.0F);
		setRotationAngle(Horizontal6, -0.7854F, 0.0F, 0.0F);
		Inlay3.addChild(Horizontal6);
		Horizontal6.cubeList.add(new ModelBox(Horizontal6, 0, 40, -4.0F, 1.2678F, 1.2678F, 10, 1, 1, 0.0F, false));
		Horizontal6.cubeList.add(new ModelBox(Horizontal6, 0, 40, -4.0F, -0.6768F, -0.6768F, 10, 1, 1, 0.0F, false));

		Vertical6 = new RendererModel(this);
		Vertical6.setRotationPoint(-5.5F, 2.114F, 0.0576F);
		setRotationAngle(Vertical6, 0.0F, -0.7854F, 0.0F);
		Inlay3.addChild(Vertical6);
		Vertical6.cubeList.add(new ModelBox(Vertical6, 0, 40, 0.2071F, -2.5F, -1.2071F, 1, 3, 1, 0.0F, false));
		Vertical6.cubeList.add(new ModelBox(Vertical6, 0, 40, 7.2782F, -2.5F, -8.2782F, 1, 3, 1, 0.0F, false));
	}

	public void render(ExteriorTile tile) {
		
		this.NorthPanel.rotateAngleY = (float) Math.toRadians(EnumDoorType.RED.getRotationForState(tile.getOpen()));
		
		NorthPanel.render(0.0625F);
		Baseframe.render(0.0625F);
		SouthPanel.render(0.0625F);
		EastPanel.render(0.0625F);
		WestPanel.render(0.0625F);
	}
	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}
}