package net.tardis.mod.tileentities.consoles;

import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class CopperConsoleTile extends ConsoleTile{

	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, 0, -2, 2, 5, 2);
	
	public CopperConsoleTile() {
		super(TTiles.CONSOLE_COPPER);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

}
