package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.ChangePageButton;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.Page;
import net.tardis.mod.contexts.gui.GuiItemContext;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateManualPageMessage;

public class ManualScreen extends Screen {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/manual.png");
	public static final int WIDTH = 256, HEIGHT = 187;
	List<Page> pages = new ArrayList<Page>();
	int page = 0;
	
	public ManualScreen() {
		super(new StringTextComponent("Manual"));
	}
	
	public ManualScreen(GuiContext contex) {
		super(new StringTextComponent("Manual"));
		if(contex instanceof GuiItemContext) {
			ItemStack stack = ((GuiItemContext)contex).getItemStack();
			if(stack.hasTag() && stack.getTag().contains("page"))
				page = stack.getTag().getInt("page");
				
		}
	}

	@Override
	protected void init() {
		super.init();
		
		this.addPage("TT Capsule Manual", "Type 40 Mark 3");
		
		pages.add(new Page(this.font));
		pages.add(new Page(this.font, "TARDIS Maintenance", "", "While it is very tempting to just take off and see where it takes you, a little forethought and planning will ensure a much more enjoyable time."));
		pages.add(new Page(this.font, "Part one: The Engine"));
		pages.add(new Page(this.font, 
		"The TARDIS Engine is the most crucial part of a TARDIS, save the console.", "", 
		"   This holds all your components, upgrades, artron banks, and also has a compartment for embuing things with Artron Energy."));
		pages.add(new Page(this.font, "The Compartment with a blue door is the Component Panel, explained more in the Subsystems section.", "", 
				"The Compartment with a yellow door is the charging panel, charging any Artron-capable items added to it"));
		pages.add(new Page(this.font, "", "The Compartment with a green door is an upgrade slot, more on that in the Upgrades section."));
		pages.add(new Page(this.font, "The final Compartment is the Artron slot, with a red door. This is for fuel cells to add to your time-ship's internal buffer of 256AU"));
		pages.add(new Page(this.font, "Part 2: Subsystems", "", " Subsystems are parts of your time-ship. Some of these are not required for flight, but most are"));
		this.addPage();
		pages.add(new Page(this.font, "Dematerialization", "", "  The most important sub-system, allowing your time-ship to enter and exit the vortex.", "",
				"It is repaired by combining it with an ender pearl in the Multi Quantiscope"));
		pages.add(new Page(this.font, "Fluid Links", "", " A critial component of the TARDIS, when it breaks it releases deadly mercury gas, so keep an eye on it", "", "It is repaired with a bottle of Mercury in a Multi Quantiscope"));
		pages.add(new Page(this.font, "Upgrades", "", " Beyond just required parts, time-ships can be upgraded. Type 40's are especially good for this."));
		this.addPage();
		this.addPage("Atrium Module", "", "An Upgrade to the deamaterialization circuit. The Atrium Module allows you to build an exterior atrium for your time-ship that travels with it. The Atrium area must be square of a maximum");
		this.addPage("size no larger than 11 blocks across, centered on the time-ship's door. The area is defined using Atrium Blocks to build a baseplate platform under the structure.", "Using the Atrium module limits available landing areas for a time-ship.");
		this.addPage("(Atrium cont.)A time-ship will not land in a location that does not provide adequate open area for it to materialize.");
		this.addPage();
		//Flight
		this.addPage("Flight", "", " This section is for the controls and what they do. For your specific capsule and it's layout, please shadow the Holographic Pilot for a few flights (NOT IMPLEMENTED)");
		this.addPage("");
		this.addPage("Dimensional Control", "", " This control is responsible for flights into different times, planets, and dimensions.");
		this.addPage("Directional Control", "", "This control allows you to select the cardinal direction for your time-ship on it's next materialization.");
		this.addPage("Handbreak", "", " Stops or allows flight. When this is set, flights by unruly time-ships cannot occur until it is lifted");
		this.addPage("Throttle", "", " Controls the speed of flight. (200Blocks/Sec max, or 20B/S min) The faster it is, the more fuel is used. (1AU/T at max, 0.1AU/T at least)");
		this.addPage("Increment Modifier", "", " This controls by how many blocks the Real-Space controls move you. (X, Y, Z, or a Randomizer)");
		this.addPage("Landing Type", "", " In the event that something obstructs your time-ship's desired landing spot, it will try to find another nearby. This controls whether it perfers a highter ot lower point.");
		this.addPage("Randomizer", "", " This scrambles your time-ship's destination coordinates, making it extremly difficult to track. If you don't know where you are bound, how could others?");
		this.addPage("Refueler", "", " This activates your time-ship's refuling circuits, absorbing nearby time radiation (Artron) to use as fuel. Some places have more than others, especially places where the veil betwixt worlds is thin.");
		this.addPage("Real-Space Controls", "(X, Y, Z)", "", " These control your time-ship's position in the three main axes.", "", " How much is determined by your Coordinate Increment Modifer");
		//Monitor
		this.addPage();
		this.addPage("Protocols", "", " Turning your attention to your time-ship's monitor, you'll notice a lot of helpful information for flight.", "", " But in addition, when you interact with it, you'll be greeted with");
		this.addPage("a multitude of different menus and submenus.");
		this.addPage("Change Interior", "", " A function of your time-ship ARS, this allows a pilot to select their console room of choice.");
		this.addPage("Change Console Unit", "", " Allows a pilot to select their preferred console unit.");
		this.addPage("Security Submenu", "", " Our first submenu, this holds security- related things", "", "Toggle Alarm", "", " Turns on or off your time-ship's alarm. This is triggered when something your");
		this.addPage("", "time-ship deems hostile enters it.", "", "Scan for Life Signs", "", " In the event your time-ship is entered unexpectedly, it may benefit you to have a count of your adversaries. This protocol counts all living beings");
		this.addPage("", " in your time-ship and reports the number to you.");
		this.addPage("Interior Properties Submenu", "", " This menu holds customization options for your time-ship.", "", "Interior Hum", "", " Allows you to adjust your time-ship's ambience.");
		this.addPage("", "Light Level", "", " Allows your to adjust the lights inside your time-ship");
		this.addPage();
		
		//Addendum
		this.addPage("Addendum: Other situations", "", " This is a chapter set a side for situations you may find yourself in not directly pertaining to your time-ship.");
		this.addPage("Abandoned Time-Ships", "", " Sometimes, when a time-ship has been abandoned by it's crew for whatever reason, it's security systems may degrade, allowing it's emotions to override these systems.");
		this.addPage("(Aban. T.S. Cont.)", "", " A time-ship this has happened to may accept tokens representing time or exploration.");
		this.addPage("Some common examples include: ", "  Clocks", "  Compasses", "  Maps", "  Village Bells", "  Ender Pearls", "  Ender Eyes");
		this.addPage("(Aban. T.S. Cont.)", "", " When trying to find one, it is helpful to remember that time-ships will relay village bells with their own cloister bells, making them easy to locate the general area.");
		this.addPage(" For more fine-tuned locating, your standard Gallifreyan Pocket Watch detects space-time anomalies", "", " In addition to charming it, it is also possible to just rip it open by attaching a lead to a horse or other such animal.");
		this.addPage("(Aban. T.S. Cont.)", ""," However, forcing it open may influence your time-ship to despise you for your forceful demeanor, which can be an issue for you later on.");
		//TODO: Add these back when we have ships 'n' such
		/*this.addPage("Crashed Ships", "", " On most planets you may find some crashed ships. While quite unfortunate for the previous owners, If you find yourself low on parts or supplies you might find these invaluable.");
		this.addPage("(Crashed ships, cont.)", "", " If you have an Interstitial Antenna, you maybe be able to pick up an S.O.S on these, even though in most situations anyone requiring assistance will be long gone.");
		this.addPage(" Besides parts, these ship's computers may contain valuable knowledge like schematics.");*/
		
		
		this.buttons.clear();
		
		this.addButton(new ChangePageButton(width / 2 + 85, height / 2 + 45, true, button -> {
			if(page + 2 < pages.size())
				page += 2;
		}, true));
		this.addButton(new ChangePageButton(width / 2 - 110, height / 2 + 45, false, button -> {
			if(page - 2 >= 0) {
				page -= 2;
			}
		}, true));
	}
	
	public void addPage(String... lines) {
		this.pages.add(new Page(this.font, lines));
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.blit(width / 2 - WIDTH / 2, height / 2 - HEIGHT / 2, 0, 0, WIDTH, HEIGHT);
		if(pages.size() > page) {
			this.pages.get(page).render(width / 2 - 110, height / 2 - 70, mouseX, mouseY);
		}
		if(page + 1 < this.pages.size()) {
			this.pages.get(page + 1).render(width / 2 + 10, height / 2 - 70, mouseX, mouseY);
		}
		
		for(Widget w : this.buttons) {
			w.render(mouseX, mouseY, p_render_3_);
		}
	}

	@Override
	public void onClose() {
		super.onClose();
		Network.sendToServer(new UpdateManualPageMessage(page));
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int p_mouseClicked_5_) {
		
		return super.mouseClicked(mouseX, mouseY, p_mouseClicked_5_);
	}
}
