package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.tileentities.ConsoleTile;

public class ChameleonSubsystem extends Subsystem{


	public ChameleonSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}
	
	@Override
	public boolean stopFlight() {
		return false;
	}

}
