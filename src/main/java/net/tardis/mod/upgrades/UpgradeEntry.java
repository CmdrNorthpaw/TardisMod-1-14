package net.tardis.mod.upgrades;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;

public class UpgradeEntry<T extends Upgrade> implements IRegisterable<UpgradeEntry<T>>{

	private ResourceLocation registryName;
	private IConsoleSpawner<T> spawn;
	private Item item;
	
	public UpgradeEntry(IConsoleSpawner<T> spawn, Item item) {
		this.spawn = spawn;
		this.item = item;
	}
	
	@Override
	public UpgradeEntry<T> setRegistryName(ResourceLocation regName) {
		this.registryName = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public T create(ConsoleTile tile) {
		T upgrade = spawn.create(this, tile);
		return upgrade;
	}
	
	public static interface IConsoleSpawner<T>{
		T create(UpgradeEntry<?> entry, ConsoleTile console);
	}

}
