package net.tardis.mod.compat.jei;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.Recipes;
import net.tardis.mod.tags.TardisItemTags;
import net.tardis.mod.tileentities.AlembicTile;

public class AlembicRecipe implements IRecipe<AlembicTile>{

	public static final ResourceLocation name = new ResourceLocation(Tardis.MODID, "mercury");
	
	@Override
	public boolean matches(AlembicTile inv, World worldIn) {
		return inv.getStackInSlot(2).getItem().isIn(TardisItemTags.CINNABAR);
	}

	@Override
	public ItemStack getCraftingResult(AlembicTile inv) {
		return new ItemStack(TItems.MERCURY_BOTTLE);
	}

	@Override
	public boolean canFit(int width, int height) {
		return true;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return new ItemStack(TItems.MERCURY_BOTTLE);
	}

	@Override
	public ResourceLocation getId() {
		return name;
	}

	@Override
	public IRecipeSerializer<?> getSerializer() {
		return null;
	}

	@Override
	public IRecipeType<?> getType() {
		return Recipes.ALEMBIC;
	}

}
