package net.tardis.mod.upgrades;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public abstract class Upgrade{
	
	private UpgradeEntry<?> entry;
	private ConsoleTile console;
	
	private ItemStack stack;
	
	protected Upgrade(UpgradeEntry<?> entry, ConsoleTile tile) {
		this.entry = entry;
		this.console = tile;
	}
	
	public ConsoleTile getConsole() {
		if(console != null && !console.isRemoved())
			return console;
		if(console != null && console.getWorld() != null) {
			TileEntity te = console.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile)
				return (ConsoleTile)te;
		}
		return null;
	}
	
	public UpgradeEntry<?> getEntry(){
		return this.entry;
	}
	
	//Default functions
	
	public ItemStack getStack() {
		if(this.stack == null || stack.isEmpty())
			return (stack = this.getStackFromWorld(console.getWorld()));
		return stack;
	}
	
	private ItemStack getStackFromWorld(World world) {
		ITardisWorldData data = world.getCapability(Capabilities.TARDIS_DATA).orElse(null);
		if(data != null) {
			PanelInventory inv = data.getEngineInventoryForSide(Direction.SOUTH);
			for(int i = 0; i < inv.getSizeInventory(); ++i) {
				if(inv.getStackInSlot(i).getStack().getItem() == this.getEntry().getItem()) {
					return inv.getStackInSlot(i);
				}
			}
		}
		return ItemStack.EMPTY;
	}
	
	public boolean isUsable() {
		return !this.getStack().isEmpty();
	}
	
	public abstract void onLand();
	public abstract void onTakeoff();

}
