package net.tardis.mod.registries;

import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.animation.IExteriorAnimation.ExteriorAnimationEntry;
import net.tardis.mod.entity.ai.dalek.types.DalekType;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.consoles.Console;
import net.tardis.mod.sounds.InteriorHum;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.upgrades.UpgradeEntry;

import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TardisRegistries {

	public static Registry<DalekType> DALEK_TYPE;
	public static Registry<Protocol> PROTOCOL_REGISTRY;
	public static Registry<Console> CONSOLE_REGISTRY;
	public static Registry<SubsystemEntry<?>> SUBSYSTEM_REGISTRY;
	public static Registry<UpgradeEntry<?>> UPGRADES;
	public static Registry<ExteriorAnimationEntry<?>> EXTERIOR_ANIMATIONS;
	public static Registry<InteriorHum> HUM_REGISTRY;



	private static List<Runnable> REGISTRIES = new ArrayList<Runnable>();

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public static void init(FMLCommonSetupEvent event) {
		PROTOCOL_REGISTRY = new Registry<Protocol>();
		DALEK_TYPE = new Registry<DalekType>();
		CONSOLE_REGISTRY = new Registry<Console>();
		SUBSYSTEM_REGISTRY = new Registry<SubsystemEntry<?>>();
		UPGRADES = new Registry<UpgradeEntry<?>>();
		EXTERIOR_ANIMATIONS = new Registry<ExteriorAnimationEntry<?>>();
		HUM_REGISTRY = new Registry<InteriorHum>();
		
		doRegisters();
	}
	
	public static void doRegisters() {
		for (Runnable run : REGISTRIES) {
			run.run();
		}
	}
	
	public static void registerRegisters(Runnable run) {
		REGISTRIES.add(run);
	}
}
