package net.tardis.mod.tileentities.exteriors;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.client.animation.IExteriorAnimation;
import net.tardis.mod.client.animation.IExteriorAnimation.ExteriorAnimationEntry;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class ExteriorTile extends TileEntity implements ITickableTileEntity{
	
	private boolean locked = false;
	private EnumDoorState openState = EnumDoorState.CLOSED;
	//MUST contain at least one state
	private EnumDoorState[] validDoorStates = new EnumDoorState[]{
			EnumDoorState.CLOSED,
			EnumDoorState.ONE,
			EnumDoorState.BOTH
	};
	private DimensionType interiorDimension;
	private int antiSpamTicks = 0;
	private EnumMatterState matterState = EnumMatterState.SOLID;
	private List<UUID> teleportedIDs = new ArrayList<>();
	private String customName = "";
	private IExteriorAnimation animation;
	
	//Render variables
	public float alpha = 1F;
	public float lightLevel = 1F;
	
	public ExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
		animation = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(new ResourceLocation(Tardis.MODID, "classic"))
		.create(this);
	}

	@Override
	public void tick() {
		
		//decrement door timer
		if(this.antiSpamTicks > 0)
			--this.antiSpamTicks;
		
		this.transferEntities();

		if(this.matterState != EnumMatterState.SOLID)
			this.animation.tick();
		
		if(this.getMatterState() == EnumMatterState.DEMAT) {
			if(this.alpha <= 0.0) {
				world.setBlockState(getPos(), Blocks.AIR.getDefaultState());
				world.setBlockState(getPos().down(), Blocks.AIR.getDefaultState());
			}
		}
		
		else if(this.getMatterState() == EnumMatterState.REMAT) {
			if(alpha >= 1.0)
				this.setMatterState(EnumMatterState.SOLID);
		}
	}
	
	//Functions
	
	public void updateClient() {
		if(!world.isRemote)
			world.notifyBlockUpdate(this.getPos(), this.world.getBlockState(getPos()), this.world.getBlockState(getPos()), 3);
	}

	public void open(Entity opening) {
		if(this.antiSpamTicks == 0) {
			
			if(this.locked) {
				if(opening instanceof PlayerEntity)
					((PlayerEntity)opening).sendStatusMessage(ExteriorBlock.LOCKED, true);
				return;
			}
			
			this.setDoorState(this.getNextDoorState());
			this.setOther();
			if (getOpen().equals(EnumDoorState.CLOSED)) {
				world.playSound(null, getPos(), TSounds.DOOR_CLOSE, SoundCategory.BLOCKS, 0.5F, 1F);
			}
			else {
				world.playSound(null, getPos(), TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 0.5F, 1F);
			}
			this.antiSpamTicks = 5;
		}
	}
	
	public void demat() {
		this.setMatterState(EnumMatterState.DEMAT);
		this.setLocked(true);
		this.setDoorState(EnumDoorState.CLOSED);
		System.out.println("Demat called");
		
	}
	
	public void remat() {
		this.setMatterState(EnumMatterState.REMAT);
		this.setLocked(true);
		this.setDoorState(EnumDoorState.CLOSED);
		this.alpha = 0.0F;
		System.out.println("Remat called");
	}
	
	public boolean isKeyValid(ItemStack stack) {
		return stack.hasTag() &&
				stack.getTag().contains("tardis_key") &&
				Helper.getPlayerFromTARDIS(interiorDimension).toString().equals(stack.getTag().getString("tardis_key"));
	}
	
	public void setOther() {
		if(!world.isRemote) {
			ServerWorld server = world.getServer().getWorld(interiorDimension);
			if(server != null) {
				TileEntity te = server.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					ChunkPos pos = server.getChunk(TardisHelper.TARDIS_POS).getPos();
					for(int x = -3; x < 3; ++x) {
						for(int z = -3; z < 3; ++z) {
							server.forceChunk(pos.x + x, pos.z + z, true);
						}
					}
					DoorEntity door = ((ConsoleTile)te).getDoor();
					if(door != null) {
						door.setOpenState(this.getOpen());
						door.setLocked(this.locked);
						door.world.playSound(null, door.getPosition(), this.openState.equals(EnumDoorState.CLOSED)  ? TSounds.DOOR_CLOSE : TSounds.DOOR_OPEN, SoundCategory.BLOCKS, 0.5F, 1F);
						door.world.playSound(null, door.getPosition(), this.locked ? TSounds.DOOR_LOCK : TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 0.5F, 1F);
					}
				}
			}
		}
	}
	
	public void transferEntities() {
		if(!world.isRemote && this.getOpen() != EnumDoorState.CLOSED) {
			if(this.interiorDimension == null)
				return;
			List<Entity> entityList = world.getWorld().getEntitiesWithinAABB(Entity.class, this.getDoorAABB().offset(this.getPos()));
			
			//Stop if no entities to move
			
			List<UUID> tempIDs = new ArrayList<>();
			for(Entity e : entityList) {
				if(this.teleportedIDs.contains(e.getUniqueID())) {
					tempIDs.add(e.getUniqueID());
				}
			}
			this.teleportedIDs = tempIDs;
			
			if(entityList.isEmpty())
				return;
			
			double x = 0, y = TardisHelper.TARDIS_POS.getY(), z = 0;
			ConsoleTile console = null;
			ServerWorld ws = this.world.getServer().getWorld(this.interiorDimension);
			
			//Get Console
			if(ws != null) {
				TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile)
					console = (ConsoleTile)te;
			}
			
			//If an interior door exists, put the player near it
			DoorEntity door = console != null ? console.getDoor() : null;
			
			if(door != null) {
				x = door.posX;
				z = door.posZ;
				y = door.posY;
			}
			
			for(Entity e : entityList) {
				
				if(this.teleportedIDs.contains(e.getUniqueID()))
					return;
				
				if(door != null) {
					e.rotationYaw = door.rotationYaw - 180;
					door.addEntityToTeleportedList(e.getUniqueID());
				}
				
				Helper.teleportEntities(e, ws, x, y, z, door != null ? (door.rotationYaw - 180) : 0, e.rotationPitch);
			}
		}
	}
	
	public void addTeleportedEntity(UUID id) {
		this.teleportedIDs.add(id);
	}
	
	public EnumDoorState getNextDoorState() {
		int index = 0;
		for(EnumDoorState state : this.validDoorStates) {
			if(state == this.openState) {
				if(index + 1 < this.validDoorStates.length) 
					return this.validDoorStates[index + 1];
				 else return this.validDoorStates[0];
			}
			++index;
		}
		return EnumDoorState.CLOSED;
	}
	
	//Getters and setters
	
	public void setDoorState(EnumDoorState state) {
		this.openState = state;
		this.markDirty();
		this.updateClient();
	}
	
	public EnumDoorState getOpen() {
		return this.openState;
	}
	
	public void setLocked(boolean locked) {
		this.locked = locked;
		this.markDirty();
	}
	
	public boolean getLocked() {
		return this.locked;
	}
	
	public void toggleLocked() {
		if(this.antiSpamTicks <= 0) {
			this.locked = !this.locked;
			if(locked) {
				this.setDoorState(EnumDoorState.CLOSED);
				this.setOther();
			}
			this.antiSpamTicks = 20;
		}
	}
	
	public void setInterior(DimensionType type) {
		this.interiorDimension = type;
		this.markDirty();
	}
	
	public DimensionType getInterior() {
		return this.interiorDimension;
	}

	public EnumMatterState getMatterState() {
		return this.matterState;
	}
	
	public void setMatterState(EnumMatterState state) {
		this.matterState = state;
		this.markDirty();
		if(state != EnumMatterState.SOLID)
			this.setDoorState(EnumDoorState.CLOSED);
		this.updateClient();
	}
	
	public void setLightLevel(float percent) {
		this.lightLevel = percent;
		this.markDirty();
		this.updateClient();
	}
	
	public float getLightLevel() {
		return this.lightLevel;
	}
	
	public String getCustomName() {
		return this.customName;
	}
	
	public void setCustomName(String name) {
		this.customName = name;
		this.markDirty();
		this.updateClient();
	}
	
	public IExteriorAnimation getExteriorAnimation() {
		return this.animation;
	}
	
	public void setExteriorAnimation(ExteriorAnimationEntry<?> anim) {
		this.animation = anim.create(this);
		this.markDirty();
		this.updateClient();
	}
	
	//Scrape the necessary Data, including dimension from console
	public void copyConsoleData(ConsoleTile console) {
		this.interiorDimension = console.getWorld().getDimension().getType();
		DoorEntity ent = console.getDoor();
		if(ent != null) {
			this.locked = ent.isLocked();
			this.openState = ent.getOpenState();
		}
		else {
			this.locked = false;
			this.openState = EnumDoorState.CLOSED;
		}
		this.customName = console.getCustomName();
		this.animation = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(console.getExteriorManager().getExteriorAnimation())
				.create(this);
		this.lightLevel = console.getInteriorManager().getLight() / 15.0F;
		this.updateClient();
		
	}
	
	/*
	 * If an entity is inside this it will be transfered to the TARDIS
	 */
	public abstract AxisAlignedBB getDoorAABB();
	
	//Required Minecraft shit
	
	@Override
	public void read(CompoundNBT compound) {
		this.locked = compound.getBoolean("locked");
		this.openState = EnumDoorState.valueOf(compound.getString("state"));
		if(compound.contains("interior"))
			this.interiorDimension = DimensionType.byName(new ResourceLocation(compound.getString("interior")));
		if(compound.contains("matter_state"))
			this.matterState = EnumMatterState.values()[compound.getInt("matter_state")];
		this.lightLevel = compound.getFloat("light_level");
		this.customName = compound.getString("custom_name");
		this.animation = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(new ResourceLocation(compound.getString("animation")))
				.create(this);
		super.read(compound);
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putBoolean("locked", this.locked);
		compound.putString("state", this.openState.name());
		if(this.interiorDimension != null)
			compound.putString("interior", this.interiorDimension.getRegistryName().toString());
		compound.putInt("matter_state", this.matterState.ordinal());
		compound.putFloat("light_level", this.lightLevel);
		compound.putString("custom_name", customName);
		compound.putString("animation", this.animation.getType().getRegistryName().toString());
		return super.write(compound);
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		CompoundNBT tag = this.serializeNBT();
		tag.putFloat("alpha", this.alpha);
		tag.putInt("door_state", this.openState.ordinal());
		return tag;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.openState = EnumDoorState.values()[pkt.getNbtCompound().getInt("door_state")];
		this.locked = pkt.getNbtCompound().getBoolean("locked");
		this.matterState = EnumMatterState.values()[pkt.getNbtCompound().getInt("matter_state")];
		this.alpha = pkt.getNbtCompound().getFloat("alpha");
		this.lightLevel = pkt.getNbtCompound().getFloat("light_level");
		this.customName = pkt.getNbtCompound().getString("custom_name");
		this.animation = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(new ResourceLocation(pkt.getNbtCompound().getString("animation")))
				.create(this);
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
		this.alpha = tag.getFloat("alpha");
	}
	
}
