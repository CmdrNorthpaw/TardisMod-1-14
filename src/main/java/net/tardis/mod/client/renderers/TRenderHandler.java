package net.tardis.mod.client.renderers;
import javax.annotation.Nonnull;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;

public interface TRenderHandler {

	 /**
	   * Called when an item that implements this interface is in one of the equipment slot
	   * @param renderPlayer
	   *          The player renderer this is called for.
	   * @param equipmentSlot
	   *          The equipment slot the item is in. 
	   * @param item
	   *          The item that should render
	   * @param entitylivingbaseIn
	   *          see {@link LayerRenderer}
	   * @param limbSwing
	   *          see {@link LayerRenderer}
	   * @param limbSwingAmount
	   *          see {@link LayerRenderer}
	   * @param partialTicks
	   *          see {@link LayerRenderer}
	   * @param ageInTicks
	   *          see {@link LayerRenderer}
	   * @param netHeadYaw
	   *          see {@link LayerRenderer}
	   * @param headPitch
	   *          see {@link LayerRenderer}
	   * @param scale
	   *          see {@link LayerRenderer}
	   */
	
	
	  void doRenderLayer(@Nonnull PlayerRenderer renderPlayer, Inventory inventory, @Nonnull ItemStack item,
		      @Nonnull AbstractClientPlayerEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw,
		      float headPitch, float scale);
	
}
