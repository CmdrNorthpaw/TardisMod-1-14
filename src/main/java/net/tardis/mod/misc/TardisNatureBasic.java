package net.tardis.mod.misc;

import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class TardisNatureBasic extends TardisNature {

	@Override
	public int modFlight(int mood) {
		return mood + 10;
	}

	@Override
	public int modLanded(int mood) {
		return mood;
	}

	@Override
	public int modEmptyTick(int mood) {
		return mood > EnumHappyState.DISCONTENT.getTreshold() ? mood - 1 : 0;
	}

}
