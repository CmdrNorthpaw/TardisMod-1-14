package net.tardis.mod.tileentities.exteriors;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.TTiles;

public class RedExteriorTile extends ExteriorTile{

	public RedExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	public RedExteriorTile() {
		super(TTiles.EXTERIOR_RED);
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		return new AxisAlignedBB(-0.1, 0, -0.1, 1.1, 2, 1.1).offset(0, -1, 0);
	}

}
