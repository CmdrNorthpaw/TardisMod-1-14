package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.WaypointSaveMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class WaypointNewMonitorScreen extends MonitorScreen {

	private TextFieldWidget waypointName;
	public WaypointNewMonitorScreen(IMonitorGui mon) {
		super(mon, "waypoint_new");
	}

	@Override
	protected void init() {
		super.init();
		
		this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY(), new TranslationTextComponent("> Cancel"), 
				but -> Minecraft.getInstance().displayGuiScreen(null)));
		
		this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY(), new TranslationTextComponent("> Save"), but -> {
			Network.sendToServer(new WaypointSaveMessage(this.waypointName.getText()));
			Minecraft.getInstance().displayGuiScreen(null);
		}));
		
		int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
		int width = 150;
		this.addButton(this.waypointName = new TextFieldWidget(this.font, centerX - width / 2, this.parent.getMaxY() + 20, width, 20, "Test"));
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		
		TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile) {
			ConsoleTile con = (ConsoleTile)te;
			
			this.font.drawString(Helper.formatBlockPos(con.getLocation()),
					this.parent.getMinX(), this.parent.getMaxY() + 50, 0xFFFFFF);
			
			this.font.drawString(Helper.formatDimName(con.getDimension()),
					this.parent.getMinX(), this.parent.getMaxY() + 60, 0xFFFFFF);
		}
		
	}
}
