package net.tardis.mod.client.guis.widgets;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.tardis.mod.helper.Helper;

public class Page {
		
	List<Element> lines = new ArrayList<Element>();
	private int index;
	private FontRenderer font;
	
	public Page(FontRenderer font) {
		this.font = font;
	}
	
	public Page(FontRenderer font, String... text) {
		this(font);
		int size = 110;
		for(String s : text) {
			String temp = s;
			while(!(temp = this.splitString(temp, size)).isEmpty());
		}
	}
	
	//Returns left over
	private String splitString(String text, int width) {
		if(font.getStringWidth(text) > width) {
			String line = "";
			for(String word : text.split(" ")) {
				if(font.getStringWidth(word + " ") + font.getStringWidth(line) < width)
					line += word + " ";
				else {
					this.addLine(line);
					return text.replace(line, "");
				}
			}
		}
		this.addLine(text);
		return "";
	}
	
	public void render(int x, int y, int mouseX, int mouseY) {
		index = 0;
		for(Element ele : lines) {
			ele.render(x, y + (index * font.FONT_HEIGHT) + 2, mouseX, mouseY);
			++index;
		}
	}
	
	public void addLine(String s) {
		this.lines.add(new Element(s));
	}
	
	public static class Element{
		
		private String text;
		private Color color = Color.BLACK;
		private ElementType type = null;
		
		public Element(String text) {
			this.text = text;
			
			if(text.startsWith("<")) {
				for(ElementType type : ElementType.values()) {
					//If this is the element type
					if(text.startsWith("<" + type.getTag())) {
						if(text.length() > type.getTag().length() + 2) {
							String inner = text.substring(type.getTag().length() + 1, text.indexOf(">"));
							System.out.print(inner);
							this.text = text.substring(text.indexOf(">") + 1);
							this.type = type;
						}
						else System.err.println("Incorrect page format! " + text);
						return;
					}
				}
			}
		}
		
		public String getText() {
			return text;
		}
		
		public void setColor(Color color) {
			this.color = color;
		}
		
		public Color getColor() {
			return this.color;
		}
		
		public void render(int x, int y, int mouseX, int mouseY) {
			FontRenderer font = Minecraft.getInstance().fontRenderer;
			if(type == ElementType.LINK && Helper.isInBounds(mouseX, mouseY, x, y, x + font.getStringWidth(text), y + font.FONT_HEIGHT))
				font.drawString(text, x, y, 0x0000FF);
			else font.drawString(text, x, y, color.getRGB());
		}
	}
	
	public static enum ElementType{
		LINK("link"),
		IMAGE("img");
		
		String tag;
		
		ElementType(String tag){
			this.tag = tag;
		}
		
		public String getTag() {
			return this.tag;
		}
	}
}
