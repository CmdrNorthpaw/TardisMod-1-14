package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class TardisWorldCapability implements ITardisWorldData{

	private World world;
	
	public PanelInventory northInv = new PanelInventory(Direction.NORTH);
	public PanelInventory eastInv = new PanelInventory(Direction.EAST);
	public PanelInventory southInv = new PanelInventory(Direction.SOUTH);
	public PanelInventory westInv = new PanelInventory(Direction.WEST);
	
	public TardisWorldCapability(World world) {
		this.world = world;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.put("north_inv", northInv.serializeNBT());
		tag.put("east_inv", eastInv.serializeNBT());
		tag.put("south_inv", southInv.serializeNBT());
		tag.put("west_inv", westInv.serializeNBT());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		northInv.deserializeNBT(tag.getList("north_inv", Constants.NBT.TAG_COMPOUND));
		eastInv.deserializeNBT(tag.getList("east_inv", Constants.NBT.TAG_COMPOUND));
		southInv.deserializeNBT(tag.getList("south_inv", Constants.NBT.TAG_COMPOUND));
		westInv.deserializeNBT(tag.getList("west_inv", Constants.NBT.TAG_COMPOUND));
	}

	@Override
	public PanelInventory getEngineInventoryForSide(Direction dir) {
		switch(dir) {
			case EAST: return eastInv;
			case SOUTH: return southInv;
			case WEST: return westInv;
			default: return northInv;
		}
	}
	
	@Override
	public void tick() {
		
		//Artron charging
		if(world.getGameTime() % 200 == 0) {
			for(int index = 0; index < eastInv.getSizeInventory(); ++index) {
				if(eastInv.getStackInSlot(index).getItem() instanceof IArtronBattery) {
					((IArtronBattery)eastInv.getStackInSlot(index).getItem())
						.charge(eastInv.getStackInSlot(index), 10);
				}
			}
		}
	}

}
