package net.tardis.mod.items;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.artron.IArtronBattery;

/**
 * Created by 50ap5ud5
 * on 24 Mar 2020 @ 9:29:34 am
 */

//Used for providing charge to items
//Also used to power machines
public class BatteryItem extends TardisPartItem implements IArtronBattery{
	
	public BatteryItem() {
		
	}

	@Override
	public float charge(ItemStack stack, float amount) {
		//TODO: this
		return 0;
	}

	@Override
	public float discharge(ItemStack stack, float amount) {
		float current = readCharge(stack);
		if(amount <= current) {
			writeCharge(stack, current - amount);
			return amount;
		}
		writeCharge(stack, 0);
		return current;
	}

	@Override
	public float getMaxCharge(ItemStack stack) {
		return 250;
	}

	@Override
	public float getCharge(ItemStack stack) {
		return readCharge(stack);
	}
	
	public void writeCharge(ItemStack stack, float charge) {
		if(!stack.hasTag())
			stack.setTag(new CompoundNBT());
		stack.getTag().putFloat("artron", charge);
	}
	
	public float readCharge(ItemStack stack) {
		if(stack.getTag() != null && stack.getTag().contains("artron"))
			return stack.getTag().getFloat("artron");
		return 0F;
	}
	
}
