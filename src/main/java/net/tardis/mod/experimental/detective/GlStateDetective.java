package net.tardis.mod.experimental.detective;

import java.lang.reflect.Field;

import com.mojang.blaze3d.platform.GlStateManager;

/**
 * Created by Swirtzly
 * on 20/10/2019 @ 14:04
 */
public class GlStateDetective {

    public static void main(String... args) throws IllegalAccessException {
        final StringBuilder stringBuilder = new StringBuilder();
        appendClassFields(stringBuilder, GlStateManager.class, null, 15, 0);
        System.out.println(stringBuilder.toString());
    }

    static void appendClassFields(StringBuilder stringBuilder, Class<?> clazz, Object instance, int maxIterations, int currentIteration) throws IllegalAccessException {
        if (maxIterations <= currentIteration) {
            return;
        }
        for (final Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            for (int i = 0; i < currentIteration; ++i) {
                stringBuilder.append("  ");
            }
            if (field.getType().isPrimitive())
                stringBuilder
                        .append(field.getName())
                        .append(": ")
                        .append(field.get(instance))
                        .append("\n");
            else {
                stringBuilder
                        .append(field.getName())
                        .append(": ")
                        .append(field.getType().getSimpleName())
                        .append("\n");
                appendClassFields(stringBuilder, field.getType(), field.get(instance), maxIterations, currentIteration + 1);
            }
        }
    }

}
