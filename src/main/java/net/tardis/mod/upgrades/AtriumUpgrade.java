package net.tardis.mod.upgrades;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;

public class AtriumUpgrade extends Upgrade implements INBTSerializable<CompoundNBT>{

	private static ResourceLocation REGISTRY_NAME = new ResourceLocation(Tardis.MODID, "atrium");
	public static int radius = 5;
	private Map<BlockPos, BlockState> blocks = new HashMap<BlockPos, BlockState>();
	private int width = 0;
	private int height = 0;
	
	public AtriumUpgrade(UpgradeEntry<?> entry, ConsoleTile tile) {
		super(entry, tile);
		tile.registerDataHandler(REGISTRY_NAME, this);
	}
	
	//Should be relative to the exterior
	public void add(BlockPos pos, BlockState state) {
		this.blocks.put(pos, state);
	}
	
	public BlockState getState(BlockPos pos) {
		return this.blocks.getOrDefault(pos, Blocks.AIR.getDefaultState());
	}
	
	public Map<BlockPos, BlockState> getStoredBlocks(){
		return this.blocks;
	}
	
	public boolean isActive() {
		return this.isUsable();
	}
	
	public void recalculateSize() {
		int width = 0;
		int height = 0;
		for(BlockPos pos : this.blocks.keySet()) {
			if(Math.abs(pos.getX()) > width)
				width = Math.abs(pos.getX());
			if(Math.abs(pos.getZ()) > width)
				width = Math.abs(pos.getZ());
			
			if(pos.getY() > height)
				height = pos.getY();
		}
		this.width = width;
		this.height = height;
		System.out.println("Atrium size " + this.width + ", " + this.height);
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		ListNBT list = new ListNBT();
		for(Entry<BlockPos, BlockState> entry : this.blocks.entrySet()) {
			CompoundNBT nbt = new CompoundNBT();
			nbt.putLong("pos", entry.getKey().toLong());
			nbt.put("state", Helper.serializeBlockState(entry.getValue()));
			list.add(nbt);
		}
		tag.put("list", list);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		ListNBT list = nbt.getList("list", Constants.NBT.TAG_COMPOUND);
		for(INBT item : list) {
			CompoundNBT comp = (CompoundNBT)item;
			this.blocks.put(BlockPos.fromLong(comp.getLong("pos")), Helper.deserializeBlockState(comp.getCompound("state")));
		}
	}

	@Override
	public void onLand() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTakeoff() {
		// TODO Auto-generated method stub
		
	}
}
