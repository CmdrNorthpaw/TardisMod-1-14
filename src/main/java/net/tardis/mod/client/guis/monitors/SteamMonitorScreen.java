package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class SteamMonitorScreen extends BaseMonitorScreen{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitors/steam.png");
    private static int guiWidth = 255;
    private static int guiHeight = 189;
    
    public SteamMonitorScreen() {
		super(title);
	}
    
	@Override
	public int getMinY() {
		return this.height / 2 + 65;
	}

	@Override
	public int getMinX() {
		return this.width / 2 - 105;
	}

	@Override
	public int getMaxX() {
		return this.getMinX() + 200;
	}

	@Override
	public int getMaxY() {
		return this.getMinY() - 140;
	}
	
	
	@Override
	public void renderMonitor() {
		this.renderBackground();
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		this.blit(this.width / 2 - guiWidth / 2, this.height / 2 - guiHeight / 2, 11, 0, guiWidth, guiHeight);
	}
}
