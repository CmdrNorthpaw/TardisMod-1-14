package net.tardis.mod.client.guis.widgets;

import net.minecraft.client.gui.widget.AbstractSlider;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.LightUpdateMessage;

public class IntSliderWidget extends AbstractSlider{

	public IntSliderWidget(int x, int y, int width, int height, double p_i51144_5_) {
		super(x, y, width, height, p_i51144_5_);
	}

	@Override
	protected void applyValue() {
		
	}

	@Override
	protected void updateMessage() {
		Network.sendToServer(new LightUpdateMessage(this.value));
	}

}
