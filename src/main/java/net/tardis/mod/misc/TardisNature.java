package net.tardis.mod.misc;

/*
 * Natures are singletons, so beware
 */
public abstract class TardisNature {
	
	public abstract int modFlight(int mood);
	public abstract int modLanded(int mood);
	public abstract int modEmptyTick(int mood);

}
